<?php
Route::get('sendbasicemail', 'MailController@basic_email');
Route::get('tio_pu_logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/administrator/commingson', ['as'=>'commingson','uses'=>'Admin\DataAnggotaController@commingson']);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);
Route::get('/profil', 'UmumController@profil');
Route::get('/kontak', 'UmumController@kontak');
Route::get('/struktur-organisasi', 'UmumController@organisasi');
Route::get('/daftar', 'UmumController@daftar');
Route::post('/daftar', 'UmumController@create');

Route::get('/logout', 'HomeController@index');
Auth::routes();
//ROUTE ADMINISTRATOR
Route::get('/kartu', function () {
    return view('administrator.report.pdf');
});
Route::get('/administrator/reset-password', ['as'=>'admin-reset-password','uses'=>'Admin\DataAnggotaController@datagroup']);
Route::post('/administrator/reset-password', ['as'=>'admin-reset-password','uses'=>'Admin\DataAnggotaController@carigroup']);
Route::post('/administrator/update-password', ['as'=>'reset-password','uses'=>'Admin\DataAnggotaController@resetpassword']);

Route::get('/administrator/data-karyawan', ['as'=>'administrator-data-karyawan','uses'=>'Admin\StatistikController@karyawan']);
Route::post('/administrator/data-karyawan', ['as'=>'administrator-data-karyawan','uses'=>'Admin\StatistikController@karyawan']);

// Route::get('/administrator/update', 'Admin\PulsaController@update');
Route::get('/administrator/home', 'Admin\DataAnggotaController@home');
Route::post('/administrator/home', 'Admin\DataAnggotaController@tambarberita');
Route::post('/administrator/home/{id}', 'Admin\DataAnggotaController@editberita');

Route::get('/administrator/data-pendidikan', 'Admin\AdminController@pendidikan');
Route::post('/administrator/data-pendidikan', 'Admin\AdminController@pendidikan');
Route::post('/administrator/data-pendidikan/{id}', 'Admin\AdminController@editpendidikan');

Route::get('/administrator/data-rekening', ['as'=>'administratordata-rekening','uses'=>'Admin\DepositController@rekening']);
Route::post('/administrator/data-rekening', ['as'=>'administratordata-rekening','uses'=>'Admin\DepositController@rekening']);

Route::get('/administrator/data-komunitas', 'Admin\AdminController@komunitas');
Route::post('/administrator/data-komunitas', 'Admin\AdminController@komunitas');
Route::post('/administrator/data-komunitas/{id}', 'Admin\AdminController@editkomunitas');

Route::get('/administrator/data-kelurahan', 'Admin\AdminController@kelurahan');
Route::post('/administrator/data-kelurahan', 'Admin\AdminController@kelurahan');
Route::post('/administrator/data-kelurahan/{id}', 'Admin\AdminController@editkelurahan');

Route::get('/administrator/data-kecamatan', 'Admin\AdminController@kecamatan');
Route::post('/administrator/data-kecamatan', 'Admin\AdminController@kecamatan');
Route::post('/administrator/data-kecamatan/{id}', 'Admin\AdminController@editkecamatan');

Route::get('/administrator/data-kabupaten', 'Admin\AdminController@kabupaten');
Route::post('/administrator/data-kabupaten', 'Admin\AdminController@kabupaten');
Route::post('/administrator/data-kabupaten/{id}', 'Admin\AdminController@editkabupaten');

Route::get('/administrator/data-propinsi', 'Admin\AdminController@propinsi');
Route::post('/administrator/data-propinsi', 'Admin\AdminController@propinsi');
Route::post('/administrator/data-propinsi/{id}', 'Admin\AdminController@editpropinsi');

Route::get('/administrator/data-pendapatan', 'Admin\AdminController@pendapatan');
Route::post('/administrator/data-pendapatan', 'Admin\AdminController@pendapatan');
Route::post('/administrator/data-pendapatan/{id}', 'Admin\AdminController@editpendapatan');

Route::get('/administrator/data-pekerjaan', 'Admin\AdminController@pekerjaan');
Route::post('/administrator/data-pekerjaan', 'Admin\AdminController@pekerjaan');
Route::post('/administrator/data-pekerjaan/{id}', 'Admin\AdminController@editpekerjaan');

Route::get('/administrator/data-jenis-simpanan', ['as'=>'administrator-data-jenis-simpanan','uses'=>'Admin\AdminController@jenissimpanan']);
Route::post('/administrator/data-jenis-simpanan', ['as'=>'administrator-data-jenis-simpanan','uses'=>'Admin\AdminController@jenissimpanan']);
Route::post('/administrator/data-jenis-simpanan/{id}', ['as'=>'administrator-data-jenis-simpanan','uses'=>'Admin\AdminController@editjenissimpanan']);

Route::get('/administrator/data-admin', ['as'=>'admin-data-administrator','uses'=>'Admin\DataAnggotaController@dataadmin']);
Route::post('/administrator/data-admin', ['as'=>'admin-data-administrator','uses'=>'Admin\DataAnggotaController@dataadmin']);

Route::get('/administrator/data-administrator', 'Admin\DataAnggotaController@datagroup');
Route::post('/administrator/data-administrator', 'Admin\DataAnggotaController@carigroup');

Route::get('/administrator/data-anggota', ['as'=>'administrator-data-anggota','uses'=>'Admin\DataAnggotaController@index']);
Route::post('/administrator/data-anggota', 'Admin\DataAnggotaController@create');

Route::post('/administrator/data-anggota/propinsi', ['as'=>'admin-select-propinsi','uses'=>'Admin\DataAnggotaController@caripropinsi']);

Route::post('/administrator/export-data-anggota', 'Admin\DataAnggotaController@exportdataanggota');

Route::get('/administrator/detail-anggota/{id}', 'Admin\DataAnggotaController@detailanggota');
//BUKU SALDO
Route::post('/administrator/tambah-simpanan', 'Admin\DataAnggotaController@tambahsimpanan');
Route::post('/administrator/tambah-simpanan/{id}', 'Admin\DataAnggotaController@editsimpanan');
Route::get('/administrator/mutasi-simpanan', ['as'=>'admin-history-simpanan','uses'=>'Admin\DataAnggotaController@mutasi']);
Route::post('/administrator/mutasi-simpanan', ['as'=>'admin-history-simpanan','uses'=>'Admin\DataAnggotaController@mutasi']);
Route::get('/administrator/buku-saldo-transaksi-anggota', ['as'=>'buku-saldo-transaksi-anggota','uses'=>'Admin\DepositController@bukusaldoanggota']);
Route::post('/administrator/buku-saldo-transaksi-anggota', ['as'=>'buku-saldo-transaksi-anggota','uses'=>'Admin\DepositController@bukusaldoanggota']);

Route::get('/administrator/tarik-simpanan', ['as'=>'admin-tarik-simpanan','uses'=>'Admin\TransaksiController@tariksimpanan']);
Route::post('/administrator/tarik-simpanan', ['as'=>'admin-tarik-simpanan','uses'=>'Admin\TransaksiController@tariksimpanan']);
//DATA MASTER ADMIN
Route::get('/administrator/jenis-pemasukan-pengeluaran', ['as'=>'admin-jenis-pemasukan-pengeluaran','uses'=>'Admin\DataMasterController@ajpp']);
Route::post('/administrator/jenis-pemasukan-pengeluaran', ['as'=>'admin-jenis-pemasukan-pengeluaran','uses'=>'Admin\DataMasterController@ajpp']);
Route::get('/administrator/data-pemegang-saham', ['as'=>'admin-data-pemegang-saham','uses'=>'Admin\DataMasterController@pemegangsaham']);
Route::post('/administrator/data-pemegang-saham', ['as'=>'admin-data-pemegang-saham','uses'=>'Admin\DataMasterController@pemegangsaham']);

//EDIT SALDO
// Route::post('/administrator/edit-buku-saldo', 'Admin\DataAnggotaController@mutasi');
//TRANSAKSI
Route::post('/administrator/create-simpanan', ['as'=>'admin-simpan-pokok','uses'=>'Admin\DataAnggotaController@createsimpanan']);

Route::get('/administrator/transaksi/simpanan-pokok', ['as'=>'admin-form-simpanan-pokok','uses'=>'Admin\TransaksiController@adminsimpananpokok']);
Route::post('/administrator/transaksi/cek-nominal', ['as'=>'admin-check-anggota','uses'=>'Admin\TransaksiController@cekanggota']);
Route::post('/administrator/transaksi/check-pemegang-saham', ['as'=>'admin-check-pemegang-saham','uses'=>'Admin\TransaksiController@cekpemegangsaham']);
Route::post('/administrator/transaksi/simpanan-pokok', ['as'=>'admin-form-simpanan-pokok','uses'=>'Admin\TransaksiController@adminsimpananpokok']);
Route::get('/administrator/transaksi/simpanan-wajib', ['as'=>'admin-form-simpanan-wajib','uses'=>'Admin\TransaksiController@adminsimpananwajib']);
Route::post('/administrator/transaksi/simpanan-wajib', ['as'=>'admin-form-simpanan-wajib','uses'=>'Admin\TransaksiController@adminsimpananwajib']);
Route::get('/administrator/transaksi/simpanan-sukarela', ['as'=>'admin-form-simpanan-sukarela','uses'=>'Admin\TransaksiController@adminsimpanansukarela']);
Route::get('/administrator/transaksi/simpanan-khusus', ['as'=>'admin-form-simpanan-khusus','uses'=>'Admin\TransaksiController@adminsimpanankhusus']);
Route::post('/administrator/cek-pinjaman-anggota', ['as'=>'admin-pinjaman-anggota','uses'=>'Admin\TransaksiController@pinjamananggota']);
Route::get('/administrator/transaksi/penjualan-saham', ['as'=>'admin-transaksi-penjualan-saham','uses'=>'Admin\TransaksiController@penjualansaham']);
Route::post('/administrator/transaksi/penjualan-saham', ['as'=>'admin-transaksi-penjualan-saham','uses'=>'Admin\TransaksiController@penjualansaham']);
Route::get('/administrator/transaksi/pembelian-saham', ['as'=>'admin-transaksi-pembelian-saham','uses'=>'Admin\TransaksiController@pembeliansaham']);
Route::post('/administrator/transaksi/pembelian-saham', ['as'=>'admin-transaksi-pembelian-saham','uses'=>'Admin\TransaksiController@pembeliansaham']);

//MUTASI KAS
Route::get('/administrator/mutasi/kas', ['as'=>'admin-mutasi-kas','uses'=>'Admin\DatamutasiController@mutasikas']);
Route::post('/administrator/mutasi/kas', ['as'=>'admin-mutasi-kas','uses'=>'Admin\DatamutasiController@mutasikas']);
//MUTASI BANK
Route::get('/administrator/mutasi/bank', ['as'=>'admin-mutasi-bank','uses'=>'Admin\DatamutasiController@mutasibank']);
Route::post('/administrator/mutasi/bank', ['as'=>'admin-mutasi-bank','uses'=>'Admin\DatamutasiController@mutasibank']);

//PINJAMAN
Route::get('/administrator/transaksi/pinjaman', ['as'=>'admin-form-pinjaman','uses'=>'Admin\TransaksiController@pinjaman']);
Route::post('/administrator/transaksi/pinjaman', ['as'=>'admin-form-pinjaman','uses'=>'Admin\TransaksiController@pinjaman']);
Route::post('/administrator/transaksi/kalkulasi', ['as'=>'admin-check-kalkulasi','uses'=>'Admin\TransaksiController@kalkulasi']);

Route::get('/administrator/transaksi/bayar-pinjaman', ['as'=>'admin-bayar-pinjaman','uses'=>'Admin\TransaksiController@bayarpinjaman']);
Route::post('/administrator/transaksi/bayar-pinjaman', ['as'=>'admin-bayar-pinjaman','uses'=>'Admin\TransaksiController@bayarpinjaman']);

//LAPORAN SIMPANAN
Route::get('/administrator/laporan/simpanan', ['as'=>'admin-laporan-simpanan','uses'=>'Admin\LaporanController@simpananadmin']);
Route::post('/administrator/laporan/simpanan', ['as'=>'admin-laporan-simpanan','uses'=>'Admin\LaporanController@simpananadmin']);
Route::get('/administrator/laporan/pinjaman', ['as'=>'admin-laporan-pinjaman','uses'=>'Admin\LaporanController@pinjaman']);
Route::post('/administrator/laporan/pinjaman', ['as'=>'admin-laporan-pinjaman','uses'=>'Admin\LaporanController@pinjaman']);
Route::get('/administrator/laporan/list-simpanan', ['as'=>'admin-laporan-list-simpanan','uses'=>'Admin\LaporanController@listsimpanan']);
Route::post('/administrator/laporan/list-simpanan', ['as'=>'admin-laporan-list-simpanan','uses'=>'Admin\LaporanController@listsimpanan']);

//LAPORAN PEMBAYARAN PINJAMAN
Route::get('/administrator/laporan/bayar-pinjaman', ['as'=>'admin-pemabayaran-pinjaman','uses'=>'Admin\LaporanController@pembayaranpinjaman']);
Route::post('/administrator/laporan/bayar-pinjaman', ['as'=>'admin-pemabayaran-pinjaman','uses'=>'Admin\LaporanController@pembayaranpinjaman']);
Route::get('/administrator/laporan-pembayaran-pinjaman', ['as'=>'admin-laporan-pembayaran-pinjaman','uses'=>'Admin\LaporanController@pembayaran']);
Route::post('/administrator/laporan-pembayaran-pinjaman', ['as'=>'admin-laporan-pembayaran-pinjaman','uses'=>'Admin\LaporanController@pembayaran']);
//LAPORAN PENARIKAN SIAMPANAN
Route::get('/administrator/laporan/penarikan-simpanan', ['as'=>'admin-laporan-penarikan-simpanan','uses'=>'Admin\LaporanController@laporantariksimpanan']);
Route::post('/administrator/laporan/penarikan-simpanan', ['as'=>'admin-laporan-penarikan-simpanan','uses'=>'Admin\LaporanController@laporantariksimpanan']);
//LAPORAN ALL TRANSAKSI
Route::get('/administrator/laporan/keseluruhan', ['as'=>'admin-laporan-keseluruhan','uses'=>'Admin\LaporanController@laporankeseluruhan']);
Route::post('/administrator/laporan/keseluruhan', ['as'=>'admin-laporan-keseluruhan','uses'=>'Admin\LaporanController@laporankeseluruhan']);
//LAPORAN SAHAM
Route::get('/administrator/laporan-penjualan-saham', ['as'=>'admin-laporan-penjualan-saham','uses'=>'Admin\LaporanController@jualsaham']);
Route::post('/administrator/laporan-penjualan-saham', ['as'=>'admin-laporan-penjualan-saham','uses'=>'Admin\LaporanController@jualsaham']);
Route::get('/administrator/laporan-pembelian-saham', ['as'=>'admin-laporan-pembelian-saham','uses'=>'Admin\LaporanController@belisaham']);
Route::post('/administrator/laporan-pembelian-saham', ['as'=>'admin-laporan-pembelian-saham','uses'=>'Admin\LaporanController@belisaham']);

//TRANSAKSI ASET
Route::get('/administrator/transaksi-aset-masuk', ['as'=>'admin-aset-masuk','uses'=>'Admin\TransaksiController@asetmasuk']);
Route::post('/administrator/cek-jenis-aset', ['as'=>'admin-check-jenis','uses'=>'Admin\TransaksiController@cekjenis']);
Route::post('/administrator/transaksi-aset-masuk', ['as'=>'admin-aset-masuk','uses'=>'Admin\TransaksiController@asetmasuk']);
Route::get('/administrator/transaksi-aset-keluar', ['as'=>'admin-aset-keluar','uses'=>'Admin\TransaksiController@asetkeluar']);
Route::post('/administrator/transaksi-aset-keluar', ['as'=>'admin-aset-keluar','uses'=>'Admin\TransaksiController@asetkeluar']);
Route::get('/administrator/transaksi-pemindahan-aset', ['as'=>'admin-pemindahan-aset','uses'=>'Admin\TransaksiController@pindahaset']);
Route::post('/administrator/transaksi-pemindahan-aset', ['as'=>'admin-pemindahan-aset','uses'=>'Admin\TransaksiController@pindahaset']);
Route::get('/administrator/form-transaksi-aset-masuk', ['as'=>'admin-form-transaksi-aset-masuk','uses'=>'Admin\TransaksiController@formasetmasuk']);
Route::get('/administrator/form-edit-aset-masuk/{id}', ['as'=>'admin-form-edit-aset-masuk','uses'=>'Admin\TransaksiController@editmasetmasuk']);
Route::get('/administrator/form-transaksi-aset-keluar', ['as'=>'admin-form-transaksi-aset-keluar','uses'=>'Admin\TransaksiController@formasetkeluar']);
Route::get('/administrator/form-edit-aset-keluar/{id}', ['as'=>'admin-form-edit-aset-keluar','uses'=>'Admin\TransaksiController@editmasetkeluar']);

//DATA SHU
Route::get('/administrator/data-shu', ['as'=>'admin-data-shu','uses'=>'Admin\ShuController@index']);
Route::post('/administrator/data-shu', ['as'=>'admin-data-shu','uses'=>'Admin\ShuController@index']);

//DATA SIMPANAN
Route::get('/administrator/data-simpanan', 'Admin\DataAnggotaController@simpananadmin');
Route::post('/administrator/export-data-simpanan', 'Admin\DataAnggotaController@exporsimpananadmin');
Route::post('/administrator/data-simpanan', 'Admin\DataAnggotaController@simpananadmin');
//REPORT SIMPANAN WAJIB
Route::get('/administrator/status-simpanan-wajib', ['as'=>'administratorstatus-simpanan-wajib','uses'=>'Admin\DataAnggotaController@alert']);
Route::post('/administrator/status-simpanan-wajib', ['as'=>'administratorstatus-simpanan-wajib','uses'=>'Admin\DataAnggotaController@alert']);
Route::get('/administrator/simpanan-jatuh-tempo', 'Admin\DataAnggotaController@jthtempo');
Route::post('/administrator/simpanan-jatuh-tempo', 'Admin\DataAnggotaController@jthtempo');
//SIMPANAN JATUH TEMPO
Route::get('/administrator/jatuh-tempo-simpanan-wajib', ['as'=>'admin-simpanan-jatuh-tempo','uses'=>'Admin\LaporanController@tempo']);
Route::post('/administrator/jatuh-tempo-simpanan-wajib', ['as'=>'admin-simpanan-jatuh-tempo','uses'=>'Admin\LaporanController@tempo']);

//Akumulasi
Route::get('/administrator/akumulasi-belanja', 'Admin\DataAnggotaController@akumulasiadmin');
Route::post('/administrator/akumulasi-belanja', 'Admin\DataAnggotaController@akumulasiadmin');
Route::get('/administrator/saldo-akumulasi-belanja', 'Admin\DataAnggotaController@saldoakumulasiadmin');
Route::post('/administrator/saldo-akumulasi-belanja', 'Admin\DataAnggotaController@saldoakumulasiadmin');

Route::get('/administrator/data-pengurus/admin/{id}', 'Admin\AdminController@toadmin');
Route::get('/administrator/data-pengurus/pengurus/{id}', 'Admin\AdminController@topengurus');
Route::get('/administrator/data-pengurus/anggota/{id}', 'Admin\AdminController@toanggota');
//ROUTE DAFTAR ONLINE
Route::get('/administrator/daftar-online', 'Admin\AdminController@todaftar');
Route::post('/administrator/daftar-online/{id}', 'Admin\AdminController@actiondaftar');
//STATISTIK
Route::get('/administrator/statistik-akumulasi', 'Admin\StatistikController@statistikakumulasi');
Route::post('/administrator/statistik-akumulasi', 'Admin\StatistikController@statistikakumulasi');
Route::get('/administrator/statistik-jenis-kelamin', ['as'=>'administrator-statistik-jenis-kelamin','uses'=>'Admin\StatistikController@chart']);
Route::get('/administrator/statistik-kelurahan', 'Admin\StatistikController@statistikkelurahan');
Route::get('/administrator/statistik-kecamatan', 'Admin\StatistikController@statistikkecamatan');
Route::get('/administrator/statistik-kabupaten', 'Admin\StatistikController@statistikkabupaten');
Route::get('/administrator/statistik-simpanan', ['as'=>'administrator-statistik-simpanan','uses'=>'Admin\StatistikController@statistiksimpanan']);
Route::get('/administrator/statistik-pinjaman', ['as'=>'admin-statistik-pinjaman','uses'=>'Admin\StatistikController@statistikpinjaman']);
Route::get('/administrator/statistik-simpanan-tahunan', ['as'=>'admin-statistik-simpanan-tahunan','uses'=>'Admin\StatistikController@statistiksimpananpokok']);

Route::get('/administrator/kartu-anggota', ['as'=>'administrator-kartu-anggota','uses'=>'Admin\AdminController@kartu']);
Route::post('/administrator/kartu-anggota', ['as'=>'administrator-kartu-anggota','uses'=>'Admin\AdminController@uploads']);
//PORTAL PULSA
Route::get('/administrator/daftar-harga', 'Admin\PortalPulsaController@daftarharga');
Route::post('/administrator/daftar-harga', 'Admin\PortalPulsaController@daftarharga');

//data Diri
Route::get('/administrator/profil', ['as'=>'administrator-profil','uses'=>'Admin\DataDiriController@profil']);
Route::post('/administrator/profil', ['as'=>'administrator-profil','uses'=>'Admin\DataDiriController@profil']);
Route::get('/administrator/simpanan', 'Admin\DataDiriController@simpanananggota');
Route::post('/administrator/simpanan', 'Admin\DataDiriController@simpanananggota');
Route::get('/administrator/my-buku-saldo', 'Admin\DataDiriController@bukusaldo');
Route::post('/administrator/my-buku-saldo', 'Admin\DataDiriController@bukusaldo');
Route::get('/administrator/ganti-password', ['as'=>'administrator-ganti-password','uses'=>'Admin\DataDiriController@password']);
Route::post('/administrator/ganti-password', ['as'=>'administrator-ganti-password','uses'=>'Admin\DataDiriController@gantipassword']);
Route::get('/administrator/kartu', 'Admin\DataDiriController@kartu');
Route::get('/administrator/Download-kartu/{id}', 'Admin\DataDiriController@download');
Route::post('/administrator/Download-kartu/{id}', 'Admin\DataDiriController@downloads');
Route::get('/administrator/download-foto/{id}', 'Admin\DataDiriController@foto');
Route::get('/administrator/my-akumulasi-belanja', 'Admin\DataDiriController@akumulasi');
Route::post('/administrator/my-akumulasi-belanja', 'Admin\DataDiriController@akumulasi');
Route::get('/administrator/buku-saldo-transaksi', ['as'=>'my-buku-saldo-transaksi','uses'=>'Admin\DataDiriController@saldotransaksi']);
Route::post('/administrator/buku-saldo-transaksi', ['as'=>'my-buku-saldo-transaksi','uses'=>'Admin\DataDiriController@saldotransaksi']);
//DEPOSIT
Route::get('/administrator/topup-deposit', ['as'=>'topup-deposit','uses'=>'Admin\DepositController@topup']);
Route::post('/administrator/topup-deposit', ['as'=>'topup-deposit','uses'=>'Admin\DepositController@topup']);
Route::post('select-bank', ['as'=>'select-bank','uses'=>'Admin\DepositController@selectbank']);
Route::get('/administrator/konfirmasi-deposit', ['as'=>'konfirmasi-deposit','uses'=>'Admin\DepositController@konfdep']);
Route::post('/administrator/konfirmasi-deposit', ['as'=>'konfirmasi-deposit','uses'=>'Admin\DepositController@konfdep']);
Route::get('/administrator/laporan-deposit', ['as'=>'laporan-deposit','uses'=>'Admin\DepositController@laporan']);
Route::post('/administrator/laporan-deposit', ['as'=>'laporan-deposit','uses'=>'Admin\DepositController@laporan']);
//SUPLAYER
Route::get('/administrator/topup-suplayer', ['as'=>'topup-suplayer','uses'=>'Admin\DepositController@topupsuplayer']);
Route::post('/administrator/topup-suplayer', ['as'=>'topup-suplayer','uses'=>'Admin\DepositController@topupsuplayer']);
Route::get('/administrator/buku-saldo-suplayer', ['as'=>'buku-saldo-suplayer','uses'=>'Admin\DepositController@bukusaldosuplayer']);
Route::post('/administrator/buku-saldo-suplayer', ['as'=>'buku-saldo-suplayer','uses'=>'Admin\DepositController@bukusaldosuplayer']);
//ROUTE PULSA
Route::get('/administrator/harga-ppob', ['as'=>'harga-ppob','uses'=>'Admin\PulsaController@cekharga']);
Route::post('/administrator/harga-ppob', 'Admin\PulsaController@cekharga');
Route::post('select-provider', ['as'=>'select-provider','uses'=>'Admin\PulsaController@selectprovider']);
Route::post('select-paket', ['as'=>'select-paket','uses'=>'Admin\PulsaController@selectpaket']);
//PLN PASCABAYAR
Route::get('/administrator/pln-pascabayar', ['as'=>'pln-pascabayar','uses'=>'Admin\PulsaController@pascabayar']);
Route::post('/administrator/pln-pascabayar', ['as'=>'pln-pascabayar','uses'=>'Admin\PulsaController@pascabayar']);
//TOKO ONLINE
Route::get('/administrator/produk', 'Admin\TokoController@produk');
Route::post('/administrator/produk', 'Admin\TokoController@produk');
Route::get('/administrator/belanja', ['as'=>'belanja','uses'=>'Admin\TokoController@belanja']);
Route::post('/administrator/belanja', ['as'=>'belanja','uses'=>'Admin\TokoController@belanja']);
Route::post('/administrator/bayar', 'Admin\TokoController@bayar');
Route::get('/administrator/alamat-kirim', 'Admin\TokoController@alamat');
Route::post('/administrator/alamat-kirim', 'Admin\TokoController@alamat');
Route::get('/administrator/permintaan-barang', ['as'=>'administrator-permintaan-barang','uses'=>'Admin\TokoController@permintaan']);
Route::post('/administrator/permintaan-barang', ['as'=>'administrator-permintaan-barang','uses'=>'Admin\TokoController@permintaan']);
Route::get('/administrator/pembatalan-barang', ['as'=>'administrator-pembatalan-barang','uses'=>'Admin\TokoController@pembatalan']);
Route::post('/administrator/pembatalan-barang', ['as'=>'administrator-pembatalan-barang','uses'=>'Admin\TokoController@pembatalan']);
Route::get('/administrator/kirim-barang', ['as'=>'administrator-kirim-barang','uses'=>'Admin\TokoController@kirimbarang']);
Route::post('/administrator/kirim-barang', ['as'=>'administrator-kirim-barang','uses'=>'Admin\TokoController@kirimbarang']);
Route::get('/administrator/laporan-penjualan', ['as'=>'administrator-laporan-penjualan','uses'=>'Admin\TokoController@terimabarang']);
Route::post('/administrator/laporan-penjualan', ['as'=>'administrator-laporan-penjualan','uses'=>'Admin\TokoController@terimabarang']);
Route::get('/administrator/laporan-barang', ['as'=>'administrator-laporan-barang','uses'=>'Admin\TokoController@terimabarang']);
Route::post('/administrator/laporan-barang', ['as'=>'administrator-laporan-barang','uses'=>'Admin\TokoController@terimabarang']);
Route::get('/administrator/transaksi-ppob', ['as'=>'transaksi-ppob','uses'=>'Admin\TokoController@transaksi']);
//PULSA
Route::get('/administrator/pulsa', ['as'=>'admin-transaksi-pulsa','uses'=>'Admin\TokoController@pulsa']);
Route::get('/administrator/paket-data-internet', ['as'=>'admin-paket-data-internet','uses'=>'Admin\TokoController@paketdata']);
Route::get('/administrator/voucher-listrik', ['as'=>'admin-voucher-listrik','uses'=>'Admin\TokoController@voucher']);
Route::get('/administrator/saldo-ojek', ['as'=>'admin-saldo-ojek','uses'=>'Admin\TokoController@saldoojek']);
Route::get('/administrator/paket-sms', ['as'=>'admin-paket-sms','uses'=>'Admin\TokoController@paketsms']);
Route::get('/administrator/transfer-pulsa', ['as'=>'admin-transfer-pulsa','uses'=>'Admin\TokoController@transferpulsa']);
Route::post('/administrator/proses-ppob', ['as'=>'proses-ppob','uses'=>'Admin\ProsesPpobController@prosesppob']);
Route::get('/administrator/data-transaksi-ppob', ['as'=>'data-transaksi-ppob','uses'=>'Admin\PulsaController@datappob']);
Route::get('/administrator/laporan-penjualan-ppob', ['as'=>'administrator-laporan-ppob','uses'=>'Admin\PulsaController@reportppob']);
Route::post('/administrator/laporan-penjualan-ppob', ['as'=>'administrator-laporan-ppob','uses'=>'Admin\PulsaController@reportppob']);

// Route::get('/administrator/pengaturan',['as'=>'administrator.pengaturan','uses'=>'Admin\PengaturanController@index']);

//RAJAONGKIR
Route::post('select-jumlah', ['as'=>'select-jumlah','uses'=>'Admin\TokoController@jumlah']);
Route::post('select-province', ['as'=>'select-province','uses'=>'Admin\TokoController@province']);
Route::post('select-city', ['as'=>'select-city','uses'=>'Admin\TokoController@city']);
Route::post('select-kirim', ['as'=>'select-kirim','uses'=>'Admin\TokoController@kirim']);

// ROUTE Anggota
Route::post('anggota-select-jumlah', ['as'=>'anggota-select-jumlah','uses'=>'Anggota\ProfilController@jumlah']);
Route::post('anggota-select-province', ['as'=>'anggota-select-province','uses'=>'Anggota\ProfilController@province']);
Route::post('anggota-select-city', ['as'=>'anggota-select-city','uses'=>'Anggota\ProfilController@city']);
Route::post('anggota-select-kirim', ['as'=>'anggota-select-kirim','uses'=>'Anggota\ProfilController@kirim']);
Route::post('anggota/bayar', ['as'=>'anggota-bayar','uses'=>'Anggota\ProfilController@bayar']);
Route::post('anggota/anggota-bayar-dgn-saldo', ['as'=>'anggota-bayar-dgn-saldo','uses'=>'Anggota\ProfilController@bayardgnsaldo']);


Route::get('/anggota/home', 'Anggota\ProfilController@home');
Route::get('/anggota/profil', 'Anggota\ProfilController@profil');
Route::post('/anggota/profil', 'Anggota\ProfilController@profil');
Route::get('/anggota/kartu', 'Anggota\ProfilController@kartu');
Route::get('/anggota/Download-kartu/{id}', 'Anggota\ProfilController@download');
Route::get('/anggota/simpanan', 'Anggota\ProfilController@simpanananggota');
Route::post('/anggota/simpanan', 'Anggota\ProfilController@simpanananggota');
Route::get('/anggota/akumulasi-belanja', 'Anggota\ProfilController@akumulasi');
Route::post('/anggota/akumulasi-belanja', 'Anggota\ProfilController@akumulasi');
Route::get('/anggota/mutasi-simpanan', ['as'=>'anggota-mutasi-simpanan','uses'=>'Anggota\ProfilController@bukusaldo']);
Route::post('/anggota/mutasi-simpanan', ['as'=>'anggota-mutasi-simpanan','uses'=>'Anggota\ProfilController@bukusaldo']);
Route::get('/anggota/buku-saldo-transaksi', ['as'=>'anggota-buku-saldo-transaksi','uses'=>'Anggota\ProfilController@saldotransaksi']);
Route::post('/anggota/buku-saldo-transaksi', ['as'=>'anggota-buku-saldo-transaksi','uses'=>'Anggota\ProfilController@saldotransaksi']);
Route::get('/anggota/topup-deposit', ['as'=>'topup-deposit-anggota','uses'=>'Anggota\ProfilController@topup']);
Route::post('/anggota/topup-deposit', ['as'=>'topup-deposit-anggota','uses'=>'Anggota\ProfilController@topup']);
Route::post('bank-select', ['as'=>'bank-select','uses'=>'Anggota\ProfilController@bankselect']);

Route::get('/anggota/ganti-password', 'Anggota\ProfilController@password');
Route::post('/anggota/ganti-password', 'Anggota\ProfilController@gantipassword');
Route::get('/anggota/statistik', 'Anggota\ProfilController@statistik');
Route::get('/anggota/statistik-penjualan', 'Anggota\ProfilController@penjualan');
Route::post('/anggota/statistik-penjualan', 'Anggota\ProfilController@penjualan');
Route::get('/anggota/statistik-jenis-kelamin', 'Anggota\ProfilController@kelamin');
Route::get('/anggota/statistik-kelurahan', 'Anggota\ProfilController@kelurahan');
Route::get('/anggota/statistik-kecamatan', 'Anggota\ProfilController@kecamatan');
Route::get('/anggota/statistik-kabupaten', 'Anggota\ProfilController@kabupaten');

//DATA SAHAM
Route::get('/anggota/data-penjualan-saham', ['as'=>'anggota-penjualan-saham','uses'=>'Anggota\ProfilController@datajualsaham']);
Route::post('/anggota/data-penjualan-saham', ['as'=>'anggota-penjualan-saham','uses'=>'Anggota\ProfilController@datajualsaham']);
Route::get('/anggota/data-pembelian-saham', ['as'=>'anggota-pembelian-saham','uses'=>'Anggota\ProfilController@databelisaham']);
Route::post('/anggota/data-pembelian-saham', ['as'=>'anggota-pembelian-saham','uses'=>'Anggota\ProfilController@databelisaham']);
//DATA PINJAMAN
Route::get('/anggota/data-pinjaman', ['as'=>'anggota-data-pinjaman','uses'=>'Anggota\ProfilController@datapinjaman']);
Route::post('/anggota/data-pinjaman', ['as'=>'anggota-data-pinjaman','uses'=>'Anggota\ProfilController@datapinjaman']);

//MENU LAIN ANGGOTA
Route::get('/anggota/belanja', ['as'=>'anggota-belanja','uses'=>'Anggota\ProfilController@belanja']);
Route::post('/anggota/belanja', ['as'=>'anggota-belanja','uses'=>'Anggota\ProfilController@belanja']);

Route::get('/anggota/pulsa', ['as'=>'transaksi-pulsa','uses'=>'Anggota\ProfilController@pulsa']);
Route::get('/anggota/paket-data-internet', ['as'=>'paket-data-internet','uses'=>'Anggota\ProfilController@paketdata']);
Route::get('/anggota/voucher-listrik', ['as'=>'voucher-listrik','uses'=>'Anggota\ProfilController@voucher']);
Route::get('/anggota/saldo-ojek', ['as'=>'saldo-ojek','uses'=>'Anggota\ProfilController@saldoojek']);
Route::get('/anggota/paket-sms', ['as'=>'paket-sms','uses'=>'Anggota\ProfilController@paketsms']);
Route::get('/anggota/transfer-pulsa', ['as'=>'transfer-pulsa','uses'=>'Anggota\ProfilController@transferpulsa']);

Route::post('anggota-select-provider', ['as'=>'anggota-select-provider','uses'=>'Anggota\ProfilController@selectprovider']);
Route::post('anggota-select-paket', ['as'=>'anggota-select-paket','uses'=>'Anggota\ProfilController@selectpaket']);

Route::post('/anggota/proses-ppob', ['as'=>'anggota-proses-ppob','uses'=>'Anggota\ProfilController@prosesppob']);
Route::get('/anggota/data-transaksi-ppob', ['as'=>'anggota-data-transaksi-ppob','uses'=>'Anggota\ProfilController@datappob']);
Route::get('/anggota/data-transaksi-belanja', ['as'=>'anggota-data-transaksi-belanja','uses'=>'Anggota\ProfilController@databelanja']);
Route::post('/anggota/data-transaksi-belanja', ['as'=>'anggota-data-transaksi-belanja','uses'=>'Anggota\ProfilController@databelanja']);
Route::get('/anggota/bayar-dengan-saldo/{id}', ['as'=>'anggota-bayar-dengan-saldo','uses'=>'Anggota\ProfilController@bayarbelanja']);

Route::get('/anggota/data-topup', ['as'=>'data.topup','uses'=>'Anggota\ProfilController@datatopup']);
Route::post('/anggota/data-topup', ['as'=>'data.topup','uses'=>'Anggota\ProfilController@datatopup']);

Route::post('/anggota/cek-status', ['as'=>'anggota-cek-status','uses'=>'Anggota\ProfilController@cekstatus']);
Route::get('/anggota/cek-tagihan-listrik', ['as'=>'anggota-cek-tagihan-listrik','uses'=>'Anggota\ProfilController@tagihan']);
Route::post('/anggota/cek-tagihan-listrik', ['as'=>'anggota-cek-tagihan-listrik','uses'=>'Anggota\ProfilController@tagihan']);

//ROUTE PENGURUS
Route::get('/pengurus/home', 'Pengurus\PengurusController@homepengurus');
Route::get('/pengurus/data-group', 'Pengurus\PengurusController@datagroup');
Route::post('/pengurus/data-group', 'Pengurus\PengurusController@carigroup');
Route::get('/pengurus/data-anggota', 'Pengurus\PengurusController@index');
Route::post('/pengurus/data-anggota', 'Pengurus\PengurusController@index');
Route::get('/pengurus/buku-saldo', 'Pengurus\PengurusController@mutasi');
Route::post('/pengurus/buku-saldo', 'Pengurus\PengurusController@mutasi');
Route::get('/pengurus/data-simpanan', 'Pengurus\PengurusController@simpananadmin');
Route::post('/pengurus/data-simpanan', 'Pengurus\PengurusController@simpananadmin');
Route::get('/pengurus/akumulasi-belanja', 'Pengurus\PengurusController@akumulasiadmin');
Route::post('/pengurus/akumulasi-belanja', 'Pengurus\PengurusController@akumulasiadmin');
Route::get('/pengurus/kartu-anggota', 'Pengurus\PengurusController@kartu');
Route::post('/pengurus/kartu-anggota', 'Pengurus\PengurusController@kartu');
Route::get('/pengurus/status-simpanan-wajib', 'Pengurus\PengurusController@alert');
Route::post('/pengurus/status-simpanan-wajib', 'Pengurus\PengurusController@alert');
Route::get('/pengurus/download-foto/{id}', 'Pengurus\PengurusController@foto');
//DATA MASTER
Route::get('/pengurus/data-pendidikan',['as'=>'pengurus.data-pendidikan','uses'=>'Pengurus\DataMasterController@pendidikan']);
Route::get('/pengurus/data-rekening', ['as'=>'pengurus.data-rekening','uses'=>'Pengurus\DataMasterController@rekening']);
Route::get('/pengurus/data-komunitas', ['as'=>'pengurus.data-komunitas','uses'=>'Pengurus\DataMasterController@komunitas']);
Route::get('/pengurus/data-kelurahan', ['as'=>'pengurus.data-kelurahan','uses'=>'Pengurus\DataMasterController@kelurahan']);
Route::get('/pengurus/data-kecamatan', ['as'=>'pengurus.data-kecamatan','uses'=>'Pengurus\DataMasterController@kecamatan']);
Route::get('/pengurus/data-kabupaten', ['as'=>'pengurus.data-kabupaten','uses'=>'Pengurus\DataMasterController@kabupaten']);
Route::get('/pengurus/data-propinsi', ['as'=>'pengurus.data-propinsi','uses'=>'Pengurus\DataMasterController@propinsi']);
Route::get('/pengurus/data-pendapatan', ['as'=>'pengurus.data-pendapatan','uses'=>'Pengurus\DataMasterController@pendapatan']);
Route::get('/pengurus/data-pekerjaan', ['as'=>'pengurus.data-pekerjaan','uses'=>'Pengurus\DataMasterController@pekerjaan']);
Route::get('/pengurus/data-jenis-simpanan', ['as'=>'pengurus.data-jenis-simpanan','uses'=>'Pengurus\DataMasterController@jenissimpanan']);

//data Diri
Route::get('/pengurus/profil', 'Pengurus\DataDiriController@profil');
Route::post('/pengurus/profil', 'Pengurus\DataDiriController@profil');
Route::get('/pengurus/simpanan', 'Pengurus\DataDiriController@simpanananggota');
Route::post('/pengurus/simpanan', 'Pengurus\DataDiriController@simpanananggota');
Route::get('/pengurus/my-buku-saldo', 'Pengurus\DataDiriController@bukusaldo');
Route::post('/pengurus/my-buku-saldo', 'Pengurus\DataDiriController@bukusaldo');
Route::get('/pengurus/buku-saldo-transaksi', ['as'=>'buku-saldo-transaksi','uses'=>'Pengurus\DataDiriController@saldotransaksi']);
Route::post('/pengurus/buku-saldo-transaksi', ['as'=>'buku-saldo-transaksi','uses'=>'Pengurus\DataDiriController@saldotransaksi']);
Route::get('/pengurus/ganti-password', 'Pengurus\DataDiriController@password');
Route::post('/pengurus/ganti-password', 'Pengurus\DataDiriController@gantipassword');
Route::get('/pengurus/kartu', 'Pengurus\DataDiriController@kartu');
Route::get('/pengurus/Download-kartu/{id}', 'Pengurus\DataDiriController@download');
Route::get('/pengurus/my-akumulasi-belanja', 'Pengurus\DataDiriController@akumulasi');
Route::post('/pengurus/my-akumulasi-belanja', 'Pengurus\DataDiriController@akumulasi');
Route::get('/pengurus/statistik-akumulasi', 'Pengurus\PengurusController@statistik');
Route::get('/pengurus/statistik-jenis-kelamin', 'Pengurus\PengurusController@chart');
Route::post('/pengurus/statistik-akumulasi', 'Pengurus\PengurusController@statistik');
Route::get('/pengurus/statistik-simpanan', 'Pengurus\PengurusController@statistiksimpanan');
Route::get('/pengurus/statistik-kelurahan', 'Pengurus\PengurusController@statistikkelurahan');
Route::get('/pengurus/statistik-kecamatan', 'Pengurus\PengurusController@statistikkecamatan');
Route::get('/pengurus/statistik-kabupaten', 'Pengurus\PengurusController@statistikkabupaten');
//DATA KARYAWAN
Route::get('/pengurus/data-karyawan', 'Pengurus\PengurusController@karyawan');
Route::post('/pengurus/data-karyawan', 'Pengurus\PengurusController@karyawan');
Route::get('/pengurus/saldo-akumulasi-belanja', 'Pengurus\PengurusController@saldoakumulasipengurus');
Route::post('/pengurus/saldo-akumulasi-belanja', 'Pengurus\PengurusController@saldoakumulasipengurus');

//TRANSAKSI PULSA
Route::get('/pengurus/belanja', ['as'=>'pengurus-belanja','uses'=>'Pengurus\DataDiriController@belanja']);
Route::post('/pengurus/belanja', ['as'=>'pengurus-belanja','uses'=>'Pengurus\DataDiriController@belanja']);
Route::post('pengurus-select-jumlah', ['as'=>'pengurus-select-jumlah','uses'=>'Pengurus\DataDiriController@jumlah']);
Route::post('pengurus-select-province', ['as'=>'pengurus-select-province','uses'=>'Pengurus\DataDiriController@province']);
Route::post('pengurus-select-city', ['as'=>'pengurus-select-city','uses'=>'Pengurus\DataDiriController@city']);
Route::post('pengurus-select-kirim', ['as'=>'pengurus-select-kirim','uses'=>'Pengurus\DataDiriController@kirim']);
Route::post('pengurus/bayar', ['as'=>'pengurus-bayar','uses'=>'Pengurus\DataDiriController@bayar']);
Route::get('/pengurus/data-transaksi-belanja', ['as'=>'pengurus-data-transaksi-belanja','uses'=>'Pengurus\DataDiriController@databelanja']);
Route::post('/pengurus/data-transaksi-belanja', ['as'=>'pengurus-data-transaksi-belanja','uses'=>'Pengurus\DataDiriController@databelanja']);


Route::get('/pengurus/pulsa', ['as'=>'pengurus-pulsa','uses'=>'Pengurus\DataDiriController@pulsa']);
Route::get('/pengurus/paket-data-internet', ['as'=>'paket-data-internet','uses'=>'Pengurus\DataDiriController@paketdata']);
Route::get('/pengurus/voucher-listrik', ['as'=>'voucher-listrik','uses'=>'Pengurus\DataDiriController@voucher']);
Route::get('/pengurus/saldo-ojek', ['as'=>'saldo-ojek','uses'=>'Pengurus\DataDiriController@saldoojek']);
Route::get('/pengurus/paket-sms', ['as'=>'paket-sms','uses'=>'Pengurus\DataDiriController@paketsms']);
Route::get('/pengurus/transfer-pulsa', ['as'=>'transfer-pulsa','uses'=>'Pengurus\DataDiriController@transferpulsa']);
Route::get('/pengurus/cek-tagihan-listrik', ['as'=>'pengurus-cek-tagihan-listrik','uses'=>'Pengurus\DataDiriController@tagihan']);
Route::post('/pengurus/cek-tagihan-listrik', ['as'=>'pengurus-cek-tagihan-listrik','uses'=>'Pengurus\DataDiriController@tagihan']);

Route::post('pengurus-select-provider', ['as'=>'pengurus-select-provider','uses'=>'Pengurus\DataDiriController@selectprovider']);
Route::post('pengurus-select-paket', ['as'=>'pengurus-select-paket','uses'=>'Pengurus\DataDiriController@selectpaket']);

Route::post('/pengurus/proses-ppob', ['as'=>'pengurus-proses-ppob','uses'=>'Pengurus\DataDiriController@prosesppob']);
Route::get('/pengurus/data-transaksi-ppob', ['as'=>'pengurus-data-transaksi-ppob','uses'=>'Pengurus\DataDiriController@datappob']);
Route::post('/pengurus/data-transaksi-ppob', ['as'=>'pengurus-data-transaksi-ppob','uses'=>'Pengurus\DataDiriController@datappob']);
Route::get('/pengurus/laporan-penjualan-ppob', ['as'=>'pengurus-laporan-ppob','uses'=>'Pengurus\PengurusController@laporanppob']);
Route::post('/pengurus/laporan-penjualan-ppob', ['as'=>'pengurus-laporan-ppob','uses'=>'Pengurus\PengurusController@laporanppob']);
Route::get('/pengurus/laporan-barang', ['as'=>'pengurus-laporan-barang','uses'=>'Pengurus\PengurusController@terimabarang']);
Route::post('/pengurus/laporan-barang', ['as'=>'pengurus-laporan-barang','uses'=>'Pengurus\PengurusController@terimabarang']);

//DEPOSIT PENGURUS
Route::get('/pengurus/topup-deposit', ['as'=>'topup-deposit-pengurus','uses'=>'Pengurus\DataDiriController@topup']);
Route::post('/pengurus/topup-deposit', ['as'=>'topup-deposit-pengurus','uses'=>'Pengurus\DataDiriController@topup']);
Route::post('pengurus-bank-select', ['as'=>'pengurus-bank-select','uses'=>'Pengurus\DataDiriController@bankselect']);
Route::get('/pengurus/buku-saldo-transaksi-anggota', ['as'=>'buku-saldo-transaksi-pengurus','uses'=>'Pengurus\PengurusController@bukusaldoanggota']);
Route::post('/pengurus/buku-saldo-transaksi-anggota', ['as'=>'buku-saldo-transaksi-pengurus','uses'=>'Pengurus\PengurusController@bukusaldoanggota']);
Route::get('/pengurus/laporan-deposit', ['as'=>'laporan-deposit-anggota','uses'=>'Pengurus\PengurusController@laporan']);
Route::post('/pengurus/laporan-deposit', ['as'=>'laporan-deposit-anggota','uses'=>'Pengurus\PengurusController@laporan']);

//SUPLAYER
Route::get('/pengurus/harga-ppob', ['as'=>'pengurus-harga-ppob','uses'=>'Pengurus\PengurusController@cekharga']);
Route::post('/pengurus/harga-ppob', ['as'=>'pengurus-harga-ppob','uses'=>'Pengurus\PengurusController@cekharga']);
Route::get('/pengurus/produk', ['as'=>'pengurus-harga-barang','uses'=>'Pengurus\PengurusController@produk']);
Route::post('/pengurus/produk', ['as'=>'pengurus-harga-barang','uses'=>'Pengurus\PengurusController@produk']);
Route::get('/pengurus/laporan-topup-suplayer', ['as'=>'laporan-topup-suplayer','uses'=>'Pengurus\PengurusController@topupsuplayer']);
Route::post('/pengurus/laporan-topup-suplayer', ['as'=>'laporan-topup-suplayer','uses'=>'Pengurus\PengurusController@topupsuplayer']);
Route::get('/pengurus/buku-saldo-suplayer', ['as'=>'pengurus-buku-saldo-suplayer','uses'=>'Pengurus\PengurusController@bukusaldosuplayer']);
Route::post('/pengurus/buku-saldo-suplayer', ['as'=>'pengurus-buku-saldo-suplayer','uses'=>'Pengurus\PengurusController@bukusaldosuplayer']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//ROUTE KARYAWAN
Route::get('/karyawan/home', 'Karyawan\KaryawanController@homekaryawan');
Route::get('/karyawan/data-anggota', ['as'=>'data-anggota','uses'=>'Karyawan\KaryawanController@anggota']);
Route::post('/karyawan/data-anggota', ['as'=>'data-anggota','uses'=>'Karyawan\KaryawanController@anggota']);
Route::get('/karyawan/ganti-password', 'Karyawan\KaryawanController@password');
Route::post('/karyawan/ganti-password', 'Karyawan\KaryawanController@gantipassword');
Route::get('/karyawan/akumulasi-belanja', 'Karyawan\KaryawanController@akumulasikaryawan');
Route::post('/karyawan/akumulasi-belanja', 'Karyawan\KaryawanController@akumulasikaryawan');
Route::get('/karyawan/profil', 'Karyawan\KaryawanController@profil');
Route::post('/karyawan/profil', 'Karyawan\KaryawanController@profil');
//CALL BACK KOPERASI

//ROUTE PEMEGANG SAHAM
Route::get('/pemegang-saham/home', 'PemegangSaham\PemegangSahamController@homesaham');
Route::get('/pemegang-saham/data-penjualan-saham', ['as'=>'saham-penjualan-saham','uses'=>'PemegangSaham\PemegangSahamController@datajualsaham']);
Route::post('/pemegang-saham/data-penjualan-saham', ['as'=>'saham-penjualan-saham','uses'=>'PemegangSaham\PemegangSahamController@datajualsaham']);
Route::get('/pemegang-saham/data-pembelian-saham', ['as'=>'saham-pembelian-saham','uses'=>'PemegangSaham\PemegangSahamController@databelisaham']);
Route::post('/pemegang-saham/data-pembelian-saham', ['as'=>'saham-pembelian-saham','uses'=>'PemegangSaham\PemegangSahamController@databelisaham']);
Route::get('/pemegang-saham/profil', ['as'=>'pemegang-saham-profil','uses'=>'PemegangSaham\PemegangSahamController@profil']);
Route::post('/pemegang-saham/profil', ['as'=>'pemegang-saham-profil','uses'=>'PemegangSaham\PemegangSahamController@profil']);
Route::get('/pemegang-saham/ganti-password', ['as'=>'pemegang-saham-ganti-password','uses'=>'PemegangSaham\PemegangSahamController@password']);
Route::post('/pemegang-saham/ganti-password', ['as'=>'pemegang-saham-ganti-password','uses'=>'PemegangSaham\PemegangSahamController@gantipassword']);
