@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Pembayaran</h4>
</div>

</div>
<div class="row">
<div class="col-md-6">
  <div class="profile-widget">
    <div class="profile-imges">
      <img src="{{url('laravel/public/gambars/'.$produk->tokoId->gambar)}}" width="100%">
    </div>

    <br>
    <div class="profile-imges">
      <h4>Detail Barang</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
    </div><br>
    <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
    <h6 class="user-name m-t-10 m-b-0 text-left">Nama : {{$produk->tokoId->name}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Harga : Rp {{number_format($produk->tokoId->harga,0,".",",")}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->tokoId->berat}} Gram / {{$produk->tokoId->berat/1000}} Kg</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->tokoId->stok}}</h6>
    <div class="small text-muted"></div>
  </div>
</div>
<div class="col-md-6">
  <div class="profile-widget">
    <div class="profile-imges">
      <h4>Riwayat Pemesanan</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
    </div>
    <br>
    <table class="table table-striped custom-table datatable">
      <tr>
        <td align="left">#Invoice & Berita Transfer</td>
        <td align="left">: {{$produk->no_pemesanan}}</td>
        @csrf
        <input type="hidden" name="no_pesanan" value="{{$produk->no_pemesanan}}" id="no_pesanan">
      </tr>
      <tr>
        <td align="left">Kode</td>
        <td align="left">: {{$produk->kode}}</td>
      </tr>
      <tr>
        <td align="left">Jumlah Pesan</td>
        <td align="left">: {{$produk->jumlah}}</td>
      </tr>
      <tr>
        <td align="left">Total Harga</td>
        <td align="left">: Rp. {{number_format($produk->tokoId->harga*$produk->jumlah)}}</td>
      </tr>
      <tr>
        <td align="left">Total Ongkir</td>
        <td align="left">: Rp. {{number_format($produk->ongkir)}}</td>
      </tr>
      <tr>
        <td align="left">Kode Unik</td>
        <td align="left">: {{$produk->kode_unik}}</td>
      </tr>
      <tr>
        <td align="left"><strong>Total Bayar/Transfer</strong></td>
        <td align="left"><strong>: Rp. {{number_format($produk->total_harga)}}</strong></td>
      </tr>
    </table>
    <hr>
    <div class="profile-imges">
      <h4>Info Rekening</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
    </div><br>
    <p style="text-align:justify;">Silahkan melakukan transfer melalui rekening di bawah ini : </p>
    <?php $banks = App\Bank::where('id',$produk->bank_id);?>
    <p><h6 class="user-name m-t-10 m-b-0 text-left"><img src="{{url('images/'.$banks->logo)}}" alt=""/> {{$banks->bank}} : {{$banks->no_rekening}} An. {{$banks->atas_nama}}</h6></p>
    <div class="small text-muted"></div><br><br>
    <p style="text-align:justify;">Untuk metode pembayaran via bank transfer. Anda dapat melakukan pembayaran via ATM, Internet banking atau Mobile banking. Pembayaran harus dilakukan dalam waktu 24 jam atau pesanan akan dibatalkan. Pesanan akan diproses setelah Anda melakukan pembayaran.</p>
    <hr>
    <div class="" id="errors"></div>
    <script>
      $("#errors").hide();
    </script>
    <button type="button" name="button" class="btn btn-primary" id="bayar-saldo">BAYAR DENGAN SALDO</button>
    <script type="text/javascript">
      $("#bayar-saldo").on("click", function(){
        $('.modal').modal('show');
          var no_pesanan = $("#no_pesanan").val();
          var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('anggota-bayar-dgn-saldo') ?>",
              method: 'POST',
              data: {_token:token,no_pesanan:no_pesanan},
              success: function(data) {
                console.log(data);
                $('.modal').modal('hide');
                if (data.code == '200') {
                  $("#errors").show();
                  $("#errors").html(data.messages);
                  alert('Transaksi Berhasil');
                  window.location = "<?php echo route('anggota-data-transaksi-belanja') ?>";
                }else {
                  // $("#message").html("");
                  $("#errors").show();
                  $("#errors").html(data.messages);
                  setTimeout(function () {
                    $('#errors').modal('hide');
                    $('#errors').html('');
                  }, 5000);
                }
              }
          });
      });
    </script>
  </div>
  <!-- MODAL LOADING -->
  <style>
  .bd-example-modal-lg .modal-dialog{
    display: table;
    position: relative;
    margin: 0 auto;
    text-align:center;
    top: calc(50% - 24px);
  }
  .bd-example-modal-lg .modal-dialog .modal-content{
    background-color: #ffffff;
    border: none;
  }
  </style>
  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...
          </div>

      </div>
  </div>
  <!-- MODAL -->
</div>
</div>
@endsection
