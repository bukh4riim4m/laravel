@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Mutasi Simpanan</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('anggota-mutasi-simpanan')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Dari Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="dari">
          @foreach($tahuns as $thn)
            @if($from == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Sampai Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="sampai">
          @foreach($tahuns as $thn)
            @if($until == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">
<div class="card-box m-b-0">
  <div class="row">
    <div class="col-md-12">
      <div class="profile-view">
        <div class="profile-img-wrap">
          <div class="profile-img">
            <a href=""><img class="avatar" src="{{ asset('foto/'.$users->fotodiri) }}" alt=""></a>
          </div>
        </div>
        <div class="profile-basic">
          <div class="row">
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <td>Nomor Anggota</td>
                    <td>: @if($users->no_anggota==null) @else {{$users->no_anggota}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    <td>: @if($users->name==null) @else {{$users->name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>: @if($users->email==null) @else {{$users->email}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nomor Telpon</td>
                    <td>: @if($users->telp==null) @else {{$users->telp}} @endif</td>
                  </tr>
                  <tr>
                    <td>No. KTP</td>
                    <td>: @if($users->npwp==null) @else {{$users->nik}} @endif</td>
                  </tr>
                  <!-- <tr>
                    <td>Tempat Lahir	</td>
                    <td>: @if($users->tpt_lahir==null) @else {{$users->tpt_lahir}} @endif</td>
                  </tr> -->

                  <tr>
                    <td><strong>Periode	</strong></td>
                    <td><strong>: @if($from==$until) {{$from}}@else {{$from}} - {{$until}} @endif</strong></td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">

                  <tr>
                    <td>Tanggal Lahir	</td>
                    <td>: @if($users->tgl_lahir==null) @else {{date('d-m-Y', strtotime($users->tgl_lahir))}} @endif</td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin	</td>
                    <td>: @if($users->jenkel==null) @else {{$users->jenkel}} @endif</td>
                  </tr>
                  <tr>
                    <td>Propinsi	<span class="pull-right">:</span></td>
                    <td>@if($users->propinsi==null) @else {{$users->propinsi_id->province}} @endif</td>
                  </tr>
                  <tr>
                    <td>Kabupaten	<span class="pull-right">:</span></td>
                    <td>@if($users->propinsi==null) @else {{$users->kabupaten_id->city_name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>: @if($users->alamat==null) @else {{$users->alamat}} @endif</td>
                  </tr>
                  <!-- <tr>
                    <td>Komunitas</td>
                    <td>: @if($users->komunitas==null) @else {{$users->komunitas_id->name}} @endif</td>
                  </tr> -->
                  <tr>
                    <td><strong>TOTAL SIMPANAN</strong></td>
                    <td><strong>: Rp {{number_format($users->saldo,0,".",",")}}</strong></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="card-box tab-box">
</div>
    <div id="tasks" class="tab-pane fade in active">
      <div class="project-task">
        <div class="tabbable">
          <ul class="nav nav-tabs nav-tabs-top nav-justified m-b-0">
            <li class="active"><a href="#pokok" data-toggle="tab" aria-expanded="true">SIMPANAN POKOK</a></li>
              <li><a href="#wajib" data-toggle="tab" aria-expanded="false">SIMPANAN WAJIB</a></li>
              <li><a href="#sukarela" data-toggle="tab" aria-expanded="false">SIMPANAN SUKARELA</a></li>
              <li><a href="#investasi" data-toggle="tab" aria-expanded="false">SIMPANAN KHUSUS</a></li>
              <!-- <li><a href="#wakaf" data-toggle="tab" aria-expanded="false">Wakaf</a></li>
              <li><a href="#infaq" data-toggle="tab" aria-expanded="false">Infaq</a></li>
              <li><a href="#shu" data-toggle="tab" aria-expanded="false">SHU</a></li>
              <li><a href="#lain" data-toggle="tab" aria-expanded="false">Lain-Lain</a></li> -->
            </ul>
          <div class="tab-content">
          <div class="tab-pane active" id="pokok">
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal Setor</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1;
                          $masuk=0;$keluar=0;$saldos=0;$saldosawal=0;$saldosakhir=0;$maxs=array();$maks=array();
                          ?>
                          @foreach($simpananpokok as $key => $pokokmax)
                            <?php $maks[]= $pokokmax->id; ?>
                          @endforeach
                          @if(count($simpananpokok) > 0)
                          <?php $id_maks = max($maks); ?>
                          @endif
                          @foreach($simpananpokok as $key => $pokok)
                          <?php if ($pokok->mutasi =='Kredit') {
                              $masuk+=$pokok->nominal;
                              if ($key ==0) {
                                  $saldosawal = $pokok->saldo - $pokok->nominal;
                              }
                          } else {
                              $keluar+=$pokok->nominal;
                              if ($key ==0) {
                                  $saldosawal = $pokok->saldo + $pokok->nominal;
                              }
                          };
                          $maxs[]=$pokok->id;
                          $jum_id = max($maxs);
                          $saldosakhir = $pokok->saldo;?>
                          <tr>
                            <td>{{$no++}}.</td>
                            <td>{{date('d-m-Y', strtotime($pokok->tgl_setor))}}</td>
                            <td>{{$pokok->no_trx}}</td>
                            @if($pokok->mutasi =='Debet')
                            <td>Rp {{number_format($pokok->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($pokok->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks == $pokok->id)<strong><font color="green">Rp {{number_format($pokok->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($pokok->saldo,0,".",",")}} @endif</td>
                          </tr>
                          @endforeach
                          @if(count($simpananpokok) < 1)
                          <tr>
                            <td colspan="6" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="wajib">
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal Setor</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no2=1;
                          $masuk2=0;$keluar2=0;$saldos2=0;$saldosawal2=0;$saldosakhir2=0;$maxs2=array();$maks2=array();
                          ?>
                          @foreach($simpananwajib as $key => $wajibmaks)
                            <?php $maks2[]= $wajibmaks->id; ?>
                          @endforeach
                          @if(count($simpananwajib) > 0)
                          <?php $id_maks2 = max($maks2); ?>
                          @endif
                          @foreach($simpananwajib as $key => $wajib)
                          <?php if ($wajib->mutasi =='Kredit') {
                              $masuk2+=$wajib->nominal;
                              if ($key ==0) {
                                  $saldosawal2 = $wajib->saldo - $wajib->nominal;
                              }
                          } else {
                              $keluar2+=$wajib->nominal;
                              if ($key ==0) {
                                  $saldosawal2 = $wajib->saldo + $wajib->nominal;
                              }
                          };

                          $saldosakhir2 = $wajib->saldo;?>
                          <tr>
                            <td>{{$no2++}}.</td>
                            <td>{{date('d-m-Y', strtotime($wajib->tgl_setor))}}</td>
                            <td>{{$wajib->no_trx}}</td>
                            @if($wajib->mutasi =='Debet')
                            <td>Rp {{number_format($wajib->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($wajib->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks2 == $wajib->id)<strong><font color="green">Rp {{number_format($wajib->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($wajib->saldo,0,".",",")}} @endif</td>
                          </tr>
                          @endforeach
                          @if(count($simpananwajib) < 1)
                          <tr>
                            <td colspan="6" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                </div>
              </div>
            </div>
            <div class="row col-md-6 table-responsive">
              <table class="table table-striped custom-table" width="40px">
                <thead>
                  <tr>
                    <th><strong>Saldo Awal</strong></th>
                    <th><strong>: Rp {{number_format($saldosawal2,0,".",",")}}</strong></th>
                  </tr>
                  <tr>
                    <th>Total Kredit</th>
                    <th>: Rp {{number_format($masuk2,0,".",",")}}</th>
                  </tr>
                  <tr>
                    <th>Total Debet</th>
                    <th>: Rp {{number_format($keluar2,0,".",",")}}</th>
                  </tr>
                  <tr>
                    <th><strong><font color="green">Saldo Akhir</font></strong></th>
                    <th><strong><font color="green">: Rp {{number_format($saldosakhir2,0,".",",")}}</font></strong></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="tab-pane" id="sukarela">
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal Setor</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no3=1;
                          $masuk3=0;$keluar3=0;$saldos3=0;$saldosawal3=0;$saldosakhir3=0;$maxs3=array();$maks3=array();
                          ?>
                          @foreach($simpanansukarela as $key => $sukarelamaks)
                            <?php $maks3[]= $sukarelamaks->id; ?>
                          @endforeach
                          @if(count($simpanansukarela) > 0)
                          <?php $id_maks3 = max($maks3); ?>
                          @endif
                          @foreach($simpanansukarela as $key => $sukarela)
                          <?php if ($sukarela->mutasi =='Kredit') {
                              $masuk3+=$sukarela->nominal;
                              if ($key ==0) {
                                  $saldosawal3 = $sukarela->saldo - $sukarela->nominal;
                              }
                          } else {
                              $keluar3+=$sukarela->nominal;
                              if ($key ==0) {
                                  $saldosawal3 = $sukarela->saldo + $sukarela->nominal;
                              }
                          };

                          $saldosakhir3 = $sukarela->saldo;?>
                          <tr>
                            <td>{{$no3++}}.</td>
                            <td>{{date('d-m-Y', strtotime($sukarela->tgl_setor))}}</td>
                            <td>{{$sukarela->no_trx}}</td>
                            @if($sukarela->mutasi =='Debet')
                            <td>Rp {{number_format($sukarela->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($sukarela->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks3 == $sukarela->id)<strong><font color="green">Rp {{number_format($sukarela->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($sukarela->saldo,0,".",",")}} @endif</td>
                          </tr>
                          @endforeach
                          @if(count($simpanansukarela) < 1)
                          <tr>
                            <td colspan="6" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal3,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk3,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar3,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir3,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="investasi">
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal Setor</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no4=1;
                          $masuk4=0;$keluar4=0;$saldos4=0;$saldosawal4=0;$saldosakhir4=0;$maxs4=array();$maks4=array();
                          ?>
                          @foreach($simpananinvestasi as $key => $investasimaks)
                            <?php $maks4[]= $investasimaks->id; ?>
                          @endforeach
                          @if(count($simpananinvestasi) > 0)
                          <?php $id_maks4 = max($maks4); ?>
                          @endif
                          @foreach($simpananinvestasi as $key => $investasi)
                          <?php if ($investasi->mutasi =='Kredit') {
                              $masuk4+=$investasi->nominal;
                              if ($key ==0) {
                                  $saldosawal4 = $investasi->saldo - $investasi->nominal;
                              }
                          } else {
                              $keluar4+=$investasi->nominal;
                              if ($key ==0) {
                                  $saldosawal4 = $investasi->saldo + $investasi->nominal;
                              }
                          };

                          $saldosakhir4 = $investasi->saldo;?>
                          <tr>
                            <td>{{$no4++}}.</td>
                            <td>{{date('d-m-Y', strtotime($investasi->tgl_setor))}}</td>
                            <td>{{$investasi->no_trx}}</td>
                            @if($investasi->mutasi =='Debet')
                            <td>Rp {{number_format($investasi->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($investasi->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks4 == $investasi->id)<strong><font color="green">Rp {{number_format($investasi->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($investasi->saldo,0,".",",")}} @endif</td>
                          </tr>
                          @endforeach
                          @if(count($simpananinvestasi) < 1)
                          <tr>
                            <td colspan="6" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal4,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk4,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar4,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir4,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>



          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>
</div>
@endsection
