@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">History Pembayaran Pinjaman</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('anggota-data-pinjaman')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="history" required>
    <div class="col-sm-4">
      <div class="form-group form-focus">
        <label class="control-label">No. Pinjaman</label>
        <input type="text" class="form-control floating" name="no_pinjam" value="{{$pinjamans->no_pinjam}}"/>
      </div>
    </div>

    <div class="col-sm-4">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="C A R I"/>
    </div>
    <div class="col-sm-4 col-xs-12"><br>

                </div>
  </form>


</div>
<div class="row">
  <div class="col-md-8">
    <div class="table-responsive">
      <table class="table table-striped custom-table">
        <thead>
          <tr>
            <th>Nomor Anggota</th>
            <th>: {{$pinjamans->no_anggota}}</th>
          </tr>
          <tr>
            <th>Nama Anggota</th>
            <th>: {{$users->name}}</th>
          </tr>
          <tr>
            <th>Nomor Pinjam</th>
            <th>: {{$pinjamans->no_pinjam}}</th>
          </tr>
          <tr>
            <th>Total Pinjam</th>
            <th>: Rp {{number_format($pinjamans->total_pinjam,0,".",",")}}</th>
          </tr>
          <tr>
            <th>Angsuran / Bulan</th>
            <th>: Rp {{number_format($pinjamans->total_pinjam/$pinjamans->kali_angsuran,0,".",",")}}  Selama {{$pinjamans->kali_angsuran}} Kali</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <div class="col-md-4">
    <form class="form" action="{{route('anggota-data-pinjaman')}}" method="post" id="exportdata">
      @csrf
      <input type="hidden" name="action" value="downloadpdf">
      <input type="hidden" name="no_pinjam" value="{{$pinjamans->no_pinjam}}">
    </form>
    <a href="#" class="pull-left"><img width="40px" src="{{asset('/images/hasil.png')}}" onclick="event.preventDefault();
                  document.getElementById('exportdata').submit();"/></a>
  </div>
</div>
<div class="row">
  @if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  <!-- <form class="" action="{{url('/administrator/export-data-anggota')}}" method="post" id="exportdata">
    @csrf
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No. Transaksi</th>
          <th>Jatuh Tempo</th>
          <th>Tgl. Bayar</th>
          <th>Pokok</th>
          <th>Bunga</th>
          <th>Angsuran/Bulan <br>(Pokok + Bunga)</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($historys as $key => $history)
        <tr>
          <td>{{$key+1}}.</td>
          <td>{{$history->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($history->tgl_tempo))}}</td>
          <td>@if($history->tgl_bayar==null)xx-xx-xxxx @else {{date('d-m-Y', strtotime($history->tgl_bayar))}} @endif</td>
          <td>Rp {{number_format($history->pokok,0,".",",")}}</td>
          <td>Rp {{number_format($history->bunga,0,".",",")}}</td>
          <td>Rp {{number_format($history->bayar,0,".",",")}}</td>
          <td>
              @if($history->status==1)
              <button class="btn btn-white btn-sm rounded dropdown-toggle" style="cursor: not-allowed;"><i class="fa fa-dot-circle-o text-warning"></i> Belum Bayar</button>
              @else
              <button class="btn btn-white btn-sm rounded dropdown-toggle" style="cursor: not-allowed;"><i class="fa fa-dot-circle-o text-success"></i> Sudah Bayar</button>
              @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="row col-md-6 table-responsive">
    <table class="table table-striped custom-table" width="40px">
      <thead>
        <tr>
          <th>Sudah Terbayar</th>
          <th>: Rp {{number_format($terbayar,2,".",",")}}</th>
        </tr>
        <tr>
          <th>Sisa Pinjaman</th>
          <th>: Rp {{number_format($pinjamans->total_pinjam-$terbayar,2,".",",")}}</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
</div>
    </div>
    </div>



      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
