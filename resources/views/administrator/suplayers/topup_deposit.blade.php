@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-4">
          <h4 class="page-title">TOPUP SUPLAYER</h4>
        </div>
        <div class="col-xs-8 text-right m-b-30">
          <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#panduan"></i>PANDUAN TOPUP</a> -->
          <div class="view-icons">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-md-6 col-xs-12">
          <form class="" action="{{url('/administrator/topup-suplayer')}}" method="post">
            <input type="hidden" name="action" value="topup">
            @csrf
          <div class="form-group">
            <select class="form-control" name="bank" id="bank" required>
              <option value="">Pilih Bank Tujuan</option>
              <?php $banks = [
                'MANDIRI','BCA','BRI'
              ]; ?>
              @foreach($banks as $bank)
                <option value="{{$bank}}">{{$bank}}</option>
              @endforeach
            </select>
          </div>
          <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
          angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
          }
          function rupiah(){ var nominal = document.getElementById("nominal").value; var rupiah = convertToRupiah(nominal);
          document.getElementById("nominal").value = rupiah; }
          </script>
          <script> function convertToRupiah (objek) {
           separator = ",";
           a = objek.value;
           b = a.replace(/[^\d]/g,"");
           c = "";
           panjang = b.length;
           j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
           c = b.substr(i-1,1) + separator + c; } else {
           c = b.substr(i-1,1) + c; } } objek.value = c; }
           </script>
          <div class="form-group">
              <input type="text" name="nominal" class="form-control" value="" onkeyup="convertToRupiah(this)" placeholder="Nominal Transfer" id="nominal" required>
          </div>
          <div class="form-group" id="buttom">
            <input type="submit" class="form-control btn btn-primary" name="btn" value="P R O S E S">
          </div>
        </form>

        </div>
        <div class="col-sm-8 col-md-6 col-xs-12">
          <p><h4>PENJELASAN FORM</h4></p>
          <p style="text-align:justify;">
            <strong>Bank</strong><br>
            Silakan pilih bank tujuan transfer.<br>
            <strong>Nominal</strong><br>
            Ketik nominal yang ingin Anda deposit minimal Rp 500.000 dan Nominal ini harus kelipatan 1000, contoh: 500000, 501000, 502000, 505000, 510000, 550000, 600000, 1000000, dst<br>
            <strong>Klik 'PROSES'</strong><br>
            Setelah diisi semua, klik 'PROSES'. Perhatikan pesan yang muncul.
            Anda HARUS mentransfer sesuai nominal yang kami sebutkan, agar sistem dapat mengenali dana transfer Anda.<br>
            Maksimal transfer dana adalah 1x24jam dari tanggal request. Jika melebihi batas waktu dan belum transfer, Maka Anda harus request deposit ulang.
            <br>Dana akan masuk ke saldo Anda maksimal 15 menit setelah dana kami terima.
          </p>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-8 col-md-12 col-xs-12">
        <div class="row filter-row">
          <form class="" action="{{url('/administrator/topup-suplayer')}}" method="post">
            @csrf
            <input type="hidden" name="action" value="cari">
            <div class="col-sm-3 col-xs-6">
              <div class="form-group form-focus">
                <label class="control-label">Dari</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$dari}}" required></div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-6">
              <div class="form-group form-focus">
                <label class="control-label">Sampai</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="until" value="{{$sampai}}" required></div>
              </div>
            </div>
            <div class="col-sm-3 col-md-3 col-xs-12">
              <div class="form-group form-focus select-focus">
                <label class="control-label">Status</label>
                <?php $statuses = [
                  'Menunggu','Berhasil','Batal'
                ]; ?>
                <select class="select floating" name="status">
                  <option value=""> -- Semua -- </option>
                  @foreach($statuses as $stts)
                    @if($stts == $status)
                    <option value="{{$stts}}" selected> {{$stts}} </option>
                    @else
                    <option value="{{$stts}}"> {{$stts}} </option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-8 col-md-12 col-xs-12">
      <div class="row">
        <div class="table-responsive">
          <table class="table table-striped custom-table">
            <thead>
            <tr>
              <th>No.</th>
              <th>Tgl Transaksi</th>
              <th>No. Transaksi</th>
              <th>Bank</th>
              <th>Nominal Request</th>
              <th>Nominal Transfer</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; ?>
            @foreach($hasil as $hsl)
            <tr>
              <td>{{$no++}}.</td>
              <td>{{$hsl['tgl_trx']}}</td>
              <td>{{$hsl['no_trx']}}</td>
              <td>{{$hsl['bank']}}</td>
              <td>Rp {{number_format($hsl['nominal'],0,".",",")}}</td>
              <td>Rp {{number_format($hsl['transfer'],0,".",",")}}</td>
              <td>@if($hsl['status'] =='Menunggu')<span class="label label-warning-border">{{$hsl['status']}}</span>@elseif($hsl['status'] =='Berhasil')<span class="label label-success-border">{{$hsl['status']}}</span> @else <span class="label label-danger-border">{{$hsl['status']}}</span> @endif </td>
              <td> <a href="#" name="button" class="btn btn-primary" data-toggle="modal" data-target="#detail{{$hsl['id']}}">Keterangan</a> </td>
            </tr>
            @endforeach
          </tbody>
          </table>
        </div>
      </div>
    </div>
    <div id="panduan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title"> PANDUAN TOPUP DEPOSIT</h4>
          </div>
            @csrf
            <div class="modal-body card-box">
              <table width="100%" class="table table-striped">
                <tr>
                  <td>
                    <p>
                      <strong>Bank</strong><br>
                      Silakan pilih bank tujuan transfer.<br>
                      <strong>Nominal</strong><br>
                      Ketik nominal yang ingin Anda deposit. Nominal ini harus kelipatan 1000, contoh: 10000, 11000, 16000, 20000, 100000, 500000, 510000, dst<br>
                      <strong>Klik 'Submit'</strong><br>
                      Setelah diisi semua, klik 'Submit'. Perhatikan pesan yang muncul.
                      Anda HARUS mentransfer sesuai nominal yang kami sebutkan, agar sistem dapat mengenali dana transfer Anda.<br>
                      Maksimal transfer dana adalah 1x24jam dari tanggal request. Jika melebihi batas waktu dan belum transfer, Maka Anda harus request deposit ulang.
                      <br>Dana akan masuk ke saldo Anda maksimal 15 menit setelah dana kami terima.
                    </p>
                  </td>
                </tr>
              </table>
              <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
              </div>
            </div>
        </div>
      </div>
    </div>
    @foreach($hasil as $detail)
    <div id="detail{{$detail['id']}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title"> Keterangan Transfer</h4>
          </div>
            @csrf
            <div class="modal-body card-box">
              <table width="100%" class="table table-striped">
                <tr>
                  <td>
                    <p>
                      {{$detail['ket']}}
                    </p>
                  </td>
                </tr>
              </table>
              <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
              </div>
            </div>
        </div>
      </div>
    </div>
    @endforeach
</div>
@endsection
