@extends('layouts.admin.appajax')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
@include('flash::message')
<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data Anggota</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/administrator/data-anggota')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="name" value="{{$name}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="C A R I"/>
    </div>
    <div class="col-sm-4 col-xs-12"><br>
    <!-- <a href="#" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                  document.getElementById('exportdata').submit();"/></a> -->
                </div>
  </form>


</div>
<div class="row">
  <!-- <form class="" action="{{url('/administrator/export-data-anggota')}}" method="post" id="exportdata">
    @csrf
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th></th>
          <th>Tgl. Daftar</th>
          <th>No.Anggota</th>
          <th>Nama</th>
          <th>No. Telp</th>
          <th>Total Simpanan</th>
          <th style="min-width:100px;" class="text-right">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td><img class="avatar" src="{{ asset('foto/'.$us->fotodiri) }}" alt=""></td>
          <td>{{date('d-m-Y', strtotime($us->tgl_daftar))}}</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td>{{$us->telp}}</td>
          <td>Rp {{number_format($us->saldo,0,".",",")}}</td>
          <td style="max-width:120px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anggota{{$us->id}}" id="editanggota{{$us->id}}">Edit</a>
            <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#detail{{$us->id}}">Detail</a>
            <a href="#" class="btn btn-danger btn-sm rounded" data-toggle="modal" data-target="#hapus{{$us->id}}">Hapus</a>
          </td>
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>

    </table>{!! $user->render() !!}


    <!-- <label class="col-lg-3 control-label">Light Logo</label> -->
    <!-- <form class="" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
      @csrf
      <input type="hidden" name="action" value="csv">
  		<div class="col-lg-3"><br>
        <span class="control-label">UPLOAD CSV</span>
          <input class="form-control rounded" type="file" name="upload">
  		</div>
      <div class="col-lg-9"><br>
  		</div>
      <div class="col-lg-2"><br>
          <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
  		</div>
    </form> -->
  </div>
  <div class="row col-md-6 table-responsive">
    <table class="table table-striped custom-table" width="40px">
      <thead>
        <tr>
          <th>Total Seluruh Simpanan</th>
          <th>: Rp {{number_format($totalsimpanan,0,".",",")}}</th>
        </tr>
        <!-- <tr>
          <th>Total Saldo Transaksi</th>
          <th>: Rp {{number_format($totalsaldotransaksi,0,".",",")}}</th>
        </tr> -->
      </thead>
    </table>
  </div>
</div>
</div>
    </div>


    <div id="add_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Anggota</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="action" value="tambah">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                    <?php
                    if ($noanggota = App\User::orderBy('no_anggota', 'DESC')->where('type', 'anggota')->first()) {

                      $nomor = (int)$noanggota->no_anggota+1;
                      $sequence = substr($nomor, -6);
                      $anggota = date('Ym').$sequence;
                    }else {
                      $anggota = date('Ym000001');
                    }
                    ?>
                    <input type="hidden" name="no_anggota" value="{{$anggota}}">
                    <label class="form-control" type="text" name="no_anggota" value="" disabled>{{$anggota}}</label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KTP<span class="text-danger">*</span></label>
                    <input class="form-control" type="number" name="nik" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email </label>
                    <input class="form-control" type="email" name="email">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="number" name="telp" required>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NPWP</label>
                    <input class="form-control" type="number" name="npwp">
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <select class="select form-control" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                      <option value="Laki-laki">LAKI-LAKI</option>
                      <option value="Perempuan">PEREMPUAN</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pekerjaan<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="kerjaan" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Diri 3 x 4</label>
                    <input class="form-control" type="file" name="fotodiri" style="margin-top:7px;">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto KTP (Pas Ukuran KTP)<span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotoktp" style="margin-top:7px;" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
                  <div class="form-group">
                    <label class="control-label">Propinsi  </label>
                    <select class="select form-control" name="propinsi" id="propinsis" required>
                      <option value="">Pilih Propinsi</option>
                      <?php $propinsi = App\Province::orderBy('id', 'ASC')->get(); ?>
                      @foreach($propinsi as $pro)
                      <option value="{{$pro->province_id}}">{{$pro->province}}</option>
                      @endforeach
                    </select>
                    <script type="text/javascript">
                    console.log('masuk javascript');
                      $("select[id='propinsis']").change(function(){
                        console.log('klik javascript');
                        var idcari = $('#propinsis').val();
                        var token = $("input[name='_token']").val();
                        // alert(idcari);
                          $.ajax({
                              url: "<?php echo route('admin-select-propinsi') ?>",
                              method: 'POST',
                              data: {idcari:idcari, _token:token},
                              success: function(data) {
                                console.log(data);
                                $("#kabupatenadd").html("");
                                $("#kabupatenadd").append(data);
                                // $("#divrekening").show();
                              }
                          });
                      });
                    </script>
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kabupaten  </label>
                    <select class="select form-control" name="kabupaten" id="kabupatenadd" required>
                    </select>
                  </div>
                </div>

                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Alamat Lengkap <span class="text-danger">*</span></label>
                      <textarea name="alamat" rows="4" cols="80" class="form-control" required></textarea>
                    </div>
                  </div>
                </div>



              </div>

              <div class="m-t-20 text-center">
                <button class="btn btn-primary">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    @foreach($user as $use)
    <div id="detail{{$use->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Detail Data Anggota</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-2">
          <img src="{{asset('/foto/'.$use->fotodiri)}}" width="100%"/> <img src="{{asset('/fotoktp/'.$use->fotoktp)}}" width="100%"/>
        </div>
        <div class="col-md-10">
          <table class="table" width="100%" style="background:white;">

            <tr>
              <td>Nomor Anggota</td>
              <td>: {{$use->no_anggota}}</td>
            </tr>
            <!-- <tr>
              <td>Nomor KS212</td>
              <td>: {{$use->no_ks212}}</td>
            </tr> -->
            <tr>
              <td>Nama Lengkap</td>
              <td>: {{$use->name}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>: {{$use->email}}</td>
            </tr>
            <tr>
              <td>Nomor Telpon</td>
              <td>: {{$use->telp}}</td>
            </tr>
            <tr>
              <td>No.NPWP</td>
              <td>: {{$use->npwp}}</td>
            </tr>

            <tr>
              <td>Propinsi </td>
              <td>: @if($use->propinsi > 0){{ $use->propinsi_id->province }} @else @endif </td>
            </tr>
            <tr>
              <td>Kabupaten/Kota</td>
              <td>: @if($use->kabupaten > 0){{ $use->kabupaten_id->city_name }} @else  @endif</td>
            </tr>

            <tr>
              <td>Alamat</td>
              <td>: {{$use->alamat}}</td>
            </tr>
            <tr>
              <td><strong>SALDO SIMPANAN</strong> </td>
              <td><strong>: Rp. {{ number_format($use->saldo) }},-</strong></td>
            </tr>

          </table>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- END DETAIL -->

<!-- EDIT -->
    <div id="edit_anggota{{$use->id}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Anggota</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="action" value="edit">
              <input type="hidden" name="ids" value="{{$use->id}}">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No.Anggota<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="no_anggota" value="{{$use->no_anggota}}" disabled>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{{$use->name}}" required>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KTP</label>
                    <input class="form-control" type="text" name="nik" value="{{$use->nik}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email </label>
                    <input class="form-control" type="email" name="email" value="{{$use->email}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon </label>
                    <input class="form-control" type="text" name="telp" value="{{$use->telp}}">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NPWP</label>
                    <input class="form-control" type="text" name="npwp" value="{{$use->npwp}}">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin</label>
                    <select class="select form-control" name="jenkel">
                      <?php $jenkel = ['Laki-Laki','Perempuan'] ;?>
                      <option value="">Pilih Jenis Kelamin</option>
                      @foreach($jenkel as $jen)
                      @if($use->jenkel == $jen)
                      <option value="{{$jen}}" selected>{{$jen}}</option>
                      @else
                      <option value="{{$jen}}">{{$jen}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Saldo Simpanan</label>
                    <input class="form-control" type="text" value="{{$use->saldo}}" name="saldo" disabled>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Alamat Lengkap </label>
                    <textarea class="form-control" name="alamat" rows="6" cols="80">{{$use->alamat}}</textarea>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Diri<span class="text-danger">*</span></label><br>
                    <img src="{{ asset('/foto/'.$use->fotodiri) }}" width="50%">
                    <input class="form-control" type="file" name="fotodiri" style="margin-top:7px;">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto KTP<span class="text-danger">*</span></label><br>
                    <img src="{{ asset('/fotoktp/'.$use->fotoktp) }}" width="100%">
                    <input class="form-control" type="file" name="fotoktp" style="margin-top:7px;">
                  </div>
                </div>

              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary" type="submit">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="hapus{{$use->id}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Anggota</h4>
          </div>
          <form action="{{url('/administrator/data-anggota')}}" method="post">
            @csrf
            <input type="hidden" name="action" value="hapus">
            <input type="hidden" name="ids" value="{{$use->id}}">
            <div class="modal-body card-box">
              <p>Apakah yakin ingin di Hapus : {{$use->name}} ???</p>
              <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-danger">Proses</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach
<!-- END EDIT -->

    <!-- END DELETE -->
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>

@endsection
