<thead>
  <tr>
    <th>No</th>
    <th>No.Anggota</th>
    <th>No. Transaksi</th>
    <th>Tgl. Transaksi</th>
    <th>Paket</th>
    <th>No Hp/Meteran</th>
    <th>Jual</th>
    <th>Modal</th>
    <th>Serial Number</th>
    <th>Status</th>
  </tr>
  <?php $no=1;
  $total=0;?>
  </thead>
  <tbody>
  @foreach($datas as $data)
  <?php $total+= $data->nominal;?>

  <tr>
    <td>{{$no++}}</td>
    <td>{{$data->no_anggota}}</td>
    <td>{{$data->no_trx}}</td>
    <td>{{$data->tgl_trx}}</td>
    <td>{{$data->paket}}</td>
    <td>{{$data->nomorhp}}</td>
    <td>{{$data->nominal}}</td>
    <td>{{$data->nta}}</td>
    <td>{{$data->sn}}</td>
    <td>{{$data->status}}</td>
  </tr>
  @endforeach
</tbody>
