<table>
  <thead>
    <tr>
      <th>No.</th>
      <th>No.Angota</th>
      <th>Nama Anggota</th>
      <th>Setor Terakhir</th>
      <th>Terakhir Bayar</th>
      <th>Tgl. Jatuh Tempo</th>
      <th>Status</th>
      <!-- <th>Status JT</th> -->
    </tr>
  </thead>
  <tbody>
    @foreach($users as $key => $user)
    <tr>
      <td>{{$key+1}}.</td>
        @if($wajib = App\Simpananadmin::where('no_anggota',$user->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->orderBy('id','DESC')->first())
          <td>{{$user->no_anggota}}</td>
          <td>{{$user->name}}</td>
          <td>{{$wajib->nominal}}</td>
          <td>{{date('d-m-Y', strtotime($wajib->tgl_setor))}}</td>
          <td>{{date('d-m-Y', strtotime($wajib->jth_tempo))}}</td>
          <td>
            @if($wajib->jth_tempo > date('Y-m-d')) Sudah Bayar @else  Belum Bayar @endif</td>
        @else
          <td>{{$user->no_anggota}}</td>
          <td>{{$user->name}}</td>
          <td>Belum Pernah</td>
          <td>##-##-####</td>
          <td>##-##-####</td>

          <td>
             Belum Bayar

            <!-- <span class="label label-danger-border">Belum Bayar</span> -->
          </td>
          <!-- <td><span class="label label-warning-border">OK</span> </td> -->
        @endif

    </tr>
    @endforeach
  </tbody>
</table>
