<table>
  <thead>
    <tr>
      <th>No.</th>
      <th>Setor Terakhir</th>
      <th>Tgl. Akhir Bayar</th>
      <th>Tgl. Jatuh Tempo</th>
      <th>Nama Anggota</th>
      <th>No.Angota</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1;?>
    @foreach($user as $us)
    @if($bayar = App\Simpananadmin::where('jenis_simpanan',2)->where('no_anggota',$us->no_anggota)->where('tgl_setor','LIKE','%'.$thn.'-'.$bln.'%')->where('aktif',1)->first())
    @if($stts == 'Semua' || $stts == 'Sudah Bayar')
    <?php $tglawal = App\Simpananadmin::where('jenis_simpanan',2)->where('no_anggota',$us->no_anggota)->where('awal',1)->where('aktif',1)->first(); ?>
    @if(date('m-', strtotime($tglawal->jth_tempo)).$alertbulan == $bln.'-'.$alertbulan)
    <tr>
      <td>{{$no++}}.</td>
      <td>{{$bayar->nominal}}</td>
      <td>{{date('d-m-Y', strtotime($bayar->tgl_setor))}}</td>
      <td>{{date('d-m-', strtotime($tglawal->jth_tempo))}}{{$alertbulan}}</td>
      <td>{{$us->name}}</td>
      <td>{{$bayar->no_anggota}}</td>
      <td>Sudah Bayar</td>
    </tr>
    @endif
    @endif
    @else
    <?php $belum = App\Simpananadmin::where('no_anggota',$us->no_anggota)->where('jenis_simpanan',2)->where('awal',1)->where('aktif',1)->first(); ?>
      @if($stts == 'Semua' || $stts == 'Belum Bayar')
        @if($belum)
          @if(date('m-', strtotime($belum->jth_tempo)).$alertbulan == $bln.'-'.$alertbulan)
          <?php $bayarterakhir = App\Simpananadmin::where('no_anggota',$us->no_anggota)->where('jenis_simpanan',2)->where('mutasi','Kredit')->where('aktif',1)->first(); ?>
          <tr>
            <td>{{$no++}}.</td>
            <td>{{$bayarterakhir->nominal}}</td>
            <td>{{date('d-m-Y', strtotime($bayarterakhir->tgl_setor))}}</td>
            <td>{{date('d-m-', strtotime($belum->jth_tempo))}}{{$alertbulan}}</td>
            <td>{{$us->name}}</td>
            <td>{{$us->no_anggota}}</td>
            <td>Belum Bayar</td>
          </tr>
          @endif
        @endif
      @endif
    @endif
    @endforeach
  </tbody>
</table>
