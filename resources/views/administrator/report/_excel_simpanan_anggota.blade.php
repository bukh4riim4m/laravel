<tbody>
  <tr>
    <th>No</th>
    <th>No.Anggota</th>
    <th>Nama Anggota</th>
    <th>Sim.Pokok</th>
    <th>Sim.Wajib</th>
    <th>Sim.Sukarela</th>
    <th>Sim.Khusus</th>
    <th>Total Simpanan</th>
  </tr>
  <?php $tot_pokok = 0;$tot_wajib=0;$tot_khusus=0;$tot_sukarela=0; ?>
  @foreach($datas as $key => $data)
  <?php $pokok_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',1)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
  $pokok_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',1)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
  $wajib_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',2)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
  $wajib_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',2)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
  $sukarel_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',3)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
  $sukarel_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',3)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
  $khusus_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',4)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
  $khusus_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',4)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
  $tot_pokok+= $pokok_k - $pokok_d;
  $tot_wajib+= $wajib_k - $wajib_d;
  $tot_khusus+= $sukarel_k - $sukarel_d;
  $tot_sukarela+= $khusus_k - $khusus_d;?>
  <tr>
    <td>{{$key+1}}.</td>
    <td>{{substr($data->no_anggota, -6)}}</td>
    <td>{{$data->name}}</td>
    <td>{{$pokok_k - $pokok_d}}</td>
    <td>{{$wajib_k - $wajib_d}}</td>
    <td>{{$sukarel_k - $sukarel_d}}</td>
    <td>{{$khusus_k - $khusus_d}}</td>
    <td>{{$data->saldo}}</td>
  </tr>
  @endforeach
  <tr>
    <td colspan="3">Total</td>
    <td>{{$tot_pokok}}</td>
    <td>{{$tot_wajib}}</td>
    <td>{{$tot_khusus}}</td>
    <td>{{$tot_sukarela}}</td>
    <td>{{$tot_pokok+$tot_wajib+$tot_khusus+$tot_sukarela}}</td>
  </tr>
</tbody>
