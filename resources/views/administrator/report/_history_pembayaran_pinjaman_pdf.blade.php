<center> <u><h2>History Pembayaran Pinjaman</h2></u></center><br>
<table width="50%" style="background:#F4F4F4;border: 1px solid black;">
  <thead>
    <tr>
      <th>Nomor Anggota</th>
      <th>: {{$pinjamans->no_anggota}}</th>
    </tr>
    <tr>
      <th>Nama Anggota</th>
      <th>: {{$users->name}}</th>
    </tr>
    <tr>
      <th>Nomor Pinjam</th>
      <th>: {{$pinjamans->no_pinjam}}</th>
    </tr>
    <tr>
      <th>Total Pinjam</th>
      <th>: Rp {{number_format($pinjamans->total_pinjam,0,".",",")}}</th>
    </tr>
    <tr>
      <th>Angsuran / Bulan</th>
      <th>: Rp {{number_format($pinjamans->total_pinjam/$pinjamans->kali_angsuran,0,".",",")}} Selama {{$pinjamans->kali_angsuran}} Kali</th>
    </tr>
  </thead>
</table><br>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr style="background:#F4F4F4;padding:20;">
      <th>No.</th>
      <th>No. Transaksi</th>
      <th>Jatuh Tempo</th>
      <th>Tgl. Bayar</th>
      <th>Pokok</th>
      <th>Bunga</th>
      <th>Angsuran/Bulan <br>(Pokok + Bunga)</th>
      <th>Status</th>
    </tr>
    @foreach($historys as $key => $history)
    @if($history->status==1)
    <tr style="background:#E5BEC4;padding:20;">
      @else
    <tr style="background:#BEE5C8;padding:20;">
      @endif
      <td>{{$key+1}}.</td>
      <td>{{$history->no_trx}}</td>
      <td>{{date('d-m-Y', strtotime($history->tgl_tempo))}}</td>
      <td>@if($history->tgl_bayar==null)xx-xx-xxxx @else {{date('d-m-Y', strtotime($history->tgl_bayar))}} @endif</td>
      <td>Rp {{number_format($history->pokok,0,".",",")}}</td>
      <td>Rp {{number_format($history->bunga,0,".",",")}}</td>
      <td>Rp {{number_format($history->bayar,0,".",",")}}</td>
      <td>
          @if($history->status==1)
          Belum Bayar
          @else
          Sudah Bayar
          @endif
      </td>
    </tr>

    @endforeach
</table> <br>
<table width="50%" style="background:#F4F4F4;border: 1px solid black;">
  <thead>
    <tr>
      <th>Sudah Terbayar</th>
      <th>: Rp {{number_format($terbayar,0,".",",")}}</th>
    </tr>
    <tr>
      <th>Sisa Pinjaman</th>
      <th>: Rp {{number_format($pinjamans->total_pinjam-$terbayar,0,".",",")}}</th>
    </tr>
  </thead>
</table>
