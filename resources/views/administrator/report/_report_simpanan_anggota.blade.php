<tbody>
  <tr>
    <th>No</th>
    <th>No.Transaksi</th>
    <th>No anggota</th>
    <th>Nama anggota</th>
    <th>Tanggal Setor</th>
    <th>Jenis Simpanan</th>
    <th>KAS / BANK</th>
    <th>Mutasi</th>
    <th>Jumlah Simpanan</th>
    <th>Keterangan</th>
  </tr>
  <?php $no=1;
  $kredit = 0;
  $debet = 0;?>
  @foreach($simpanan as $us)
  <tr>
    <td>{{$no++}}</td>
    <td>{{$us->no_trx}}</td>
    <td>{{$us->no_anggota}}</td>
    <td>{{$us->name}}</td>
    <td>{{$us->tgl_setor}}</td>
    <td>{{$us->jenisSimpanan->name}}</td>
    <td>{{$us->kasbank}}</td>
    <td>{{$us->mutasi}}</td>
    <td>{{$us->nominal}}</td>
    <td>{{$us->ket}}</td>
  </tr>
  @endforeach
</tbody>
