@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
@include('flash::message')
    <div class="row filter-row">
      <div class="modal-body">
          <form class="m-b-30" action="{{route('admin-aset-keluar')}}" method="post">
            <input type="hidden" name="action" value="add" required>
            @csrf
            <h3>Transaksi Aset Keluar</h3><br>
            <div class="row">

              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Transaksi <span class="text-danger">*</span></label>
                  <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y')}}" disabled></div>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Jenis Pengeluaran 1 <span class="text-danger">*</span></label>
                  <?php $jenises = App\JenisMasukanKeluaran::where('aktif',1)->where('status','Induk')->get(); ?>
                  <select name="jenis" id="jenis" class="form-control" required>
                    <option value="">Pilih </option>
                    @foreach($jenises as $jeniss)
                    <option value="{{$jeniss->id}}">{{$jeniss->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <script type="text/javascript">
              $("select[name='jenis']").change(function(){
                $('.modal').modal('show');
                var idjenis = $("#jenis").val();
                var token = $("input[name='_token']").val();
                    $.ajax({
                      url: "<?php echo route('admin-check-jenis') ?>",
                      method:'POST',
                      data:{_token:token,jenis:idjenis},
                      success:function(data){
                        console.log(data);
                        $('.modal').modal('hide');
                        // if (data) {
                          $("#jenis2").html("");
                          $("#jenis2").append(data);
                        //   $("#respon").show();
                        //   $('.modal').modal('hide');
                        // }else if (data.code=400){
                        //   alert('Nomor Anggota Tidak Terdaftar');
                        //   $('.modal').modal('hide');
                        // }
                      }
                    });

                });
              </script>
              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Jenis Pengeluaran 2 <span class="text-danger">*</span></label>
                  <select class="select" name="jenis2" id="jenis2" required>
                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Kas / Bank <span class="text-danger">*</span></label>
                  <?php $saldobanks = App\SaldoKasbank::get(); ?>
                  <select class="select form-control" name="kasbank">
                    <option value="">Pilih Keluar Dari Kas / Bank</option>
                    @foreach($saldobanks as $saldobank)
                    <option value="{{$saldobank->name}}">Keluar {{$saldobank->name}} (Saldo : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                    @endforeach
                  </select>
                  <!-- <select class="select" name="kasbank" required>
                    <option value="">Pilih Keluar Dari Kas / Bank</option>
                    <option value="Kas">Keluar Dari Kas</option>
                    <option value="Bank">Keluar Dari Bank</option>
                  </select> -->
                </div>
              </div>
              <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
                var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
                angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
                }
                function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
                document.getElementById("nominal").value = rupiah; }
                </script>
                <script> function convertToRupiah (objek) {
                 separator = ",";
                 a = objek.value;
                 b = a.replace(/[^Rp \d]/g,"");
                 c = "";
                 panjang = b.length;
                 j = 0; for (i = panjang; i > 0; i--) {
                   j = j + 1;
                  if (((j % 3) == 1) && (j != 1)) {
                     c = b.substr(i-1,1) + separator + c;
                   } else {
                     c = b.substr(i-1,1) + c;
                   }
                 } objek.value = c;
                }
         </script>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Nominal<span class="text-danger">*</span></label>
                  <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" id="nominal" placeholder="Rp 0" required>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                  <textarea class="form-control" name="ket" rows="4" cols="60" required></textarea>
                </div>
              </div>

            </div>

            <div class="m-t-20 text-center">
              <button class="btn btn-primary">PROSES</button> <a href="{{route('admin-aset-keluar')}}" class="btn btn-danger">BATAL</a>
            </div>
          </form>
        </div>
      </div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>

  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...<br>
          </div>

      </div>
  </div>
@endsection
