@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
@include('flash::message')
      <div class="row">
      <div class="col-xs-4">
        <h4 class="page-title">Pemindahan Aset</h4>
      </div>
      <div class="col-xs-8 text-right m-b-30">
        <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#transaksiaset"><i class="fa fa-plus"></i> Transaksi </a>
        <div class="view-icons">
          <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

        </div>
      </div>
      </div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-pemindahan-aset')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Pemindahan</label>
        <?php $toendtos = App\ToEndTo::get(); ?>
        <select class="select floating" name="dari_ke">
          <option value="">-- Semua -- </option>
          @foreach($toendtos as $toendto)
          @if($toendto->id == $kasbank)
          <option value="{{$toendto->id}}" selected> {{$toendto->name}} </option>
          @else
          <option value="{{$toendto->id}}"> {{$toendto->name}} </option>
          @endif
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2 col-xs-12"><br>
  <!-- <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">
<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Transaksi</th>
          <th>Tgl.Transaksi</th>
          <th>Nominal</th>
          <th>Pemindahan</th>
          <th>Keterangan</th>
          <th>Admin</th>
          {{--<th class="text-right">Action</th>--}}
        </tr>
      </thead>
      <tbody>
        <?php $kaskebank = 0; $bankkekas = 0;?>
        @foreach($datas as $key => $data)

        <?php
        if ($data->pemindahan == 1) {
          $kaskebank+= $data->nominal;
        }elseif ($data->pemindahan == 2) {
          $bankkekas+= $data->nominal;
        }?>

        <tr>
          <td>{{$key+1}}.</td>
          <td>{{$data->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}</td>
          <td>Rp {{number_format($data->nominal,0,".",",")}}</td>
          <td>{{$data->dariKe->name}}</td>
          <td>{{$data->ket}}</td>
          <td>{{$data->userId->name}}</td>
          {{--<td class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit">Edit</a>
            <a href="#" class="btn btn-danger btn-sm rounded" data-toggle="modal" data-target="#hapus">Hapus</a>
          </td>--}}
        </tr>
        @endforeach
        @if(count($datas) < 1)
        <tr>
          <td colspan="7" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Kas ke Bank</td>
    <td>: Rp {{number_format($kaskebank,0,".",",")}}</td>
  </tr>
  <tr>
    <td>Total Bank ke Kas</td>
    <td>: Rp {{number_format($bankkekas,0,".",",")}}</td>
  </tr>
</table>

</div>
</div>

<div id="edit" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Edit  Aset Masuk</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-pemindahan-aset')}}" method="post">
          @csrf
          <div class="row">
            <input type="hidden" name="action" value="edit" required>
            <input type="hidden" name="ids" value="" required>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Transaksi <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y')}}" disabled></div>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Kas / Bank <span class="text-danger">*</span></label>
                <?php $kasbanks = [
                  'Kas','Bank'
                ]; ?>
                <select class="select floating" name="kasbank">
                  @foreach($kasbanks as $kasbank)
                  @if($kasbank)
                  <option value="{{$kasbank}}" selected> {{$kasbank}} </option>
                  @else
                  <option value="{{$kasbank}}"> {{$kasbank}} </option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
              var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
              angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
              }
              function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
              document.getElementById("nominal").value = rupiah; }
              </script>
              <script> function convertToRupiah (objek) {
               separator = ",";
               a = objek.value;
               b = a.replace(/[^Rp \d]/g,"");
               c = "";
               panjang = b.length;
               j = 0; for (i = panjang; i > 0; i--) {
                 j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                   c = b.substr(i-1,1) + separator + c;
                 } else {
                   c = b.substr(i-1,1) + c;
                 }
               } objek.value = c;
              }
       </script>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nominal<span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" id="nominal" value="" placeholder="Rp 0" required>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                <textarea class="form-control" name="ket" rows="4" cols="60" required></textarea>
              </div>
            </div>

          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">PROSES</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<div id="hapus" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Aset Masuk</h4>
      </div>
      <form action="{{route('admin-pemindahan-aset')}}" method="post">
        @csrf
        <input type="hidden" name="action" value="hapus">
        <input type="hidden" name="ids" value="">
        <div class="modal-body card-box">
          <p>Apakah yakin ingin di Hapus : Rp  ???</p>
          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-danger">Proses</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="transaksiaset" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Transaksi Pemindahan Aset</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-pemindahan-aset')}}" method="post">
          <input type="hidden" name="action" value="add" required>
          @csrf
          <div class="row">

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Transaksi <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y')}}" disabled></div>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Pemindahan <span class="text-danger">*</span></label>
                <?php $toendtos = App\ToEndTo::get(); ?>
                <select class="select floating" name="dari_ke">
                  <option value="">-- Pilih -- </option>
                  @foreach($toendtos as $toendto)
                  @if($toendto->id == old('dari_ke'))
                  <option value="{{$toendto->id}}" selected> {{$toendto->name}} </option>
                  @else
                  <option value="{{$toendto->id}}"> {{$toendto->name}} </option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
              var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
              angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
              }
              function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
              document.getElementById("nominal").value = rupiah; }
              </script>
              <script> function convertToRupiah (objek) {
               separator = ",";
               a = objek.value;
               b = a.replace(/[^Rp \d]/g,"");
               c = "";
               panjang = b.length;
               j = 0; for (i = panjang; i > 0; i--) {
                 j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                   c = b.substr(i-1,1) + separator + c;
                 } else {
                   c = b.substr(i-1,1) + c;
                 }
               } objek.value = c;
              }
       </script>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nominal<span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" value="{{old('nominal')}}" name="nominal" id="nominal" placeholder="Rp 0">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                <textarea class="form-control" name="ket" rows="4" cols="60">{{old('ket')}}</textarea>
              </div>
            </div>

          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">PROSES</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
