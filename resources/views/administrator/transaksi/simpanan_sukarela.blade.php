@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @include('flash::message')
<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Simpanan Sukarela</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <div class="modal-body">
    <form class="m-b-30" action="{{route('admin-simpan-pokok')}}" method="post" id="submit">
      @csrf
      <input type="hidden" name="action" value="tambah">
      <div class="row">
        <div id="respon"></div>
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label">Nomor Anggota Min 6 angka terakhir <span class="text-danger">*</span></label>
            <input class="form-control" type="text" value="{{old('no_anggota')}}" name="no_anggota" id="no_anggota">
          </div>
        </div>
        <script type="text/javascript">
        console.log('masuk javascript');
        $("input[name='no_anggota']").on("change keyup paste", function(){
          var noanggota = $("#no_anggota").val();
          var token = $("input[name='_token']").val();
          var jenis = 3;
            if (noanggota.length>=6) {
              $('.modal').modal('show');
              $.ajax({
                url: "<?php echo route('admin-check-anggota') ?>",
                method:'POST',
                data:{_token:token, noanggota:noanggota,jenis:jenis},
                success:function(data){
                  console.log(data);
                  if (data.code=200) {
                    $("#respon").show();
                    $("#respon").html("");
                    $("#respon").append(data.datas);
                    $("#tabledata").html("");
                    $("#tabledata").append(data.table);
                    $('.modal').modal('hide');
                  }else if (data.code=400){
                    alert('Nomor Anggota Tidak Terdaftar');
                    $('.modal').modal('hide');
                  }
                }
              });
            }else {
              $("#respon").hide();
              $('.modal').modal('hide');
            }

          });
        </script>

        <div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
              <div class="cal-icon"><input class="form-control datetimepicker" type="text" value="{{old('tgl_setor')}}" name="tgl_setor"></div>
            </div>
          </div>

          <!-- <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Jumlah Bulan <span class="text-danger">*</span></label>

              <select class="select floating" name="jml_bulan" required>
                <option value="1">Bayar 1 Bulan </option>
                <option value="2">Bayar 2 Bulan </option>
                <option value="3">Bayar 3 Bulan </option>
                <option value="4">Bayar 4 Bulan </option>
                <option value="5">Bayar 5 Bulan </option>
                <option value="6">Bayar 6 Bulan </option>
                <option value="7">Bayar 7 Bulan </option>
                <option value="8">Bayar 8 Bulan </option>
                <option value="9">Bayar 9 Bulan </option>
                <option value="10">Bayar 10 Bulan </option>
                <option value="11">Bayar 11 Bulan </option>
                <option value="12">Bayar 12 Bulan </option>
              </select>
            </div>
          </div> -->
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
              <?php $jenis = App\JenisSimpanan::find(3); ?>
              <select class="select floating" name="jenis_simpanan">
                <option value="{{$jenis->id}}" selected> {{$jenis->name}} </option>

              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
              <?php $mutasi = ['Kredit']; ?>
              <select class="select floating" name="mutasi">
                @foreach($mutasi as $mut)
                    <option value="{{$mut}}"> {{$mut}} </option>
                @endforeach
              </select>
            </div>
          </div>
          <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
    angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
    }
    function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
    document.getElementById("nominal").value = rupiah; }
    </script>
    <script> function convertToRupiah (objek) {
     separator = ",";
     a = objek.value;
     b = a.replace(/[^Rp \d]/g,"");
     c = "";
     panjang = b.length;
     j = 0; for (i = panjang; i > 0; i--) {
       j = j + 1;
      if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     } objek.value = c;
    }
     </script>
     <div class="col-sm-4">
       <div class="form-group">
         <label class="control-label">Kas / Bank <span class="text-danger">*</span></label>
         <?php $banks = ['Kas','Bank']; ?>
         <select class="select floating" name="kasbank">
           <option value="">Pilih</option>
           @foreach($banks as $bank)
             @if(old('kasbank') == $bank)
               <option value="{{$bank}}" selected> {{$bank}} </option>
             @else
               <option value="{{$bank}}"> {{$bank}} </option>
             @endif
           @endforeach
         </select>
       </div>
     </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
              <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" value="{{old('nominal')}}" id="nominal" placeholder="Rp 0">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
              <textarea class="form-control" type="text" name="ket">{{old('ket')}}</textarea>
            </div>
          </div>
        <div class="m-t-20 text-center">
          <button class="btn btn-primary" id="btn_proses">P R O S E S</button>
        </div>
        </div>

    </form>
    <script type="text/javascript">
      $("#btn_proses").on("click", function(){
        $('.modal').modal('show');
        document.getElementById('submit').submit();
      });
    </script>
  </div>
</div>
  <!-- <div class="col-sm-2 col-xs-12"><br>
  <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a>
              </div> -->
</div>

<div class="row">

<div class="col-md-12">

  <div class="table-responsive">

      <div id="tabledata"></div>
  </div>
</div>


</div>

    </div>


    <div id="add_employee" class="modal custom-modal fade" role="dialog">
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...<br>
          </div>

      </div>
  </div>
@endsection
