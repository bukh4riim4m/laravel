@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Bayar Peminjaman</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Transaksi Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>-->
</div>
</div>
<div class="row filter-row">
  <div class="modal-body">
    <form class="m-b-30" action="{{route('admin-bayar-pinjaman')}}" method="post">
      @csrf
      <input type="hidden" name="action" value="proses">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label">Nomor Pinjaman<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="no_pinjaman" id="no_pinjaman" required>
          </div>
        </div>
        <div class="col-sm-12" id="notif" style="display:none;">
          <div class="form-group">
            <div id="gagal">

            </div>
            <!-- <label class="form-control btn btn-danger">NOMOR PINJAMAN TIDAK DI TEMUKAN</label> -->
          </div>
        </div>

        <script type="text/javascript">
        console.log('masuk javascript');
        $("input[name='no_pinjaman']").on("change keyup paste", function(){
          var nopinjaman = $("#no_pinjaman").val();
          var token = $("input[name='_token']").val();

            if (nopinjaman.length>=15) {
              $.ajax({
                url: "<?php echo route('admin-pinjaman-anggota') ?>",
                method:'POST',
                data:{_token:token, nopinjaman:nopinjaman},
                success:function(data){
                  console.log(data);
                  if (data.code==200) {
                    $("#notif").hide();
                    $("#nama_anggota").html("");
                    $("#nama_anggota").append(data.isi.nama_anggota);
                    $("#nomor_anggota").html("");
                    $("#nomor_anggota").append(data.isi.nomor_anggota);
                    $("#tgl_bayar").html("");
                    $("#tgl_bayar").append(data.isi.tgl_bayar);
                    $("#jml_piutang").html("");
                    $("#jml_piutang").append(data.isi.jml_piutang);
                    $("#bayar").html("");
                    $("#bayar").append(data.isi.bayar);
                    $("#pokok").html("");
                    $("#pokok").append(data.isi.pokok);
                    $("#bunga").html("");
                    $("#bunga").append(data.isi.bunga);
                    $("#sisa_pinjaman").html("");
                    $("#sisa_pinjaman").append(data.isi.sisa_pinjaman);
                    $("#sisa_piutang").html("");
                    $("#sisa_piutang").append(data.isi.sisa_piutang);
                    $("#urutanbayar").html("");
                    $("#urutanbayar").append(data.isi.urutanbayar);
                    $("#detailpembayaran").show();
                  }else if (data.code==400){
                    $("#gagal").html("");
                    $("#gagal").append(data.isi);
                    $("#notif").show();
                    $("#detailpembayaran").hide();
                  }
                }
              });

            }else {
              $("#detailpembayaran").hide();
              $("#notif").hide();
            }

          });
        </script>
        <div style="display:none;" id="detailpembayaran">
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Nomor Anggota</label>
              <div id="nomor_anggota"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Nama Anggota</label>
              <div id="nama_anggota"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Tanggal</label>
              <div id="tgl_bayar"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Jumlah Piutang</label>
              <div id="jml_piutang"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Sisa Pinjaman</label>
              <div id="sisa_pinjaman"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Bayar</label>
              <div id="bayar"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Pokok</label>
              <div id="pokok"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Bunga</label>
              <div id="bunga"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Sisa Piutang</label>
              <div id="sisa_piutang"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Pembayaran</label>
              <div id="urutanbayar"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Pembayaran<span class="text-danger">*</span></label>
              <?php $saldobanks = App\SaldoKasbank::get(); ?>
              <select class="form-control" name="pembayaran" required>
                <option value="">Pilih</option>
                @foreach($saldobanks as $saldobank)
                <option value="{{$saldobank->id}}">Uang {{$saldobank->name}} (Sisa : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="m-t-20 text-center">
             <button class="btn btn-success">P R O S E S</button> <a href=""><button type="button" class="btn btn-danger">B A T A L</button></a>
          </div>
        </div>





      </div>


    </form>

  </div>
</div>

</div>


    </div>



  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
