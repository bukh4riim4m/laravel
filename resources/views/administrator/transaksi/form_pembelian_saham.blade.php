@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
@include('flash::message')
<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Pembelian Saham</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Transaksi Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>-->
</div>
</div>
<div class="row filter-row">
  <div class="modal-body">
    <form class="m-b-30" action="{{route('admin-transaksi-pembelian-saham')}}" method="post" enctype="multipart/form-data" id="submit">
      @csrf
      <input type="hidden" name="action" value="proses">
      <div class="row">
        <div id="respon"></div>
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label">Nomor Anggota Min 6 angka terakhir <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="no_anggota" value="{{old('no_anggota')}}" minlength="6" required id="no_anggota">
          </div>
        </div>
        <script type="text/javascript">
        console.log('masuk javascript');
        $("input[name='no_anggota']").on("change keyup paste", function(){
          var noanggota = $("#no_anggota").val();
          var jenis = 1;
          var token = $("input[name='_token']").val();

            if (noanggota.length>=6) {
              $('.modal').modal('show');
              $.ajax({
                url: "<?php echo route('admin-check-pemegang-saham') ?>",
                method:'POST',
                data:{_token:token, noanggota:noanggota,jenis:jenis},
                success:function(data){
                  console.log(data);
                  if (data.code=200) {
                    $("#respon").html("");
                    $("#respon").append(data.datas);
                    $("#respon").show();
                    $('.modal').modal('hide');
                  }else if (data.code=400){
                    alert('Nomor Anggota Tidak Terdaftar');
                    $('.modal').modal('hide');
                  }
                }
              });
            }else {
              $("#respon").hide();
              $('.modal').modal('hide');
            }

          });
        </script>

        <div id="transactionLoader">
          <center> <div id="wait"><div class='col-md-12'><div class='loading9 text-center'><i></i><i></i><i></i><i></i></div></div></div></center>
        </div>
        <script>
          $("#transactionLoader").hide();
        </script>
        <div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Tanggal Pembelian <span class="text-danger">*</span></label>
              <div class="cal-icon"><input class="form-control datetimepicker" type="text" value="{{date('d-m-Y')}}" name="tgl_beli" disabled></div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Ambil Kas / Bank <span class="text-danger">*</span></label>
              <?php $banks = App\SaldoKasbank::get(); ?>
              <select class="select floating" name="kasbank" required>
                <option value="">Pilih</option>
                @foreach($banks as $bank)
                    <option value="{{$bank->id}}"> {{$bank->name}} (Saldo:Rp {{number_format($bank->saldo,0,'.',',')}})</option>
                @endforeach
              </select>
            </div>
          </div>
          <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
    angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
    }
    function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
    document.getElementById("nominal").value = rupiah; }
    </script>
    <script> function convertToRupiah (objek) {
     separator = ",";
     a = objek.value;
     b = a.replace(/[^Rp \d]/g,"");
     c = "";
     panjang = b.length;
     j = 0; for (i = panjang; i > 0; i--) {
       j = j + 1;
      if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     } objek.value = c;
    }
     </script>

          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Nomor Saham <span class="text-danger">*</span></label>
              <input type="text" class="form-control form-control-rounded"  name="no_saham" value="{{old('no_saham')}}" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Harga Saham <span class="text-danger">*</span></label>
              <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" value="{{old('nominal')}}" id="nominal" placeholder="Rp 0" required>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label class="control-label">Keterangan Tambahan <span class="text-danger">*</span></label>
              <textarea class="form-control" type="text" name="ket" required>{{old('ket')}}</textarea>
            </div>
          </div>
        <div class="m-t-20 text-center">
          <a href="#" class="btn btn-primary" id="btn_proses">P R O S E S</a>
          <!-- <button class="btn btn-primary">P R O S E S</button> -->
        </div>
        <script type="text/javascript">
          $("#btn_proses").on("click", function(){
            $('.modal').modal('show');
            document.getElementById('submit').submit();
          });
        </script>
        </div>

    </form>
  </div>
</div>
  <!-- <div class="col-sm-2 col-xs-12"><br>
  <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a>
              </div> -->
</div>

<!-- <div class="row"> -->

<!-- <div class="col-md-12">

  <div class="table-responsive">

      <div id="tabledata"></div>
  </div>
</div> -->


<!-- </div> -->

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...<br>
          </div>

      </div>
  </div>
@endsection
