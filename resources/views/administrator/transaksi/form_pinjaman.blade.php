@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Peminjaman</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Transaksi Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>-->
</div>
</div>
<div class="row filter-row">
  <div class="modal-body">
    <form class="m-b-30" action="{{route('admin-form-pinjaman')}}" method="post">
      @csrf
      <input type="hidden" name="action" value="proses">
      <div class="row">
        <div id="respon"></div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
            <div class="cal-icon"><input class="form-control datetimepicker" type="text" value="{{date('dd-mm-Y')}}" name="tgl_setor" id="tgl_setor" disabled></div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label">Nomor Pinjaman <span class="text-danger">*</span></label>
            <?php $jumlah = count(App\Pinjaman::get());
            $urut = date('10000000')+1+$jumlah;
            $urutan = substr($urut, -7);
            ?>
            <input type="hidden" name="no_pinjam" value="NP-{{date('ym')}}-{{$urutan}}" required>
            <input class="form-control" type="text" value="NP-{{date('ym')}}-{{$urutan}}" disabled>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label">Nomor Anggota Min 6 angka terakhir <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="no_anggota" minlength="6" required id="no_anggota" placeholder="Nomor Anggota">
          </div>
        </div>



        <!-- <div class="col-sm-6">
          <div class="form-group">
            <label class="control-label">Masa Kredit<span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="masa_kredit" required>
          </div>
        </div> -->


        <script type="text/javascript">
        console.log('masuk javascript');
        $("input[name='no_anggota']").on("change keyup paste", function(){
          var noanggota = $("#no_anggota").val();
          var token = $("input[name='_token']").val();

            if (noanggota.length>=6) {
              $('.modal').modal('show');
              $.ajax({
                url: "<?php echo route('admin-check-anggota') ?>",
                method:'POST',
                data:{_token:token, noanggota:noanggota},
                success:function(data){
                  console.log(data);
                  if (data.code=200) {
                    $("#respon").html("");
                    $("#respon").append(data.datas);
                    $('.modal').modal('hide');
                  }else if (data.code=400){
                    alert('Nomor Anggota Tidak Terdaftar');
                    $('.modal').modal('hide');
                  }
                }
              });

            }else {
              $("#respon").hide();
              $('.modal').modal('hide');
            }

          });
        </script>




    <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
        angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
        }
        function rupiah(){
          var target_donasi = document.getElementById("nilai_pinjam").value;
          var rupiah = convertToRupiah(target_donasi);
          document.getElementById("nilai_pinjam").value = rupiah; }
    </script>
    <script> function convertToRupiah (objek) {
     separator = ",";
     a = objek.value;
     b = a.replace(/[^Rp \d]/g,"");
     c = "";
     panjang = b.length;
     j = 0; for (i = panjang; i > 0; i--) {
       j = j + 1;
      if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     } objek.value = c;
    }
     </script>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Nilai Pinjaman <span class="text-danger">*</span></label>
              <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nilai_pinjam" id="nilai_pinjam" placeholder="Rp 0" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Angsuran (Bulanan)<span class="text-danger">*</span></label>
              <!-- <div id="angsuran"></div> -->
              <input class="form-control" type="number" name="angsuran" id="angsuran" placeholder="Berapa Kali ?" required>
            </div>
          </div>
          <!-- <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Kas & Bank<span class="text-danger">*</span></label>
              <input class="form-control" type="text" name="bank" required>
            </div>
          </div> -->
          <script language="javascript">
            function getkey(e)
            {
            if (window.event)
               return window.event.keyCode;
            else if (e)
               return e.which;
            else
               return null;
            }
            function angkadanhuruf(e, goods, field)
            {
            var angka, karakterangka;
            angka = getkey(e);
            if (angka == null) return true;

            karakterangka = String.fromCharCode(angka);
            karakterangka = karakterangka.toLowerCase();
            goods = goods.toLowerCase();

            // check goodkeys
            if (goods.indexOf(karakterangka) != -1)
                return true;
            // control angka
            if ( angka==null || angka==0 || angka==8 || angka==9 || angka==27 )
               return true;

            if (angka == 13) {
                var i;
                for (i = 0; i < field.form.elements.length; i++)
                    if (field == field.form.elements[i])
                        break;
                i = (i + 1) % field.form.elements.length;
                field.form.elements[i].focus();
                return false;
                };
            // else return false
            return false;
            }
            </script>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Bunga dalam % ( . ) <span class="text-danger">*</span></label>
              <input class="form-control" type="text" onkeypress="return angkadanhuruf(event,'0123456789.',this)" name="persen_bunga" id="persen_bunga" required>

              <!-- <select class="form-control floating" name="persen_bunga" id="persen_bunga" required>
                <option value="">Pilih Bunga</option>
                <?php for ($i=1; $i < 21; $i++) {
                  echo "<option value=".$i.">".$i." % </option>";
                } ?>
              </select> -->
            </div>
          </div>

          <div id="detail" style="display:none;">

            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label">Total Bunga dari %</label>
                <div id="nilaibunga"></div>
                <!-- <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nilai_bunga" id="nilai_bunga" placeholder="Rp 0" required> -->
              </div>
            </div>


            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label">Total Pinjaman + Bunga</label>
                <div id="jumlahtotal"></div>
                <!-- <input class="form-control" type="text" name="jumlah_total" required> -->
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label">Pokok/Bulan</label>
                <div id="nilaipokokperbulan"></div>
                <!-- <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nilai_bunga" id="nilai_bunga" placeholder="Rp 0" required> -->
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label">Bunga/Bulan</label>
                <div id="nilaibungaperbuln"></div>
                <!-- <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nilai_bunga" id="nilai_bunga" placeholder="Rp 0" required> -->
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nilai Angsuran/Bulan</label>
                <div id="nominalangsuran"></div>
                <!-- <input class="form-control" type="text" name="nominal_angsuran" required> -->
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jatuh Tempo </label>
                <div id="jatuhtempo"></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Ambil Uang Dari? <span class="text-danger">*</span></label>
                <?php $saldobanks = App\SaldoKasbank::get(); ?>
                <select class="form-control" name="ambil_uang" required>
                  <option value="">Pilih</option>
                  @foreach($saldobanks as $saldobank)
                  <option value="{{$saldobank->id}}">Uang {{$saldobank->name}} (Sisa : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan <span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div>
            <div class="m-t-20 text-center">
              <button class="btn btn-success">P R O S E S</button> <a href=""><button type="button" class="btn btn-danger">B A T A L</button></a> <a class="btn btn-primary" href="#" id="kalkulasi2">KALKULASI</a>
            </div>
          </div>

          <div class="m-t-20 text-center">
            <a class="btn btn-primary" href="#" id="kalkulasi">KALKULASI</a>
            <!-- <button class="btn btn-primary" id="kalkulasi"></button> -->
          </div>
          <script type="text/javascript">
          console.log('masuk javascript');
          $("#kalkulasi").on("click", function(){
            $('.modal').modal('show');
            var tglsetor = $("#tgl_setor").val();
            var angsurans = $("#angsuran").val();
            var nilaipinjam = $("#nilai_pinjam").val();
            var persenbunga = $("#persen_bunga").val();
            var token = $("input[name='_token']").val();


                $.ajax({
                  url: "<?php echo route('admin-check-kalkulasi') ?>",
                  method:'POST',
                  data:{_token:token, tglsetor:tglsetor, angsuran:angsurans, nilaipinjam:nilaipinjam, persenbunga:persenbunga},
                  success:function(data){
                    console.log(data);
                    if (data.code=200) {
                      $("#nilaibungaperbuln").html("");
                      $("#nilaibungaperbuln").append(data.isi.nilaibungaperbuln);
                      $("#nilaipokokperbulan").html("");
                      $("#nilaipokokperbulan").append(data.isi.nilaipokokperbulan);
                      $("#nilaibunga").html("");
                      $("#nilaibunga").append(data.isi.nilaibunga);
                      $("#jatuhtempo").html("");
                      $("#jatuhtempo").append(data.isi.nilaitempo);
                      $("#nominalangsuran").html("");
                      $("#nominalangsuran").append(data.isi.nilaiangsuran);
                      $("#jumlahtotal").html("");
                      $("#jumlahtotal").append(data.isi.nilaitotal);
                      $("#kalkulasi").hide();
                      $("#detail").show();
                      $("#proses").show();
                      $('.modal').modal('hide');
                    }
                  }
                });


            });
            // $('.modal').modal('hide');
            $("#kalkulasi2").on("click", function(){
              $('.modal').modal('show');
              var tglsetor = $("#tgl_setor").val();
              var angsurans = $("#angsuran").val();
              var nilaipinjam = $("#nilai_pinjam").val();
              var persenbunga = $("#persen_bunga").val();
              var token = $("input[name='_token']").val();


                  $.ajax({
                    url: "<?php echo route('admin-check-kalkulasi') ?>",
                    method:'POST',
                    data:{_token:token, tglsetor:tglsetor, angsuran:angsurans, nilaipinjam:nilaipinjam, persenbunga:persenbunga},
                    success:function(data){
                      console.log(data);
                      if (data.code=200) {
                        $("#nilaibungaperbuln").html("");
                        $("#nilaibungaperbuln").append(data.isi.nilaibungaperbuln);
                        $("#nilaipokokperbulan").html("");
                        $("#nilaipokokperbulan").append(data.isi.nilaipokokperbulan);
                        $("#nilaibunga").html("");
                        $("#nilaibunga").append(data.isi.nilaibunga);
                        $("#jatuhtempo").html("");
                        $("#jatuhtempo").append(data.isi.nilaitempo);
                        $("#nominalangsuran").html("");
                        $("#nominalangsuran").append(data.isi.nilaiangsuran);
                        $("#jumlahtotal").html("");
                        $("#jumlahtotal").append(data.isi.nilaitotal);
                        $("#kalkulasi").hide();
                        $("#detail").show();
                        $("#proses").show();
                        $('.modal').modal('hide');
                      }
                    }
                  });
            });


            $("#nilai_pinjam").on("change keyup paste", function(){
              $("#detail").hide();
              $("#kalkulasi").show();
            });
            $("#angsuran").on("change keyup paste", function(){
              $("#detail").hide();
              $("#kalkulasi").show();
            });

            $("#persen_bunga").on("change", function(){
              $("#detail").hide();
              $("#kalkulasi").show();
            });
            $("#tgl_tempo").on("change keyup paste click", function(){
              $("#detail").hide();
              $("#kalkulasi").show();
            });
            $("#no_anggota").on("change keyup paste", function(){
              $("#detail").hide();
              $("#kalkulasi").show();
            });

            $('.modal').modal('hide');
          </script>

    </form>

  </div>
</div>

</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...<br>
          </div>
      </div>
  </div>
@endsection
