@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center">

              <script type="text/javascript">
                window.onload = function () {

                var chart = new CanvasJS.Chart("chartContainer", {
                	theme: "light1", // "light2", "dark1", "dark2"
                	animationEnabled: false, // change to true
                	title:{
                		text: "Grafik Pinjaman"
                	},
                	data: [
                	{
                		// Change type to "bar", "area", "spline", "pie",etc.
                		type: "column",
                		dataPoints: [
                			{ label: "Januari",  y: {{$januari}}  },
                			{ label: "Februari", y: {{$februari}}  },
                			{ label: "Maret", y: {{$maret}}  },
                			{ label: "April",  y: {{$april}}  },
                			{ label: "May",  y: {{$mei}}  },
                      { label: "Juni",  y: {{$juni}}  },
                      { label: "Juli",  y: {{$juli}}  },
                      { label: "Agustus",  y: {{$agustus}}  },
                      { label: "September",  y: {{$september}}  },
                      { label: "Oktober",  y: {{$oktober}}  },
                      { label: "November",  y: {{$november}}  },
                      { label: "Desember",  y: {{$desember}}  }
                		]
                	}
                	]
                });
                chart.render();

                }
                </script>
              </head>
              <body>
              <div id="chartContainer" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
