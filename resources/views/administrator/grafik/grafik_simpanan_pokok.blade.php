@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center">

              <script type="text/javascript">
                window.onload = function () {

                var chart = new CanvasJS.Chart("chartContainer", {
                	theme: "light1", // "light2", "dark1", "dark2"
                	animationEnabled: false, // change to true
                	title:{
                		text: "Grafik Simpanan Pokok"
                	},
                	data: [
                	{
                		// Change type to "bar", "area", "spline", "pie",etc.
                		type: "column",
                		dataPoints: [
                			{ label: "Januari",  y: {{$januari}}  },
                			{ label: "Februari", y: {{$februari}}  },
                			{ label: "Maret", y: {{$maret}}  },
                			{ label: "April",  y: {{$april}}  },
                			{ label: "May",  y: {{$mei}}  },
                      { label: "Juni",  y: {{$juni}}  },
                      { label: "Juli",  y: {{$juli}}  },
                      { label: "Agustus",  y: {{$agustus}}  },
                      { label: "September",  y: {{$september}}  },
                      { label: "Oktober",  y: {{$oktober}}  },
                      { label: "November",  y: {{$november}}  },
                      { label: "Desember",  y: {{$desember}}  }
                		]
                	}
                	]
                });
                chart.render();

                var chart = new CanvasJS.Chart("chartContainer2", {
                	theme: "light1", // "light2", "dark1", "dark2"
                	animationEnabled: false, // change to true
                	title:{
                		text: "Grafik Simpanan Wajib"
                	},
                	data: [
                	{
                		// Change type to "bar", "area", "spline", "pie",etc.
                		type: "column",
                		dataPoints: [
                			{ label: "Januari",  y: {{$januariw}}  },
                			{ label: "Februari", y: {{$februariw}}  },
                			{ label: "Maret", y: {{$maretw}}  },
                			{ label: "April",  y: {{$aprilw}}  },
                			{ label: "May",  y: {{$meiw}}  },
                      { label: "Juni",  y: {{$juniw}}  },
                      { label: "Juli",  y: {{$juliw}}  },
                      { label: "Agustus",  y: {{$agustusw}}  },
                      { label: "September",  y: {{$septemberw}}  },
                      { label: "Oktober",  y: {{$oktoberw}}  },
                      { label: "November",  y: {{$novemberw}}  },
                      { label: "Desember",  y: {{$desemberw}}  }
                		]
                	}
                	]
                });
                chart.render();
                var chart = new CanvasJS.Chart("chartContainer3", {
                	theme: "light1", // "light2", "dark1", "dark2"
                	animationEnabled: false, // change to true
                	title:{
                		text: "Grafik Simpanan Sukarela"
                	},
                	data: [
                	{
                		// Change type to "bar", "area", "spline", "pie",etc.
                		type: "column",
                		dataPoints: [
                			{ label: "Januari",  y: {{$januaris}}  },
                			{ label: "Februari", y: {{$februaris}}  },
                			{ label: "Maret", y: {{$marets}}  },
                			{ label: "April",  y: {{$aprils}}  },
                			{ label: "May",  y: {{$meis}}  },
                      { label: "Juni",  y: {{$junis}}  },
                      { label: "Juli",  y: {{$julis}}  },
                      { label: "Agustus",  y: {{$agustuss}}  },
                      { label: "September",  y: {{$septembers}}  },
                      { label: "Oktober",  y: {{$oktobers}}  },
                      { label: "November",  y: {{$novembers}}  },
                      { label: "Desember",  y: {{$desembers}}  }
                		]
                	}
                	]
                });
                chart.render();
                var chart = new CanvasJS.Chart("chartContainer4", {
                	theme: "light1", // "light2", "dark1", "dark2"
                	animationEnabled: false, // change to true
                	title:{
                		text: "Grafik Simpanan Khusus"
                	},
                	data: [
                	{
                		// Change type to "bar", "area", "spline", "pie",etc.
                		type: "column",
                		dataPoints: [
                			{ label: "Januari",  y: {{$januarik}}  },
                			{ label: "Februari", y: {{$februarik}}  },
                			{ label: "Maret", y: {{$maretk}}  },
                			{ label: "April",  y: {{$aprilk}}  },
                			{ label: "May",  y: {{$meik}}  },
                      { label: "Juni",  y: {{$junik}}  },
                      { label: "Juli",  y: {{$julik}}  },
                      { label: "Agustus",  y: {{$agustusk}}  },
                      { label: "September",  y: {{$septemberk}}  },
                      { label: "Oktober",  y: {{$oktoberk}}  },
                      { label: "November",  y: {{$novemberk}}  },
                      { label: "Desember",  y: {{$desemberk}}  }
                		]
                	}
                	]
                });
                chart.render();

                }
                </script>
              <div id="chartContainer" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div><br>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center"><br>
              <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div><br>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center"><br>
              <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div><br>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center"><br>
              <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
