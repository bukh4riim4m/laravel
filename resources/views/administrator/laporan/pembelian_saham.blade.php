@extends('layouts.admin.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
            @include('flash::message')
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<h4 class="page-title">Pembelian Saham</h4>
						</div>
					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{route('admin-laporan-pembelian-saham')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-3 col-xs-12">
  							<div class="form-group form-focus">
  								<label class="control-label">No Anggota</label>
  								<input type="number" class="form-control floating" name="no_anggota" value="{{$nomor}}"/>
  							</div>
  						</div>
              <div class="col-sm-3 col-md-3 col-xs-6">
                <div class="form-group form-focus">
                  <label class="control-label">Dari Tgl</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
                </div>
              </div>
              <div class="col-sm-3 col-md-3 col-xs-6">
                <div class="form-group form-focus">
                  <label class="control-label">Sampai Tgl</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
                </div>
              </div>
  						<div class="col-sm-3 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            <style>
              #kartuname {
                position: absolute;
                top:87px;
                color: black;
                font-size: 6px;
                bottom: 100px;
                margin-right: 33px;
                right: 35px;
                overflow: hidden;
              }
            </style>

            <div class="col-md-12">

              <div class="table-responsive">

                <table class="table table-striped custom-table">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Tgl. Transaksi<br>No. Transaksi</th>
                      <th>Nama Anggota<br>No.Anggota</th>
                      <th>No.Saham</th>
                      <th>Masuk Kas/Bank<br>Nominal</th>
                      <!-- <th style="min-width:100px;" class="text-right">Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1;$total = 0; ?>
                    @foreach($datas as $data)
                    <?php $total+= $data->nominal;?>
                    <tr>
                      <td>{{$no++}}.</td>
                      <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}<br>{{$data->no_trx}}</td>
                      <td>{{$data->dataUser->name}}<br>{{$data->dataUser->no_anggota}}</td>
                      <td>{{$data->no_saham}}</td>
                      <td>{{$data->kasBankId->name}}<br>Rp {{number_format($data->nominal,'0','.',',')}}</td>
                      <!-- <td>Rp </td> -->
                      <!-- <td style="max-width:80px;" class="text-right">
                        <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anggota" id="editanggota">Edit</a>
                        <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#detail">Lihat</a>
                      </td> -->
                    </tr>
                    @endforeach
                    @if(count($datas) < 1)
                    <tr>
                      <td colspan="5" class="text-center">KOSONG</td>
                    </tr>
                    @endif
                  </tbody>

                </table>

              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th>Total </th>
                      <th>: Rp {{number_format($total,'0','.',',')}}</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
					</div>
                </div>

    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
