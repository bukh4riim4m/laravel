@extends('layouts.admin.appajax')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">History Pembayaran Pinjaman</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-pinjaman')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="history" required>
    <div class="col-sm-4">
      <div class="form-group form-focus">
        <label class="control-label">No. Pinjaman</label>
        <input type="text" class="form-control floating" name="no_pinjam" value="{{$pinjamans->no_pinjam}}"/>
      </div>
    </div>

    <div class="col-sm-4">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="C A R I"/>
    </div>
    <div class="col-sm-4 col-xs-12"><br>

                </div>
  </form>


</div>
<div class="row">
  <div class="col-md-8">
    <div class="table-responsive">
      <table class="table table-striped custom-table">
        <thead>
          <tr>
            <th>Nomor Anggota</th>
            <th>: {{$pinjamans->no_anggota}}</th>
          </tr>
          <tr>
            <th>Nama Anggota</th>
            <th>: {{$users->name}}</th>
          </tr>
          <tr>
            <th>Nomor Pinjam</th>
            <th>: {{$pinjamans->no_pinjam}}</th>
          </tr>
          <tr>
            <th>Total Pinjam</th>
            <th>: Rp {{number_format($pinjamans->total_pinjam,0,".",",")}}</th>
          </tr>
          <tr>
            <th>Angsuran / Bulan</th>
            <th>: Rp {{number_format($pinjamans->total_pinjam/$pinjamans->kali_angsuran,0,".",",")}}  Selama {{$pinjamans->kali_angsuran}} Kali</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <div class="col-md-4">
    <form class="form" action="{{route('admin-laporan-pinjaman')}}" method="post" id="exportdata">
      @csrf
      <input type="hidden" name="action" value="downloadpdf">
      <input type="hidden" name="no_pinjam" value="{{$pinjamans->no_pinjam}}">
    </form>
    <a href="#" class="pull-left"><img width="40px" src="{{asset('/images/hasil.png')}}" onclick="event.preventDefault();
                  document.getElementById('exportdata').submit();"/></a>
  </div>
</div>
<div class="row">
  @if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  <!-- <form class="" action="{{url('/administrator/export-data-anggota')}}" method="post" id="exportdata">
    @csrf
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No. Transaksi</th>
          <th>Jatuh Tempo</th>
          <th>Tgl. Bayar</th>
          <th>Pokok</th>
          <th>Bunga</th>
          <th>Angsuran/Bulan <br>(Pokok + Bunga)</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($historys as $key => $history)
        <tr>
          <td>{{$key+1}}.</td>
          <td>{{$history->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($history->tgl_tempo))}}</td>
          <td>@if($history->tgl_bayar==null)xx-xx-xxxx @else {{date('d-m-Y', strtotime($history->tgl_bayar))}} @endif</td>
          <td>Rp {{number_format($history->pokok,0,".",",")}}</td>
          <td>Rp {{number_format($history->bunga,0,".",",")}}</td>
          <td>Rp {{number_format($history->bayar,0,".",",")}}</td>
          <td>
              @if($history->status==1)
              <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="modal" data-target="#bayar{{$history->id}}"><i class="fa fa-dot-circle-o text-warning"></i> Belum Bayar</a>
              @else
              <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-dot-circle-o text-success"></i> Sudah Bayar</a>
              @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="row col-md-6 table-responsive">
    <table class="table table-striped custom-table" width="40px">
      <thead>
        <tr>
          <th>Sudah Terbayar</th>
          <th>: Rp {{number_format($terbayar,2,".",",")}}</th>
        </tr>
        <tr>
          <th>Sisa Pinjaman</th>
          <th>: Rp {{number_format($pinjamans->total_pinjam-$terbayar,2,".",",")}}</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
</div>
    </div>
    </div>



      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
  @foreach($historys as $key => $update)
  <div id="bayar{{$update->id}}" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content modal-md">
        <div class="modal-header">
          <h4 class="modal-title">Pembayaran Pinjaman</h4>
        </div>
        <form action="{{route('admin-pemabayaran-pinjaman')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="bayar">
          <input type="hidden" name="ids" value="{{$update->id}}">
          <div class="modal-body card-box">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Tgl. Bayar <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="{{date('d-m-Y')}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">No. Pinjam <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="{{$update->no_pinjamen}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Jumlah Piutang <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="Rp {{number_format($update->jumlah_piutang,0,'.',',')}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Jumlah Bayar <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="Rp {{number_format($update->bayar,0,'.',',')}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Jumlah Pokok <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="Rp {{number_format($update->pokok,0,'.',',')}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Jumlah Bunga <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="Rp {{number_format($update->bunga,0,'.',',')}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Angsuran Ke <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="{{$update->urutan_bayar}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Sisa Pinjaman <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="" value="Rp {{number_format($pinjamans->sisa_pinjam,2,'.',',')}}" disabled>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Pembayaran Masuk<span class="text-danger">*</span></label>
                  <?php $saldobanks = App\SaldoKasbank::get(); ?>
                  <select class="select form-control" name="pembayaran">
                    <option value="">Pilih</option>
                    @foreach($saldobanks as $saldobank)
                    <option value="{{$saldobank->id}}">Uang {{$saldobank->name}} (Sisa : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <!-- <p>Apakah yakin ingin di Hapus : Rp {{number_format($update->nominal,0,".",",")}} ???</p> -->

            <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
              <button type="submit" class="btn btn-danger">Proses</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  @endforeach
@endsection
