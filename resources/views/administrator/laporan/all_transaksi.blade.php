@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Laporan All Transaksi</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-keseluruhan')}}" method="post" id="submit">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" id="btn_proses" value="TAMPILKAN"/>
    </div>
  </form>
  <script type="text/javascript">
    $("#btn_proses").on("click", function(){
      $('.modal').modal('show');
      document.getElementById('submit').submit();
    });
  </script>
  <div class="col-sm-2 col-xs-12"><br>
  <!-- <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">

<div class="col-md-6">
  <div class="table-responsive">
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th colspan="2"> <strong>PEMASUKAN</strong> </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Total Simpanan Pokok</td><td>Rp {{number_format($simpananpokok,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Simpanan Wajib</td><td>Rp {{number_format($simpananwajib,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Simpanan Sukarela</td><td>Rp {{number_format($simpanansukarela,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Simpanan Khusus</td><td>Rp {{number_format($simpanankhusus,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Peminjaman Terbayar</td><td>Rp {{number_format($totalbayarpinjaman,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Aset Masuk</td><td>Rp {{number_format($totalasetmasuk,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Saham Terjual</td><td>Rp {{number_format($totalsahamterjual,'0','.',',')}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="col-md-6">
  <div class="table-responsive">
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th colspan="2"> <strong>PENGELUARAN</strong> </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Total Penarikan Simpanan Pokok</td><td>Rp {{number_format($tariksimpananpokok,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Penarikan Simpanan Wajib</td><td>Rp {{number_format($tariksimpananwajib,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Penarikan Simpanan Sukarela</td><td>Rp {{number_format($tariksimpanansukarela,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Penarikan Simpanan Khusus</td><td>Rp {{number_format($tariksimpanankhusus,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Peminjaman</td><td>Rp {{number_format($totalpinjaman,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Aset Keluar</td><td>Rp {{number_format($totalasetkeluar,'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Saham Dibeli</td><td>Rp {{number_format($totalsahamdibeli,'0','.',',')}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="col-md-6 col-md-offset-3">
  <div class="table-responsive">
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th colspan="2"> <strong>TOTAL</strong> </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Total Pemasukan</td><td>Rp {{number_format(array_sum($totalpemasukan),'0','.',',')}}</td>
        </tr>
        <tr>
          <td>Total Pengeluaran</td><td>Rp {{number_format(array_sum($totalpengeluaran),'0','.',',')}}</td>
        </tr>
        <tr>
          <td> <strong>SELISIH</strong> </td><td> <strong> Rp {{number_format(array_sum($totalpemasukan)-array_sum($totalpengeluaran),'0','.',',')}}</strong></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
  <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
              Mohon Menunggu...<br>
          </div>

      </div>
  </div>
@endsection
