@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Laporan Pembayaran Pinjaman</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-pembayaran-pinjaman')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Pinjaman</label>
        <input type="text" class="form-control floating" name="no_pinjaman" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status Pinjaman</label>
        <select class="select floating" name="status">
          @if($status==1)
          <option value=""> -- Status-- </option>
          <option value="2"> Sudah Bayar </option>
          <option value="1" selected> Belum Bayar </option>
          @elseif($status==2)
          <option value=""> -- Status-- </option>
          <option value="2" selected> Sudah Bayar </option>
          <option value="1"> Belum Bayar </option>
          @else
          <option value=""> -- Status-- </option>
          <option value="2"> Sudah Bayar </option>
          <option value="1"> Belum Bayar </option>
          @endif

        </select>
      </div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2 col-xs-12"><br>
  <!-- <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">
  <!-- <form class="form" action="" method="post" id="export1">
    @csrf
    <input type="hidden" name="no_anggota" value="{{$nomor}}"/>
    <input type="hidden" name="dari" value="{{$from}}"/>
    <input type="hidden" name="sampai" value="{{$to}}"/>
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Pinjam</th>
          <th>Anggota</th>
          <th>Tgl. Bayar</th>
          <th>Jatuh Tempo</th>
          <th align="center">Pinjaman <br>Pokok</th>
          <th align="center">Nilai <br>Bunga</th>
          <th>Total Bayar <br>(Bunga+pokok)</th>
          <th>Status</th>
          <!-- <th class="text-right">Action</th> -->
        </tr>
      </thead>
      <tbody>
        <?php $totoalpokok = 0;
        $totalbunga = 0; ?>
        @foreach($laporans as $key => $laporan)
        <tr>
          <?php $totoalpokok+= $laporan->pokok;
          $totalbunga+=$laporan->bunga;?>
          <!-- <td></td> -->
          <!-- <td></td> -->
          <td>{{$key+1}}.</td>
          <td>{{$laporan->no_pinjamen}}</td>
          <td>{{$laporan->no_anggota}}</td>
          <td>@if($laporan->tgl_bayar==null && date('Ymd') < date('Ymd', strtotime($laporan->tgl_tempo))) xx-xx-xxxx @elseif($laporan->tgl_bayar==null && date('Ymd') > date('Ymd', strtotime($laporan->tgl_tempo))) <label class="btn-danger"> JATUH TEMPO </label> @else {{date('d-m-Y', strtotime($laporan->tgl_bayar))}} @endif</td>
          <td>{{date('d-m-Y', strtotime($laporan->tgl_tempo))}}</td>
          <td>Rp {{number_format($laporan->pokok,0,".",",")}}</td>
          <td>Rp {{number_format($laporan->bunga,0,".",",")}}</td>
          <td>Rp {{number_format($laporan->bayar,0,".",",")}}</td>
          <td>
            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false" style="cursor: not-allowed;">
              @if($laporan->status==1)
              <i class="fa fa-dot-circle-o text-danger"></i> Belum Bayar
              @else
              <i class="fa fa-dot-circle-o text-success"></i> Sudah Bayar
              @endif
            </a>
          </td>

        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Pinjaman Pokok</td>
    <td>: Rp {{number_format($totoalpokok,0,".",",")}}</td>
  </tr>
  <tr>
    <td>Total Bunga</td>
    <td>: Rp {{number_format($totalbunga,0,".",",")}}</td>
  </tr>
  <tr>
    <td>Total Pinjama pokok + Bunga</td>
    <td>: Rp {{number_format($totoalpokok+$totalbunga,0,".",",")}}</td>
  </tr>
</table>

</div>
<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
