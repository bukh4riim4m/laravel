@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Laporan Pinjaman</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-pinjaman')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No.Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status Pinjaman</label>
        <?php $jenis = App\JenisSimpanan::where('aktif', 1)->get(); ?>
        <select class="select floating" name="status_pinjam">
          <option value=""> -- Status-- </option>
          <option value="Sudah Lunas"> Sudah Lunas </option>
          <option value="Belum Lunas"> Belum Lunas </option>

        </select>
      </div>
    </div>

    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2 col-xs-12"><br>
  <!-- <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">
  <!-- <form class="form" action="" method="post" id="export1">
    @csrf
    <input type="hidden" name="no_anggota" value="{{$nomor}}"/>
    <input type="hidden" name="dari" value="{{$from}}"/>
    <input type="hidden" name="sampai" value="{{$to}}"/>
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Pinjam</th>
          <th>Anggota</th>
          <th>Tanggal</th>
          <th>Jatuh Tempo</th>
          <th align="center">Pinjaman</th>
          <th align="center">Nilai Bunga</th>
          <th>Total Pinjaman <br>(Bunga+pokok)</th>
          <th>Status</th>
          <th class="text-right">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $totalpinjam=0;$totalbunga=0;
        ?>
        @foreach($pinjamans as $us)
        <?php $totalpinjam+=$us->nilai_pinjam;
        $totalbunga+=$us->nilai_bunga;
        ?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$us->no_pinjam}}</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{date('d-m-Y', strtotime($us->tgl_trx))}}</td>
          <td>{{date('d-m-Y', strtotime($us->jatuh_tempo))}}</td>
          <td align="center">Rp {{number_format($us->nilai_pinjam,2,".",",")}}<br>( {{$us->kali_angsuran}} Kali )</td>
          <td align="center">Rp {{number_format($us->nilai_bunga,2,".",",")}}<br>( {{$us->bunga_persen}} % )</td>
          <td>Rp {{number_format($us->total_pinjam,2,".",",")}}</td>
          <td>
            <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false" style="cursor: not-allowed;">
              @if($us->status_pinjam=="Belum Lunas")
              <i class="fa fa-dot-circle-o text-danger"></i> Belum Lunas
              @else
              <i class="fa fa-dot-circle-o text-success"></i> Sudah Lunas
              @endif
            </a></td>
          <td style="max-width:70px;" class="text-left">
            <form class="" action="{{route('admin-laporan-pinjaman')}}" method="post">
              <input type="hidden" name="action" value="history" required>
              @csrf
              <input type="hidden" name="no_pinjam" value="{{$us->no_pinjam}}" required>
              <button type="submit" class="btn btn-primary btn-sm rounded text-left" name="button">History Bayar</button>
            </form>
          </td>
        </tr>
        @endforeach
        @if(count($pinjamans) < 1)
        <tr>
          <td colspan="10" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Pinjaman</td>
    <td>: Rp {{number_format($totalpinjam,0,".",",")}}</td>
  </tr>
  <tr>
    <td>Total Bunga</td>
    <td>: Rp {{number_format($totalbunga,0,".",",")}}</td>
  </tr>
  <tr>
    <td>Total Pinjama + Bunga</td>
    <td>: Rp {{number_format($totalpinjam+$totalbunga,0,".",",")}}</td>
  </tr>
</table>

</div>
<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
