@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Laporan Penarikan Simpanan</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" onclick="event.preventDefault();
              document.getElementById('export1').submit();"><i class="fa fa-download"></i> Download</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-penarikan-simpanan')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-3 col-xs-12">
      <div class="form-group form-focus">
        <label class="control-label">No.Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value=""/>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2"><br>
  <!-- <a href="#" class="pull-right">
    <img class="pull-right" width="40px" src="{{url('/images/excel.png')}}" /></a> -->
              </div>
              <form class="form" action="{{route('admin-laporan-penarikan-simpanan')}}" method="post" id="export1">
                @csrf
                <input type="hidden" name="no_anggota" value="{{$nomor}}"/>
                <input type="hidden" name="dari" value="{{$from}}"/>
                <input type="hidden" name="sampai" value="{{$to}}"/>
                <input type="hidden" name="export" value="1"/>
                <input type="hidden" name="action" value="export">
              </form>
</div>

<div class="row">

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>No. Anggota</th>
          <th>Nama Anggota</th>
          <th>Nominal</th>
          <th>Keterangan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($laporans as $key => $laporan)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$laporan->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($laporan->tgl_trx))}}</td>
          <td>{{$laporan->no_anggota}}</td>
          <td>{{$laporan->userId->name}}</td>
          <td>Rp {{number_format($laporan->nominal,0,".",",")}}</td>
          <td>{{$laporan->ket}}</td>
        </tr>
        @endforeach
        @if(count($laporans) < 1 )
        <tr>
          <td colspan="7" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Pinjaman</td>
    <td>: Rp </td>
  </tr>
  <tr>
    <td>Total Bunga</td>
    <td>: Rp </td>
  </tr>
  <tr>
    <td>Total Pinjama + Bunga</td>
    <td>: Rp </td>
  </tr>
</table>

</div>
<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
