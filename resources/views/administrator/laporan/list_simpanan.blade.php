@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">List Simpanan</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded"  onclick="event.preventDefault();
              document.getElementById('export1').submit();"><i class="fa fa-download"></i> Download</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-laporan-list-simpanan')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-4 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Nomor Anggota</label>
				<input class="form-control floating" type="text" name="no_anggota" value="{{$nomor}}"/>
			</div>
		</div>
    <div class="col-sm-4 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Nama Anggota</label>
				<input class="form-control floating" type="text" name="nama_anggota" value="{{$nama}}"/>
			</div>
		</div>

    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2"><br>
  <!-- <a href="#" class="pull-right">
    <img class="pull-right" width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">
  <form class="form" action="{{route('admin-laporan-list-simpanan')}}" method="post" id="export1">
    @csrf
    <input type="hidden" name="no_anggota" value="{{$nomor}}"/>
    <input type="hidden" name="nama_anggota" value="{{$nama}}"/>
    <input type="hidden" name="export" value="1"/>
    <input type="hidden" name="action" value="export">
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No</th>
          <th>No.Anggota</th>
          <th>Nama Anggota</th>
          <th>Total<br>Sim.Pokok</th>
          <th>Total<br>Sim.Wajib</th>
          <th>Total<br>Sim.Sukarela</th>
          <th>Total<br>Sim.Khusus</th>
          <th>Total Simpanan<br>Keseluruhan</th>
        </tr>
      </thead>
      <tbody>
        <?php $total = 0; ?>
        @foreach($datas as $key => $data)
        <?php $total+=$data->saldo;
        $pokok_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',1)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
        $pokok_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',1)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
        $wajib_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',2)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
        $wajib_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',2)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
        $sukarel_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',3)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
        $sukarel_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',3)->where('mutasi','Debet')->where('aktif',1)->sum('nominal');
        $khusus_k = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',4)->where('mutasi','Kredit')->where('aktif',1)->sum('nominal');
        $khusus_d = App\Simpananadmin::where('no_anggota',$data->no_anggota)->where('jenis_simpanan',4)->where('mutasi','Debet')->where('aktif',1)->sum('nominal'); ?>
        <tr>
          <td>{{$key+1}}.</td>
          <td>{{$data->no_anggota}}</td>
          <td>{{$data->name}}</td>
          <td>Rp {{number_format($pokok_k - $pokok_d,0,".",",")}}</td>
          <td>Rp {{number_format($wajib_k - $wajib_d,0,".",",")}}</td>
          <td>Rp {{number_format($sukarel_k - $sukarel_d,0,".",",")}}</td>
          <td>Rp {{number_format($khusus_k - $khusus_d,0,".",",")}}</td>
          <td>Rp {{number_format($data->saldo,0,".",",")}}</td>
        </tr>
        @endforeach
        @if(count($datas) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>

  </div>

</div>
<div class="col-md-5">
<table class="table custom-table datatable">
  <tr>
    <td>Total Simpanan</td>
    <td>: Rp {{number_format($total,0,".",",")}}</td>
  </tr>

</table>

</div>
<div class="coi-md-5">
  <div class="text-right"></div>
</div>

<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>



      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
