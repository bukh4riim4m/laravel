@extends('layouts.admin.appajax')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data SHU</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-data-shu')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="name" value="{{$name}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="C A R I"/>
    </div>
    <div class="col-sm-4 col-xs-12"><br>
    <!-- <a href="#" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                  document.getElementById('exportdata').submit();"/></a> -->
                </div>
  </form>


</div>
<div class="row">
  <!-- <form class="" action="{{url('/administrator/export-data-anggota')}}" method="post" id="exportdata">
    @csrf
    <input type="hidden" name="export" value="1"/>
  </form> -->

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th></th>
          <th>Tgl. Daftar</th>
          <th>No.Anggota</th>
          <th>Nama</th>

          <th>No. Telp</th>
          <th>SHU</th>
          <th>Total Simpanan<br>(Non Khusus)</th>
          <!-- <th>Saldo Transaksi</th> -->
          <!-- <th style="min-width:100px;" class="text-right">Action</th> -->
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td><img class="avatar" src="{{ asset('foto/'.$us->fotodiri) }}" alt=""></td>
          <td>{{date('d-m-Y', strtotime($us->tgl_daftar))}}</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td>{{$us->telp}}</td>
          <td>@if($us->saldo_non_khusus > 0){{number_format(($us->saldo_non_khusus/$totalsimpanannonkhusus)*100,2,".",",")}} @else {{$us->saldo_non_khusus}} @endif % </td>
          <td>Rp {{number_format($us->saldo_non_khusus,0,".",",")}}</td>
          <!-- <td>Rp {{number_format($us->saldotransaksi,0,".",",")}}</td> -->
          <!-- <td style="max-width:80px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anggota{{$us->id}}" id="editanggota{{$us->id}}">Edit</a>
            <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#detail{{$us->id}}">Detail</a>
          </td> -->
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>

    </table>{!! $user->render() !!}


    <!-- <label class="col-lg-3 control-label">Light Logo</label> -->
    <!-- <form class="" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
      @csrf
      <input type="hidden" name="action" value="csv">
  		<div class="col-lg-3"><br>
        <span class="control-label">UPLOAD CSV</span>
          <input class="form-control rounded" type="file" name="upload">
  		</div>
      <div class="col-lg-9"><br>
  		</div>
      <div class="col-lg-2"><br>
          <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
  		</div>
    </form> -->
  </div>
  <div class="row col-md-6 table-responsive">
    <table class="table table-striped custom-table" width="40px">
      <thead>
        <tr>
          <th>Total Simpanan Non Khusus</th>
          <th>: Rp {{number_format($totalsimpanannonkhusus,0,".",",")}}</th>
        </tr>
        <!-- <tr>
          <th>Total Seluruh Simpanan</th>
          <th>: Rp {{number_format($totalsemuasimpanan,0,".",",")}}</th>
        </tr> -->
      </thead>
    </table>
  </div>
</div>
</div>
    </div>
    </div>



      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>

@endsection
