@extends('layouts.admin.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<h4 class="page-title">Kartu Anggota</h4>
						</div>
					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{url('/administrator/kartu-anggota')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">No Anggota</label>
  								<input type="number" class="form-control floating" name="no_anggota" value="{{$anggota}}"/>
  							</div>
  						</div>
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Nama</label>
  								<input type="text" class="form-control floating" name="name" value="{{$nama}}"/>
  							</div>
  						</div>
  						<div class="col-sm-4 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            <style>
              #kartuname {
                position: absolute;
                top:87px;
                color: black;
                font-size: 6px;
                bottom: 100px;
                margin-right: 33px;
                right: 35px;
                overflow: hidden;
              }
              #table_kartu{
                position:relative;
                margin-top: -85px;
                font-size: 10px;
                margin-left: auto;
                margin-right: 13px;
                margin-bottom: auto;
              }
            </style>
            @foreach($users as $user)
						<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget" style="background:#F9F8F8;">
								<div class="profile-imges">
									<!-- <a href="#"  data-toggle="modal" data-target="#lihat">@if($user->fotodiri !==null)<img src="{{url('foto/'.$user->fotodiri)}}" width="23%" style="margin-bottom:-63%;margin-left:69%;margin-right:10%"><center><img src="{{asset('kartu/kartu_nama.png')}}" width="100%" id="img1" style="margin-top:-17px;"/></center>@else <img src="{{asset('kartu/kartu_nama.png')}}" width="100%" id="img1" style="margin-top:1px;"/>@endif</a> -->
                  <table>
                    <tr>
                      <td><img src="{{url('kartu/kartu_nama_ki.png')}}" width="100%" style="border-radius:5px;"/></td>
                    </tr>
                  </table>
                  <table id="table_kartu">
                    <tr>
                      <td align="left">
                        <font>{{ucwords(strtolower($user->name))}}</font></td>
                      <td rowspan="3">&nbsp;&nbsp;</td>
                      <td rowspan="3">
                        <table border="1" style="border:1;border-color:black;">
                        <tr>
                          <td>@if($user->fotodiri !==null)<img src="{{url('foto/'.$user->fotodiri)}}" width="45px"> @else <img src="{{url('kartu/user.jpg')}}" width="45px"> @endif</td>
                        </tr>
                      </table>
                      </td>
                    </tr>
                    <tr>
                      <td width="20px;">
                        <?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($user->no_anggota, "C39") . '" alt="barcode" width="105px" height="30px"/>';//echo DNS1D::getBarcodeHTML($user->no_anggota, "UPCE"); ?>
                      </td>
                    </tr>
                    <tr>
                      <td><font style="font-size:7px;">{{$user->no_anggota}}</font></td>
                    </tr>
                  </table>
								</div>
								<!-- <h4 class="user-name m-t-10 m-b-0 text-ellipsis">{{$user->no_anggota}}</h4>
								<h5 class="user-name m-t-10 m-b-0 text-ellipsis">{{$user->name}}</h5> --><br><br>
								<div class="small text-muted"></div>
								<a onclick="event.preventDefault();
                              document.getElementById('submit{{$user->id}}').submit();" class="btn btn-success btn-sm m-t-10">Lihat</a>
								<a href="#" data-toggle="modal" data-target="#upload{{$user->id}}" class="btn btn-default btn-sm m-t-10">Upload</a>
							</div>
						</div>
            <form class="" action="{{url('/administrator/Download-kartu/'.$user->sequence)}}" method="post" id="submit{{$user->id}}">
              @csrf
            </form>

            @endforeach

					</div>
                </div>
                <?php $editusers = App\User::get(); ?>
                @foreach($users as $edituser)
			<div id="lihat{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">KARTU ANGGOTA</h4>
						</div>
						<div class="modal-body card-box">
							<p>@if($edituser->fotodiri==null)<img src="{{asset('/kartu/kartu_nama_ki.png')}}" width="100%"/>@else <img src="{{url('/foto/'.$edituser->fotodiri)}}" width="20%" style="margin-bottom:-290px;margin-left:17px;margin-right:50px"><img src="{{url('/kartu/kartu_nama_ki.png')}}" width="100%" id="img1" style="margin-top:-20px"/>@endif</p>
							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								<!-- <button type="submit" class="btn btn-danger">Delete</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
      <div id="upload{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">UPLOAD KARTU ANGGOTA</h4>
						</div>
						<div class="modal-body card-box">
              <form class="" action="{{url('/administrator/kartu-anggota')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="ids" value="{{$edituser->id}}">
                <input type="hidden" name="action" value="upload">
                <p>No Anggota : {{$edituser->no_anggota}}</p>
  							<p> <input type="file" name="fotodiri"> </p>
  							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
  								<button type="submit" class="btn btn-success">Upload</button>
  							</div>
              </form>
						</div>
					</div>
				</div>
			</div>
      @endforeach
    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
