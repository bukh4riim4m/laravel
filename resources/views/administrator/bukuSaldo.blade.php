@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @include('flash::message')
<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Mutasi Simpanan</h4>
</div>

</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-history-simpanan')}}" method="post">
    <input type="hidden" name="action" value="cari">
    <input type="hidden" name="jenis_simpanan" value="1">
    @csrf
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$anggota}}" minlength="6" required/>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Dari Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="dari">
          @foreach($tahuns as $thn)
            @if($from == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Sampai Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="sampai">
          @foreach($tahuns as $thn)
            @if($until == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<hr>
<div class="col-md-12">
  @if($cari)
  <div class="card-box m-b-0">
  <div class="row">
    <div class="col-md-12">
      <div class="profile-view">
        <div class="profile-img-wrap">
          <div class="profile-img">
            <a href=""><img class="avatar" src="{{ url('/foto/'.$users->fotodiri) }}" alt=""></a>
          </div>
        </div>
        <div class="profile-basic">
          <div class="row">
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <td>Nomor Anggota<span class="pull-right">:</span></td>
                    <td>@if($users->no_anggota==null) @else {{$users->no_anggota}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap<span class="pull-right">:</span></td>
                    <td>@if($users->name==null) @else {{$users->name}} @endif</td>
                  </tr>
                  <tr>
                    <td>No.KTP<span class="pull-right">:</span></td>
                    <td>@if($users->nik==null) @else {{$users->nik}} @endif</td>
                  </tr>
                  <tr>
                    <td>No.NPWP<span class="pull-right">:</span></td>
                    <td>@if($users->npwp==null) @else {{$users->npwp}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nomor Telpon<span class="pull-right">:</span></td>
                    <td>@if($users->telp==null) @else {{$users->telp}} @endif</td>
                  </tr>



                  <!-- <tr>
                    <td>Tempat Lahir<span class="pull-right">:</span></td>
                    <td>@if($users->tpt_lahir==null) @else {{$users->tpt_lahir}} @endif</td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir	<span class="pull-right">:</span></td>
                    <td>@if($users->tgl_lahir==null) @else {{date('d-m-Y', strtotime($users->tgl_lahir))}} @endif</td>
                  </tr> -->
                </table>
              </div>
            </div>
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">


                  <!-- <tr>
                    <td>Jenis Kelamin	<span class="pull-right">:</span></td>
                    <td>@if($users->jenkel==null) @else {{$users->jenkel}} @endif</td>
                  </tr>
                  <tr>
                    <td>Agama<span class="pull-right">:</span></td>
                    <td>@if($users->agama==null) @else {{$users->agama}} @endif</td>
                  </tr> -->
                  <!-- <tr>
                    <td>Pendidikan<span class="pull-right">:</span></td>
                    <td>@if($users->pendidikan==null) @else {{$users->pendidikan_id->name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Status dalam keluarga	<span class="pull-right">:</span></td>
                    <td>@if($users->statuskeluarga==null) @else {{$users->statuskeluarga}} @endif</td>
                  </tr> -->
                  <tr>
                    <td>Email<span class="pull-right">:</span></td>
                    <td>@if($users->email==null) @else {{$users->email}} @endif</td>
                  </tr>
                  <tr>
                    <td>Propinsi	<span class="pull-right">:</span></td>
                    <td>@if($users->propinsi==null) @else {{$users->propinsi_id->province}} @endif</td>
                  </tr>
                  <tr>
                    <td>Kabupaten	<span class="pull-right">:</span></td>
                    <td>@if($users->propinsi==null) @else {{$users->kabupaten_id->city_name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Alamat <span class="pull-right">:</span></td>
                    <td>@if($users->alamat==null) @else {{$users->alamat}} @endif</td>
                  </tr>
                  <!-- <tr>
                    <td>Komunitas<span class="pull-right">:</span></td>
                    <td>@if($users->komunitas==null) @else {{$users->komunitas_id->name}} @endif</td>
                  </tr> -->
                  <tr>
                    <td><strong>TOTAL SIMPANAN<span class="pull-right">:</span></strong></td>
                    <td><strong>Rp {{number_format($users->saldo,0,".",",")}}</strong></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="card-box tab-box">
</div>
    <div id="tasks" class="tab-pane fade in active">
      <div class="project-task">
        <div class="tabbable">
          <ul class="nav nav-tabs nav-tabs-top nav-justified m-b-0">
            @if($jns ==1)<li class="active">@else<li>@endif
              <a href="#pokok" data-toggle="tab" aria-expanded="true">SIMPANAN POKOK</a></li>
              @if($jns ==2)<li class="active">@else<li>@endif
                <a href="#wajib" data-toggle="tab" aria-expanded="false">SIMPANAN WAJIB</a></li>
              @if($jns ==3)<li class="active">@else<li>@endif
                <a href="#sukarela" data-toggle="tab" aria-expanded="false">SIMPANAN SUKARELA</a></li>
              @if($jns ==4)<li class="active">@else<li>@endif
                <a href="#investasi" data-toggle="tab" aria-expanded="false">SIMPANAN KHUSUS</a></li>

            </ul>
          <div class="tab-content">
          @if($jns ==1)<div class="tab-pane active" id="pokok">@else <div class="tab-pane" id="pokok">@endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                            <th class="text-right">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#tambah_pokok">Tarik Simpanan</a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1;
                          $masuk=0;$keluar=0;$saldos=0;$saldosawal=0;$saldosakhir=0;$maxs=array();$maks=array();
                          ?>
                          @foreach($simpananpokok as $key => $pokokmax)
                            <?php $maks[]= $pokokmax->id; ?>
                          @endforeach
                          @if(count($simpananpokok) > 0)
                          <?php $id_maks = max($maks); ?>
                          @endif
                          @foreach($simpananpokok as $key => $pokok)
                          <?php if ($pokok->mutasi =='Kredit') {
                              $masuk+=$pokok->nominal;
                              if ($key ==0) {
                                  $saldosawal = $pokok->saldo - $pokok->nominal;
                              }
                          } else {
                              $keluar+=$pokok->nominal;
                              if ($key ==0) {
                                  $saldosawal = $pokok->saldo + $pokok->nominal;
                              }
                          };
                          $maxs[]=$pokok->id;
                          $jum_id = max($maxs);
                          $saldosakhir = $pokok->saldo;?>
                          <tr>
                            <td>{{$no++}}.</td>
                            <td>{{date('d-m-Y', strtotime($pokok->tgl_setor))}}</td>
                            <td>{{$pokok->no_trx}}</td>
                            @if($pokok->mutasi =='Debet')
                            <td>Rp {{number_format($pokok->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($pokok->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks == $pokok->id)<strong><font color="green">Rp {{number_format($pokok->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($pokok->saldo,0,".",",")}} @endif</td>
                            <td>{{$pokok->ket}}</td>
                            <td style="max-width:70px;" class="text-right">
                              @if($id_maks == $pokok->id)<a href="editpokok{{$pokok->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#editpokok{{$pokok->id}}">Edit</a>
                              {{--<a href="hapuspokok{{$pokok->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapuspokok{{$pokok->id}}">Hapus</a> --}}@endif
                            </td>
                          </tr>
                          <div id="hapuspokok{{$pokok->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content modal-md">
                                <div class="modal-header">
                                  <h4 class="modal-title">Hapus Simpanan Pokok</h4>
                                </div>
                                <form action="{{route('admin-history-simpanan')}}" method="post">
                                  @csrf
                                  <input type="hidden" name="action" value="hapus">
                                  <input type="hidden" name="ids" value="{{$pokok->id}}">
                                  <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                  <input type="hidden" name="dari" value="{{$from}}">
                                  <input type="hidden" name="sampai" value="{{$until}}">
                                  <div class="modal-body card-box">
                                    <p>Apakah yakin ingin di Hapus ???</p>
                                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div id="editpokok{{$pokok->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <div class="modal-content modal-lg">
                                <div class="modal-header">
                                  <h4 class="modal-title">Edit Simpanan Pokok</h4>
                                </div>
                                <div class="modal-body">
                                  <form class="m-b-30" action="{{route('admin-history-simpanan')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="ids" value="{{$pokok->id}}">
                                    <div class="row">

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nama Anggota<span class="text-danger">*</span></label>
                                          <input class="form-control" type="text" name="no" value="{{$users->name}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                          <input type="hidden" name="dari" value="{{$from}}">
                                          <input type="hidden" name="sampai" value="{{$until}}">
                                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
                                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y', strtotime($pokok->tgl_setor))}}" disabled></div>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 1)->get(); ?>
                                          <select class="select floating" name="jenis_simpanan" disabled>
                                            @foreach($jenis as $jen)
                                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                                          <?php $mutasi = ['Kredit','Debet']; ?>
                                          <select class="select floating" name="mutasi" disabled>
                                            <option value=""> -- Pilih Mutasi -- </option>
                                            @foreach($mutasi as $mut)
                                              @if($mut == $pokok->mutasi)
                                                <option value="{{$mut}}" selected> {{$mut}} </option>
                                              @else
                                              <option value="{{$mut}}"> {{$mut}} </option>
                                              @endif
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="nominal" value="{{$pokok->nominal}}" required>
                                        </div>
                                      </div>
                                      <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Saldo<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="saldo" value="{{$pokok->saldo}}" required>
                                        </div>
                                      </div> -->
                                    <div class="m-t-20 text-center">
                                      <button class="btn btn-primary">SIMPAN</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>

                          @endforeach
                          @if(count($simpananpokok) < 1)
                          <tr>
                            <td colspan="8" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>

              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

            @if($jns ==2)<div class="tab-pane active" id="wajib">@else<div class="tab-pane" id="wajib">@endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                            <th class="text-right">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#tambah_wajib">Tarik Simpanan</a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no2=1;
                          $masuk2=0;$keluar2=0;$saldos2=0;$saldosawal2=0;$saldosakhir2=0;$maxs2=array();$maks2=array();
                          ?>
                          @foreach($simpananwajib as $key => $wajibmaks)
                            <?php $maks2[]= $wajibmaks->id; ?>
                          @endforeach
                          @if(count($simpananwajib) > 0)
                          <?php $id_maks2 = max($maks2); ?>
                          @endif
                          @foreach($simpananwajib as $key => $wajib)
                          <?php if ($wajib->mutasi =='Kredit') {
                              $masuk2+=$wajib->nominal;
                              if ($key ==0) {
                                  $saldosawal2 = $wajib->saldo - $wajib->nominal;
                              }
                          } else {
                              $keluar2+=$wajib->nominal;
                              if ($key ==0) {
                                  $saldosawal2 = $wajib->saldo + $wajib->nominal;
                              }
                          };

                          $saldosakhir2 = $wajib->saldo;?>
                          <tr>
                            <td>{{$no2++}}.</td>
                            <td>{{date('d-m-Y', strtotime($wajib->tgl_setor))}}</td>
                            <td>{{$wajib->no_trx}}</td>
                            @if($wajib->mutasi =='Debet')
                            <td>Rp {{number_format($wajib->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($wajib->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks2 == $wajib->id)<strong><font color="green">Rp {{number_format($wajib->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($wajib->saldo,0,".",",")}} @endif</td>
                            <td>{{$wajib->ket}}</td>
                            <td style="max-width:70px;" class="text-right">
                              @if($id_maks2 == $wajib->id)<a href="editwajib{{$wajib->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#editwajib{{$wajib->id}}">Edit</a>
                              {{--<a href="hapuswajib{{$wajib->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapuswajib{{$wajib->id}}">Hapus</a>--}}@endif
                            </td>
                          </tr>
                          <div id="hapuswajib{{$wajib->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content modal-md">
                                <div class="modal-header">
                                  <h4 class="modal-title">Hapus Data Saldo</h4>
                                </div>
                                <form action="{{route('admin-history-simpanan')}}" method="post">
                                  @csrf
                                  <input type="hidden" name="action" value="hapus">
                                  <input type="hidden" name="ids" value="{{$wajib->id}}">
                                  <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                  <input type="hidden" name="dari" value="{{$from}}">
                                  <input type="hidden" name="sampai" value="{{$until}}">
                                  <div class="modal-body card-box">
                                    <p>Apakah yakin ingin di Hapus ???</p>
                                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div id="editwajib{{$wajib->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <div class="modal-content modal-lg">
                                <div class="modal-header">
                                  <h4 class="modal-title">Edit Simpanan Wajib</h4>
                                </div>
                                <div class="modal-body">
                                  <form class="m-b-30" action="{{route('admin-history-simpanan')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="ids" value="{{$wajib->id}}">
                                    <div class="row">
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nama Anggota<span class="text-danger">*</span></label>
                                          <input class="form-control" type="text" name="no" value="{{$users->name}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"disabled>
                                          <input type="hidden" name="dari" value="{{$from}}">
                                          <input type="hidden" name="sampai" value="{{$until}}">
                                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
                                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y', strtotime($wajib->tgl_setor))}}" disabled></div>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 2)->get(); ?>
                                          <select class="select floating" name="jenis_simpanan" disabled>
                                            @foreach($jenis as $jen)
                                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                                          <?php $mutasi = ['Kredit','Debet']; ?>
                                          <select class="select floating" name="mutasi" disabled>
                                            <option value=""> -- Pilih Mutasi -- </option>
                                            @foreach($mutasi as $mut)
                                              @if($mut == $wajib->mutasi)
                                                <option value="{{$mut}}" selected> {{$mut}} </option>
                                              @else
                                              <option value="{{$mut}}"> {{$mut}} </option>
                                              @endif
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="nominal" value="{{$wajib->nominal}}" required>
                                        </div>
                                      </div>
                                      <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Saldo<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="saldo" value="{{$wajib->saldo}}" required>
                                        </div>
                                      </div> -->
                                    <div class="m-t-20 text-center">
                                      <button class="btn btn-primary">SIMPAN</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @if(count($simpananwajib) < 1)
                          <tr>
                            <td colspan="8" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                </div>
              </div>
            </div>
            <div class="row col-md-6 table-responsive">
              <table class="table table-striped custom-table" width="40px">
                <thead>
                  <tr>
                    <th><strong>Saldo Awal</strong></th>
                    <th><strong>: Rp {{number_format($saldosawal2,0,".",",")}}</strong></th>
                  </tr>
                  <tr>
                    <th>Total Kredit</th>
                    <th>: Rp {{number_format($masuk2,0,".",",")}}</th>
                  </tr>
                  <tr>
                    <th>Total Debet</th>
                    <th>: Rp {{number_format($keluar2,0,".",",")}}</th>
                  </tr>
                  <tr>
                    <th><strong><font color="green">Saldo Akhir</font></strong></th>
                    <th><strong><font color="green">: Rp {{number_format($saldosakhir2,0,".",",")}}</font></strong></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          @if($jns ==3)<div class="tab-pane active" id="sukarela">@else<div class="tab-pane" id="sukarela">@endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                            <th class="text-right">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#tambah_sukarela">Tarik Simpanan</a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no3=1;
                          $masuk3=0;$keluar3=0;$saldos3=0;$saldosawal3=0;$saldosakhir3=0;$maxs3=array();$maks3=array();
                          ?>
                          @foreach($simpanansukarela as $key => $sukarelamaks)
                            <?php $maks3[]= $sukarelamaks->id; ?>
                          @endforeach
                          @if(count($simpanansukarela) > 0)
                          <?php $id_maks3 = max($maks3); ?>
                          @endif
                          @foreach($simpanansukarela as $key => $sukarela)
                          <?php if ($sukarela->mutasi =='Kredit') {
                              $masuk3+=$sukarela->nominal;
                              if ($key ==0) {
                                  $saldosawal3 = $sukarela->saldo - $sukarela->nominal;
                              }
                          } else {
                              $keluar3+=$sukarela->nominal;
                              if ($key ==0) {
                                  $saldosawal3 = $sukarela->saldo + $sukarela->nominal;
                              }
                          };

                          $saldosakhir3 = $sukarela->saldo;?>
                          <tr>
                            <td>{{$no3++}}.</td>
                            <td>{{date('d-m-Y', strtotime($sukarela->tgl_setor))}}</td>
                            <td>{{$sukarela->no_trx}}</td>
                            @if($sukarela->mutasi =='Debet')
                            <td>Rp {{number_format($sukarela->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($sukarela->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks3 == $sukarela->id)<strong><font color="green">Rp {{number_format($sukarela->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($sukarela->saldo,0,".",",")}} @endif</td>
                            <td>{{$sukarela->ket}}</td>
                            <td style="max-width:70px;" class="text-right">
                              @if($id_maks3 == $sukarela->id)<a href="editsukarela{{$sukarela->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_sukarela{{$sukarela->id}}">Edit</a>
                              {{--<a href="hapussukarela{{$sukarela->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_sukarela{{$sukarela->id}}">Hapus</a> --}}@endif
                            </td>
                          </tr>
                          <div id="hapus_sukarela{{$sukarela->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content modal-md">
                                <div class="modal-header">
                                  <h4 class="modal-title">Hapus Data Saldo</h4>
                                </div>
                                <form action="{{route('admin-history-simpanan')}}" method="post">
                                  @csrf
                                  <input type="hidden" name="action" value="hapus">
                                  <input type="hidden" name="ids" value="{{$sukarela->id}}">
                                  <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                  <input type="hidden" name="dari" value="{{$from}}">
                                  <input type="hidden" name="sampai" value="{{$until}}">
                                  <div class="modal-body card-box">
                                    <p>Apakah yakin ingin di Hapus ???</p>
                                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div id="edit_sukarela{{$sukarela->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <div class="modal-content modal-lg">
                                <div class="modal-header">
                                  <h4 class="modal-title">Edit Simpanan Sukarela</h4>
                                </div>
                                <div class="modal-body">
                                  <form class="m-b-30" action="{{route('admin-history-simpanan')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="ids" value="{{$sukarela->id}}">
                                    <div class="row">

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                          <input type="hidden" name="dari" value="{{$from}}">
                                          <input type="hidden" name="sampai" value="{{$until}}">
                                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nama Anggota<span class="text-danger">*</span></label>
                                          <input class="form-control" type="text" name="no" value="{{$users->name}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
                                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y', strtotime($sukarela->tgl_setor))}}" disabled></div>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 3)->get(); ?>
                                          <select class="select floating" name="jenis_simpanan" disabled>
                                            @foreach($jenis as $jen)
                                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                                          <?php $mutasi = ['Kredit','Debet']; ?>
                                          <select class="select floating" name="mutasi" disabled>
                                            <option value=""> -- Pilih Mutasi -- </option>
                                            @foreach($mutasi as $mut)
                                              @if($mut == $sukarela->mutasi)
                                                <option value="{{$mut}}" selected> {{$mut}} </option>
                                              @else
                                              <option value="{{$mut}}"> {{$mut}} </option>
                                              @endif
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="nominal" value="{{$sukarela->nominal}}" required>
                                        </div>
                                      </div>
                                      <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Saldo<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="saldo" value="{{$sukarela->saldo}}" required>
                                        </div>
                                      </div> -->
                                    <div class="m-t-20 text-center">
                                      <button class="btn btn-primary">SIMPAN</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @if(count($simpanansukarela) < 1)
                          <tr>
                            <td colspan="8" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal3,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk3,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar3,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir3,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            @if($jns ==4)<div class="tab-pane active" id="investasi">@else<div class="tab-pane" id="investasi">@endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Nomor Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                            <th class="text-right">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#tambah_investasi">Tarik Simpanan</a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no4=1;
                          $masuk4=0;$keluar4=0;$saldos4=0;$saldosawal4=0;$saldosakhir4=0;$maxs4=array();$maks4=array();
                          ?>
                          @foreach($simpananinvestasi as $key => $investasimaks)
                            <?php $maks4[]= $investasimaks->id; ?>
                          @endforeach
                          @if(count($simpananinvestasi) > 0)
                          <?php $id_maks4 = max($maks4); ?>
                          @endif
                          @foreach($simpananinvestasi as $key => $investasi)
                          <?php if ($investasi->mutasi =='Kredit') {
                              $masuk4+=$investasi->nominal;
                              if ($key ==0) {
                                  $saldosawal4 = $investasi->saldo - $investasi->nominal;
                              }
                          } else {
                              $keluar4+=$investasi->nominal;
                              if ($key ==0) {
                                  $saldosawal4 = $investasi->saldo + $investasi->nominal;
                              }
                          };

                          $saldosakhir4 = $investasi->saldo;?>
                          <tr>
                            <td>{{$no4++}}.</td>
                            <td>{{date('d-m-Y', strtotime($investasi->tgl_setor))}}</td>
                            <td>{{$investasi->no_trx}}</td>
                            @if($investasi->mutasi =='Debet')
                            <td>Rp {{number_format($investasi->nominal,0,".",",")}}</td>
                            <td>-</td>
                            @else
                            <td>-</td>
                            <td>Rp {{number_format($investasi->nominal,0,".",",")}}</td>
                            @endif
                            <td>@if($id_maks4 == $investasi->id)<strong><font color="green">Rp {{number_format($investasi->saldo,0,".",",")}}</font></strong>@else Rp {{number_format($investasi->saldo,0,".",",")}} @endif</td>
                            <td>{{$investasi->ket}}</td>
                            <td style="max-width:70px;" class="text-right">
                              @if($id_maks4 == $investasi->id)<a href="editinvestasi{{$investasi->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_investasi{{$investasi->id}}">Edit</a>
                              {{--<a href="hapusinvestasi{{$investasi->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_investasi{{$investasi->id}}">Hapus</a>--}}@endif
                            </td>
                          </tr>
                          <div id="hapus_investasi{{$investasi->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content modal-md">
                                <div class="modal-header">
                                  <h4 class="modal-title">Hapus Simpanan Investasi</h4>
                                </div>
                                <form action="{{route('admin-history-simpanan')}}" method="post">
                                  @csrf
                                  <input type="hidden" name="action" value="hapus">
                                  <input type="hidden" name="ids" value="{{$investasi->id}}">
                                  <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                  <input type="hidden" name="dari" value="{{$from}}">
                                  <input type="hidden" name="sampai" value="{{$until}}">
                                  <div class="modal-body card-box">
                                    <p>Apakah yakin ingin di Hapus ???</p>
                                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <div id="edit_investasi{{$investasi->id}}" class="modal custom-modal fade" role="dialog">
                            <div class="modal-dialog">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <div class="modal-content modal-lg">
                                <div class="modal-header">
                                  <h4 class="modal-title">Edit Simpanan Investasi</h4>
                                </div>
                                <div class="modal-body">
                                  <form class="m-b-30" action="{{route('admin-history-simpanan')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="action" value="edit">
                                    <input type="hidden" name="ids" value="{{$investasi->id}}">
                                    <div class="row">

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                                          <input type="hidden" name="dari" value="{{$from}}">
                                          <input type="hidden" name="sampai" value="{{$until}}">
                                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Nama Anggota<span class="text-danger">*</span></label>
                                          <input class="form-control" type="text" name="no" value="{{$users->name}}" disabled>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
                                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y', strtotime($investasi->tgl_setor))}}" disabled></div>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 4)->get(); ?>
                                          <select class="select floating" name="jenis_simpanan" disabled>
                                            @foreach($jenis as $jen)
                                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                                          <?php $mutasi = ['Kredit','Debet']; ?>
                                          <select class="select floating" name="mutasi" disabled>
                                            <option value=""> -- Pilih Mutasi -- </option>
                                            @foreach($mutasi as $mut)
                                              @if($mut == $investasi->mutasi)
                                                <option value="{{$mut}}" selected> {{$mut}} </option>
                                              @else
                                              <option value="{{$mut}}"> {{$mut}} </option>
                                              @endif
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="nominal" value="{{$investasi->nominal}}" required>
                                        </div>
                                      </div>
                                      <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Saldo<span class="text-danger">*</span></label>
                                          <input class="form-control" type="number" name="saldo" value="{{$investasi->saldo}}" required>
                                        </div>
                                      </div> -->
                                    <div class="m-t-20 text-center">
                                      <button class="btn btn-primary">SIMPAN</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @if(count($simpananinvestasi) < 1)
                          <tr>
                            <td colspan="8" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal4,0,".",",")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($masuk4,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($keluar4,0,".",",")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir4,0,".",",")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>



        </div>
      </div>
    </div>

          <div id="tambah_pokok" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="modal-content modal-lg">
                <div class="modal-header">
                  <h4 class="modal-title">Tarik Simpanan Pokok</h4>
                </div>
                <div class="modal-body">
                  <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
                    @csrf
                    <input type="hidden" name="action" value="tambah">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                          <input type="hidden" name="dari" value="{{$from}}">
                          <input type="hidden" name="sampai" value="{{$until}}">
                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 1)->get(); ?>
                          <select class="select floating" name="jenis_simpanan" required>
                            @foreach($jenis as $jen)
                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                          <?php $mutasi = ['Debet']; ?>
                          <select class="select floating" name="mutasi" required>
                            @foreach($mutasi as $mut)
                                <option value="{{$mut}}"> {{$mut}} </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
                        var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
                        angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
                        }
                        function rupiah(){ var target_donasi = document.getElementById("nominal1").value; var rupiah = convertToRupiah(target_donasi);
                        document.getElementById("nominal1").value = rupiah; }
                        </script>
                        <script> function convertToRupiah (objek) {
                         separator = ",";
                         a = objek.value;
                         b = a.replace(/[^Rp \d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                           j = j + 1;
                          if (((j % 3) == 1) && (j != 1)) {
                             c = b.substr(i-1,1) + separator + c;
                           } else {
                             c = b.substr(i-1,1) + c;
                           }
                         } objek.value = c;
                        }
                 </script>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                          <input class="form-control" type="text" onkeyup="convertToRupiah(this)" id="nominal1" placeholder="Rp 0" name="nominal" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Tarik Dari Kas / Bank <span class="text-danger">*</span></label>
                          <?php $saldobanks = App\SaldoKasbank::get(); ?>
                          <select class="select form-control" name="kasbank">
                            <option value="">Pilih Kas / Bank</option>
                            @foreach($saldobanks as $saldobank)
                            <option value="{{$saldobank->id}}">Tarik Dari {{$saldobank->name}} (Saldo : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                          <textarea class="form-control" type="text" name="ket" required></textarea>
                        </div>
                      </div>
                    <div class="m-t-20 text-center">
                      <button class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
          <div id="tambah_wajib" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="modal-content modal-lg">
                <div class="modal-header">
                  <h4 class="modal-title">Tarik Simpanan Wajib</h4>
                </div>
                <div class="modal-body">
                  <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
                    @csrf
                    <input type="hidden" name="action" value="tambah">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                          <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                          <input type="hidden" name="dari" value="{{$from}}">
                          <input type="hidden" name="sampai" value="{{$until}}">
                          <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                          <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 2)->get(); ?>
                          <select class="select floating" name="jenis_simpanan" required>
                            @foreach($jenis as $jen)
                                <option value="{{$jen->id}}"> {{$jen->name}} </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                          <?php $mutasi = ['Debet']; ?>
                          <select class="select floating" name="mutasi" required>
                            @foreach($mutasi as $mut)
                                <option value="{{$mut}}"> {{$mut}} </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
                        var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
                        angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
                        }
                        function rupiah(){ var target_donasi = document.getElementById("nominal2").value; var rupiah = convertToRupiah(target_donasi);
                        document.getElementById("nominal2").value = rupiah; }
                        </script>
                        <script> function convertToRupiah (objek) {
                         separator = ",";
                         a = objek.value;
                         b = a.replace(/[^Rp \d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                           j = j + 1;
                          if (((j % 3) == 1) && (j != 1)) {
                             c = b.substr(i-1,1) + separator + c;
                           } else {
                             c = b.substr(i-1,1) + c;
                           }
                         } objek.value = c;
                        }
                 </script>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                          <input class="form-control" type="text" onkeyup="convertToRupiah(this)" name="nominal" id="nominal2" placeholder="Rp 0" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label class="control-label">Tarik Dari Kas / Bank <span class="text-danger">*</span></label>
                          <?php $saldobanks = App\SaldoKasbank::get(); ?>
                          <select class="select form-control" name="kasbank">
                            <option value="">Pilih Kas / Bank</option>
                            @foreach($saldobanks as $saldobank)
                            <option value="{{$saldobank->id}}">Tarik Dari {{$saldobank->name}} (Saldo : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                          <textarea class="form-control" type="text" name="ket" required></textarea>
                        </div>
                      </div>
                    <div class="m-t-20 text-center">
                      <button class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          </div>
        </div>
      </div>
    </div>

    <div id="tambah_sukarela" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tarik Simpanan Sukarela</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="tambah">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                    <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                    <input type="hidden" name="dari" value="{{$from}}">
                    <input type="hidden" name="sampai" value="{{$until}}">
                    <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                    <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 3)->get(); ?>
                    <select class="select floating" name="jenis_simpanan" required>
                      @foreach($jenis as $jen)
                          <option value="{{$jen->id}}"> {{$jen->name}} </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                    <?php $mutasi = ['Debet']; ?>
                    <select class="select floating" name="mutasi" required>
                      @foreach($mutasi as $mut)
                          <option value="{{$mut}}"> {{$mut}} </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
                  var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
                  angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
                  }
                  function rupiah(){ var target_donasi = document.getElementById("nominal3").value; var rupiah = convertToRupiah(target_donasi);
                  document.getElementById("nominal3").value = rupiah; }
                  </script>
                  <script> function convertToRupiah (objek) {
                   separator = ",";
                   a = objek.value;
                   b = a.replace(/[^Rp \d]/g,"");
                   c = "";
                   panjang = b.length;
                   j = 0; for (i = panjang; i > 0; i--) {
                     j = j + 1;
                    if (((j % 3) == 1) && (j != 1)) {
                       c = b.substr(i-1,1) + separator + c;
                     } else {
                       c = b.substr(i-1,1) + c;
                     }
                   } objek.value = c;
                  }
           </script>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" id="nominal3" placeholder="Rp 0" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Tarik Dari Kas / Bank <span class="text-danger">*</span></label>
                    <?php $saldobanks = App\SaldoKasbank::get(); ?>
                    <select class="select form-control" name="kasbank">
                      <option value="">Pilih Kas / Bank</option>
                      @foreach($saldobanks as $saldobank)
                      <option value="{{$saldobank->id}}">Tarik Dari {{$saldobank->name}} (Saldo : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                    <textarea class="form-control" type="text" name="ket" required></textarea>
                  </div>
                </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


    </div>
  </div>
</div>
</div>
<div id="tambah_investasi" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tarik Simpanan Investasi</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="tambah">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                <input type="hidden" name="dari" value="{{$from}}">
                <input type="hidden" name="sampai" value="{{$until}}">
                <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 4)->get(); ?>
                <select class="select floating" name="jenis_simpanan" required>
                  @foreach($jenis as $jen)
                      <option value="{{$jen->id}}"> {{$jen->name}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                <?php $mutasi = ['Debet']; ?>
                <select class="select floating" name="mutasi" required>
                  @foreach($mutasi as $mut)
                      <option value="{{$mut}}"> {{$mut}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
              var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
              angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
              }
              function rupiah(){ var target_donasi = document.getElementById("nominal4").value; var rupiah = convertToRupiah(target_donasi);
              document.getElementById("nominal4").value = rupiah; }
              </script>
              <script> function convertToRupiah (objek) {
               separator = ",";
               a = objek.value;
               b = a.replace(/[^Rp \d]/g,"");
               c = "";
               panjang = b.length;
               j = 0; for (i = panjang; i > 0; i--) {
                 j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                   c = b.substr(i-1,1) + separator + c;
                 } else {
                   c = b.substr(i-1,1) + c;
                 }
               } objek.value = c;
              }
       </script>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" id="nominal4" placeholder="Rp 0" required>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tarik Dari Kas / Bank <span class="text-danger">*</span></label>
                <?php $saldobanks = App\SaldoKasbank::get(); ?>
                <select class="select form-control" name="kasbank">
                  <option value="">Pilih Kas / Bank</option>
                  @foreach($saldobanks as $saldobank)
                  <option value="{{$saldobank->id}}">Tarik Dari {{$saldobank->name}} (Saldo : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div>
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>
<div id="tambah_wakaf" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Simpanan Wakaf</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="tambah">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                <input type="hidden" name="dari" value="{{$from}}">
                <input type="hidden" name="sampai" value="{{$until}}">
                <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 5)->get(); ?>
                <select class="select floating" name="jenis_simpanan" required>
                  @foreach($jenis as $jen)
                      <option value="{{$jen->id}}"> {{$jen->name}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                <?php $mutasi = ['Kredit','Debet']; ?>
                <select class="select floating" name="mutasi" required>
                  <option value=""> -- Pilih Mutasi -- </option>
                  @foreach($mutasi as $mut)
                      <option value="{{$mut}}"> {{$mut}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                <input class="form-control" type="number" name="nominal" required>
              </div>
            </div>
            <!-- <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div> -->
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>

<div id="tambah_infaq" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Simpanan Infaq</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="tambah">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                <input type="hidden" name="dari" value="{{$from}}">
                <input type="hidden" name="sampai" value="{{$until}}">
                <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 6)->get(); ?>
                <select class="select floating" name="jenis_simpanan" required>
                  @foreach($jenis as $jen)
                      <option value="{{$jen->id}}"> {{$jen->name}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                <?php $mutasi = ['Kredit','Debet']; ?>
                <select class="select floating" name="mutasi" required>
                  <option value=""> -- Pilih Mutasi -- </option>
                  @foreach($mutasi as $mut)
                      <option value="{{$mut}}"> {{$mut}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                <input class="form-control" type="number" name="nominal" required>
              </div>
            </div>
            <!-- <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div> -->
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>

<div id="tambah_shu" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Simpanan SHU</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="tambah">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                <input type="hidden" name="dari" value="{{$from}}">
                <input type="hidden" name="sampai" value="{{$until}}">
                <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 7)->get(); ?>
                <select class="select floating" name="jenis_simpanan" required>
                  @foreach($jenis as $jen)
                      <option value="{{$jen->id}}"> {{$jen->name}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                <?php $mutasi = ['Kredit','Debet']; ?>
                <select class="select floating" name="mutasi" required>
                  <option value=""> -- Pilih Mutasi -- </option>
                  @foreach($mutasi as $mut)
                      <option value="{{$mut}}"> {{$mut}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                <input class="form-control" type="number" name="nominal" required>
              </div>
            </div>
            <!-- <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div> -->
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>

<div id="tambah_lain" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Simpanan Lain-Lain</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-tarik-simpanan')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="tambah">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Tanggal <span class="text-danger">*</span></label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_setor" value="{{date('d-m-Y')}}" required></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                <input type="hidden" name="no_anggota" value="{{$users->no_anggota}}"required>
                <input type="hidden" name="dari" value="{{$from}}">
                <input type="hidden" name="sampai" value="{{$until}}">
                <input class="form-control" type="text" name="no" value="{{$users->no_anggota}}" disabled>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <?php $jenis = App\JenisSimpanan::where('aktif', 1)->where('id', 8)->get(); ?>
                <select class="select floating" name="jenis_simpanan" required>
                  @foreach($jenis as $jen)
                      <option value="{{$jen->id}}"> {{$jen->name}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
                <?php $mutasi = ['Kredit','Debet']; ?>
                <select class="select floating" name="mutasi" required>
                  <option value=""> -- Pilih Mutasi -- </option>
                  @foreach($mutasi as $mut)
                      <option value="{{$mut}}"> {{$mut}} </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
                <input class="form-control" type="number" name="nominal" required>
              </div>
            </div>
            <!-- <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
                <textarea class="form-control" type="text" name="ket" required></textarea>
              </div>
            </div> -->
          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


</div>
</div>
</div>
</div>
@endif

</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
