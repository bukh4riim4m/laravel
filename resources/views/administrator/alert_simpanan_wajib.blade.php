@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @include('flash::message')
<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Simpanan Wajib Jatuh Tempo</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" onclick="event.preventDefault();
                document.getElementById('exportdata').submit();"><i class="fa fa-download"></i> Download</a>
  <!-- <div class="view-icons">
    <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>

  </div> -->
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-simpanan-jatuh-tempo')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No.Anggota</label>
        <input type="number" class="form-control floating" name="no_anggota" value="{{$no_anggota}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="nama_anggota" value="{{$nama_anggota}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-3 col-xs-12"><br>
    <form class="" action="{{route('admin-simpanan-jatuh-tempo')}}" method="post" id="exportdata">
      @csrf
      <input type="hidden" name="action" value="export">
      <!-- <input type="hidden" name="tahun" value=""> -->
      <input type="hidden" name="no_anggota" value="{{$no_anggota}}">
      <input type="hidden" name="nama_anggota" value="{{$nama_anggota}}">
      <!-- <input type="hidden" name="status" value=""> -->
      <!-- <input type="hidden" name="bulan" value=""> -->
      <input type="hidden" name="excel" value="1">
    </form>
  <!-- <a href="{{url('administrator/simpanan-wajib-belum-bayar')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export').submit();"/></a> -->
              </div>
</div>

<div class="row">
<div class="col-md-12">

  <div class="table-responsive">
<br>
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Angota</th>
          <th>Nama Anggota</th>
          <th>Setor Terakhir</th>
          <th>Terakhir Bayar</th>
          <th>Tgl. Jatuh Tempo</th>
          <th>Status</th>
          <!-- <th>Status JT</th> -->
        </tr>
      </thead>
      <tbody>
        @foreach($users as $key => $user)
        <tr>
          <td>{{$key+1}}.</td>
            @if($wajib = App\Simpananadmin::where('no_anggota',$user->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->orderBy('id','DESC')->first())
              <td>@if($wajib->jth_tempo > date('Y-m-d')) <font style="color:green;"> @else <font style="color:red;"> @endif {{$user->no_anggota}}</font></td>
              <td>@if($wajib->jth_tempo > date('Y-m-d')) <font style="color:green;"> @else <font style="color:red;"> @endif {{$user->name}}</font></td>
              <td><font>Rp {{number_format($wajib->nominal)}}</font></td>
              <td><font>{{date('d-m-Y', strtotime($wajib->tgl_setor))}}</font></td>
              <td>@if($wajib->jth_tempo > date('Y-m-d')) <font style="color:green;"> @else <font style="color:red;"> @endif {{date('d-m-Y', strtotime($wajib->jth_tempo))}}</label></td>


              <td>
                @if($wajib->jth_tempo > date('Y-m-d'))<a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-dot-circle-o text-success"></i> Sudah Bayar</a> @else <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="modal" data-target="#bayar{{$user->id}}"><i class="fa fa-dot-circle-o text-danger"></i> Belum Bayar</a> @endif</td>
              <!-- <td><span class="label label-warning-border">OK</span> </td> -->
            @else
              <td><font style="color:red;">{{$user->no_anggota}}</font></td>
              <td><font style="color:red;">{{$user->name}}</font></td>
              <td><font style="color:red;">Belum Pernah</font></td>
              <td><font style="color:red;">##-##-####</font></td>
              <td><font style="color:red;">##-##-####</font></td>

              <td>
                <a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="modal" data-target="#bayar{{$user->id}}"><i class="fa fa-dot-circle-o text-danger"></i> Belum Bayar</a>

                <!-- <span class="label label-danger-border">Belum Bayar</span> -->
              </td>
              <!-- <td><span class="label label-warning-border">OK</span> </td> -->
            @endif

        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="coi-md-5">
      <div class="text-right">{{$users->links()}}</div>
    </div>
  </div>
</div>

<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
@foreach($users as $key => $update)
<div id="bayar{{$update->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Transaksi Simpanan Wajib</h4>
      </div>
      <form action="{{route('admin-simpan-pokok')}}" method="post">
        @csrf
        <input type="hidden" name="action" value="tambah">
        <input type="hidden" name="no_anggota" value="{{$update->no_anggota}}">
        <input type="hidden" name="jenis_simpanan" value="2">
        <input type="hidden" name="mutasi" value="Kredit">
        <input type="hidden" name="tgl_setor" value="{{date('Y-m-d')}}">

        <div class="modal-body card-box">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Tgl. Bayar <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="" value="{{date('d-m-Y')}}" disabled>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">No. Anggota <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="no_anggota" value="{{$update->no_anggota}}" disabled>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Nama Anggota <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="nama_anggota" value="{{$update->name}}" disabled>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="" value="Simpanan Wajib" disabled>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Nominal Simpanan <span class="text-danger">*</span></label>
                <input type="text" class="form-control"  onkeyup="convertToRupiah(this)" name="nominal" value="{{old('nominal')}}" id="nominal{{$update->id}}" placeholder="Rp 0">
              </div>
            </div>
            <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
      angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
      }
      function rupiah(){ var target_donasi = document.getElementById("nominal{{$update->id}}").value; var rupiah = convertToRupiah(target_donasi);
      document.getElementById("nominal{{$update->id}}").value = rupiah; }
      </script>
      <script> function convertToRupiah (objek) {
       separator = ",";
       a = objek.value;
       b = a.replace(/[^Rp \d]/g,"");
       c = "";
       panjang = b.length;
       j = 0; for (i = panjang; i > 0; i--) {
         j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
           c = b.substr(i-1,1) + separator + c;
         } else {
           c = b.substr(i-1,1) + c;
         }
       } objek.value = c;
      }
       </script>


            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pembayaran Masuk<span class="text-danger">*</span></label>
                <?php $saldobanks = App\SaldoKasbank::get(); ?>
                <select class="select form-control" name="kasbank">
                  <option value="">Pilih</option>
                  @foreach($saldobanks as $saldobank)
                    @if($saldobank->name == old('kasbank'))
                    <option value="{{$saldobank->name}}" selected>Uang {{$saldobank->name}} (Sisa : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                    @else
                    <option value="{{$saldobank->name}}">Uang {{$saldobank->name}} (Sisa : Rp {{number_format($saldobank->saldo,0,'.',',')}})</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Keterangan Tambahan <span class="text-danger">*</span></label>
                <textarea class="form-control" name="ket" rows="3" cols="60"></textarea>
              </div>
            </div>
          </div>

          <!-- <p>Apakah yakin ingin di Hapus : Rp {{number_format($update->nominal,0,".",",")}} ???</p> -->

          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-success">Proses</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach
</div>

    </div>


      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
