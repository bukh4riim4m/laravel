@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
@include('flash::message')
      <div class="row">
      <div class="col-xs-4">
        <h4 class="page-title">Jenis Masukan/Keluaran</h4>
      </div>
      <div class="col-xs-8 text-right m-b-30">
        <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#transaksiaset"><i class="fa fa-plus"></i> Tambah Induk </a>
        <div class="view-icons">
          <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

        </div>
      </div>
      </div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-jenis-pemasukan-pengeluaran')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-3 col-md-6 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Jenis Masukan/Keluaran</label>
        <?php $jmks = App\JenisMasukanKeluaran::where('aktif',1)->where('status','induk')->get(); ?>
        <select class="select floating" name="jenis">
          <option value="">-- Semua -- </option>
          @foreach($jmks as $jmk)
          @if($jenises == $jmk->id)
          <option value="{{$jmk->id}}" selected> {{$jmk->name}} </option>
          @else
          <option value="{{$jmk->id}}"> {{$jmk->name}} </option>
          @endif
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-sm-6 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-2 col-xs-12"><br>
  <!-- <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a> -->
              </div>
</div>

<div class="row">
<div class="col-md-12">

  <div class="table-responsive">

    <table class="table  custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Jenis Masukan/Keluaran</th>
          <th>Status</th>
          <th class="text-right">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($datas as $key => $data)
        <tr>
          <td><label>{{$key+1}}.</label></td>
          <td><label>{{$data->name}}</label></td>
          <td><label>{{$data->status}}</label></td>
          <td class="text-right">
            <a href="#" class="btn btn-success btn-sm rounded" data-toggle="modal" data-target="#tambahanak{{$data->id}}">Tambah Anak</a>
            <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#editinduk{{$data->id}}">Edit</a>
            <a href="#" class="btn btn-danger btn-sm rounded" data-toggle="modal" data-target="#hapus{{$data->id}}">Hapus</a>
          </td>
        </tr>
          @if($anak = App\JenisMasukanKeluaran::where('status','anak')->where('id_induk',$data->id)->first())
          <?php $anaks = App\JenisMasukanKeluaran::where('status','anak')->where('id_induk',$data->id)->where('aktif',1)->get(); ?>
              @foreach($anaks as $keys=> $anak)
              <tr class="responsive" style="background:#F0FFF0;">
                <td></td>
                <td>{{$keys+1}}.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$anak->name}}</td>
                <td>{{$anak->status}}</td>
                <td class="text-right">
                  <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#editinduk{{$anak->id}}">Edit</a>
                  <a href="#" class="btn btn-danger btn-sm rounded" data-toggle="modal" data-target="#hapus{{$anak->id}}">Hapus</a>

                </td>
              </tr>
              @endforeach
          @endif
        @endforeach
        @if(count($datas) < 1)
        <tr>
          <td colspan="4" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<!-- <div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Semua Aset Masuk</td>
    <td>: Rp </td>
  </tr>
</table>

</div> -->
</div>
@foreach($alldatas as $key => $editdata)
<div id="tambahanak{{$editdata->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Anak</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-jenis-pemasukan-pengeluaran')}}" method="post">
          <input type="hidden" name="action" value="tambahinduk" required>
          <input type="hidden" name="status" value="Anak" required>
          <input type="hidden" name="id_induk" value="{{$editdata->id}}" required>
          @csrf
          <div class="row">

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Induk <span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" name="jenis" value="{{$editdata->name}}" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Anak <span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" name="jenis">
              </div>
            </div>

          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">PROSES</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="editinduk{{$editdata->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Edit Jenis Masukan/Keluaran</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-jenis-pemasukan-pengeluaran')}}" method="post">
          <input type="hidden" name="action" value="edit" required>
          <input type="hidden" name="ids" value="{{$editdata->id}}" required>
          @csrf
          <div class="row">

            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Jenis Masukan/Keluaran <span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" value="{{$editdata->name}}" name="jenis">
              </div>
            </div>

          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">PROSES</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<div id="hapus{{$editdata->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Jenis</h4>
      </div>
      <form action="{{route('admin-jenis-pemasukan-pengeluaran')}}" method="post">
        @csrf
        <input type="hidden" name="action" value="hapus">
        <input type="hidden" name="ids" value="{{$editdata->id}}">
        <div class="modal-body card-box">
          <p>Yakin Hapus : <strong>{{$editdata->name}} ({{$editdata->status}})</strong> ???</p>
          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-danger">Proses</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach
<div id="transaksiaset" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Induk</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{route('admin-jenis-pemasukan-pengeluaran')}}" method="post">
          <input type="hidden" name="action" value="tambahinduk" required>
          <input type="hidden" name="status" value="Induk" required>
          <input type="hidden" name="id_induk" value="0" required>
          @csrf
          <div class="row">

            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Jenis Masukan/Keluaran <span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-rounded" name="jenis">
              </div>
            </div>

          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">PROSES</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

    </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
