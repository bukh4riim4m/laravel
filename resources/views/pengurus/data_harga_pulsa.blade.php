@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Daftar Harga PPOB</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30">
  <div class="view-icons">
  </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/pengurus/harga-ppob')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Provider</label>
        <input type="text" class="form-control floating" name="provider" value="{{$providers}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Deskripsi</label>
        <input type="text" class="form-control floating" name="description" value="{{$deskropsi}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-12">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = ['Normal','Gangguan']; ?>
        <select class="select floating" name="status">
          <option value="">Semua</option>
          @foreach($statuses as $status)
            @if($stts == $status)
              <option value="{{$status}}" selected> {{$status}} </option>
            @else
              <option value="{{$status}}"> {{$status}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>


</div>
<div class="row">
<div class="col-md-12">
  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Provider</th>
          <th>Deskripsi</th>
          <th>Harga Jual</th>
          <th>Harga dari Suplayer</th>
          <th>Keuntungan Koperasi</th>
          <th>status</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($pulsa as $pul)
        <?php $untung = $pul['jual']-$pul['price']; ?>
        @if($untung < 200 || $pul['status']=='gangguan')
        <tr style="background:red;">
        @else
        <tr>
        @endif
          <td>{{$no++}}.</td>
          <td style="max-width:150px;">{{$pul['operator']}}</td>
          <td style="max-width:150px;">{{$pul['description']}}</td>
          <td>Rp {{number_format($pul['jual'],0,".",",")}}</td>
          <td>Rp {{number_format($pul['price'],0,".",",")}}</td>
          <td>Rp {{number_format($pul['jual']-$pul['price'],0,".",",")}}</td>
          <td>{{$pul['status']}}</td>
        </tr>
        @endforeach
        @if(count($pulsa) < 1)
        <tr>
          <td colspan="7" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
