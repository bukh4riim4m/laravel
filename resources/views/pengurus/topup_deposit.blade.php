@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-4">
          <h4 class="page-title">DATA TOPUP SUPLAYER</h4>
        </div>
        <div class="col-xs-8 text-right m-b-30">
          <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#panduan"></i>PANDUAN TOPUP</a> -->
          <div class="view-icons">
          </div>
        </div>
      </div>
      <div class="row">

    </div>
    <div class="row">
      <div class="col-sm-8 col-md-12 col-xs-12">
        <div class="row filter-row">
          <form class="" action="{{route('laporan-topup-suplayer')}}" method="post">
            @csrf
            <input type="hidden" name="action" value="cari">
            <div class="col-sm-3 col-xs-6">
              <div class="form-group form-focus">
                <label class="control-label">Dari</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$dari}}" required></div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-6">
              <div class="form-group form-focus">
                <label class="control-label">Sampai</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="until" value="{{$sampai}}" required></div>
              </div>
            </div>
            <div class="col-sm-3 col-md-3 col-xs-12">
              <div class="form-group form-focus select-focus">
                <label class="control-label">Status</label>
                <?php $statuses = [
                  'Menunggu','Berhasil','Batal'
                ]; ?>
                <select class="select floating" name="status">
                  <option value=""> -- Semua -- </option>
                  @foreach($statuses as $stts)
                    @if($stts == $status)
                    <option value="{{$stts}}" selected> {{$stts}} </option>
                    @else
                    <option value="{{$stts}}"> {{$stts}} </option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-8 col-md-12 col-xs-12">
      <div class="row">
        <div class="table-responsive">
          <table class="table table-striped custom-table">
            <thead>
            <tr>
              <th>No.</th>
              <th>Tgl Transaksi</th>
              <th>No. Transaksi</th>
              <th>Bank</th>
              <th>Nominal Request</th>
              <th>Nominal Transfer</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; ?>
            @foreach($hasil as $hsl)
            <tr>
              <td>{{$no++}}.</td>
              <td>{{$hsl['tgl_trx']}}</td>
              <td>{{$hsl['no_trx']}}</td>
              <td>{{$hsl['bank']}}</td>
              <td>Rp {{number_format($hsl['nominal'],0,".",",")}}</td>
              <td>Rp {{number_format($hsl['transfer'],0,".",",")}}</td>
              <td>@if($hsl['status'] =='Menunggu')<span class="label label-warning-border">{{$hsl['status']}}</span>@elseif($hsl['status'] =='Berhasil')<span class="label label-success-border">{{$hsl['status']}}</span> @else <span class="label label-danger-border">{{$hsl['status']}}</span> @endif </td>
            </tr>
            @endforeach
          </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection
