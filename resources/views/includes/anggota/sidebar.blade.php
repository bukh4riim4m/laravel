<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  @if($dashboard =='dashboard')<li class="active">@else <li>@endif
  <!-- <li> -->
    <a href="{{url('/anggota/home')}}">DASHBOARD ANGGOTA</a>
  </li>
  {{--@if($dashboard =='transaksi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/anggota/belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{url('/anggota/pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{url('/anggota/paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data Internet</a></li>
      <li><a href="{{url('/anggota/voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{url('/anggota/paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{url('/anggota/transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <li><a href="{{url('/anggota/saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <!-- <li><a href="{{route('anggota-cek-tagihan-listrik')}}">&nbsp;&nbsp;&nbsp;Tagihan Listrik</a></li> -->
      <li><a href="{{route('anggota-data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('anggota-data-transaksi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Belanja</a></li>
    </ul>
  --}}
  @if($dashboard =='bukuSaldo')<li class="active">@else <li>@endif
  <!-- <li> -->
    <a href="{{route('anggota-mutasi-simpanan')}}">MUTASI SIMPANAN</a>
  </li>
  @if($dashboard =='datapinjaman')<li class="active">@else <li>@endif
    <a href="{{route('anggota-data-pinjaman')}}">DATA PINJAMAN</a>
  </li>

  @if($dashboard =='datasaham')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA SAHAM</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('anggota-pembelian-saham')}}">&nbsp;&nbsp;&nbsp;Pembelian Saham</a></li><!--{{route('admin-aset-masuk')}}-->
      <li><a href="{{route('anggota-penjualan-saham')}}">&nbsp;&nbsp;&nbsp;Penjualan Saham</a></li>
    </ul>
  </li>
  @if($dashboard =='profil')<li class="active">@else <li>@endif
    <a href="{{url('/anggota/profil')}}">PROFIL</a>
  </li>
  {{--
  @if($dashboard =='kartu')<li class="active">@else <li>@endif
    <a href="{{url('/anggota/kartu')}}">KARTU ANGGOTA</a>
  </li>
  @if($dashboard =='simpanan')<li class="active">@else <li>@endif
    <a href="{{url('/anggota/simpanan')}}">SIMPANAN</a>
  </li>
  @if($dashboard =='statistik')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/anggota/statistik')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a href="{{url('/anggota/statistik-penjualan')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a href="{{url('/anggota/statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a href="{{url('/anggota/statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a href="{{url('/anggota/statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a href="{{url('/anggota/statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  @if($dashboard =='akumulasi')<li class="active">@else <li>@endif
    <a href="{{url('/anggota/akumulasi-belanja')}}">AKUMULASI BELANJA</a>
  </li>
  @if($dashboard =='deposit')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DEPOSIT</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit-anggota')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{route('data.topup')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li>
    </ul>
  @if($dashboard =='bukuSaldo')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>BUKU SALDO</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/anggota/buku-saldo-simpanan')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('anggota-buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>--}}

  @if($dashboard =='gantiPassword')<li class="active">@else <li>@endif
    <a href="{{url('/anggota/ganti-password')}}">GANTI PASSWORD</a>
  </li>
  <li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">KELUAR</a>
  </li>
</ul>
</div>
    </div>
</div>
