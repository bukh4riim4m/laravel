<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  @if($dashboard =='dashboard')<li class="active">@else <li>@endif
    <a href="{{url('/pemegang-saham/home')}}">DASHBOARD SAHAM</a>
  </li>
  @if($dashboard =='datasaham')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA SAHAM</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('saham-pembelian-saham')}}">&nbsp;&nbsp;&nbsp;Pembelian Saham</a></li><!--{{route('admin-aset-masuk')}}-->
      <li><a href="{{route('saham-penjualan-saham')}}">&nbsp;&nbsp;&nbsp;Penjualan Saham</a></li>
    </ul>
  </li>
  @if($dashboard =='dataSaya')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA SAYA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pemegang-saham-profil')}}">&nbsp;&nbsp;&nbsp;Profil</a></li>
      {{--<!-- <li><a href="{{url('/administrator/alamat-kirim')}}">&nbsp;&nbsp;&nbsp;Alamat Kirim</a></li> -->
      <!-- <li><a href="{{url('/administrator/simpanan')}}">&nbsp;&nbsp;&nbsp;Simpanan</a></li> -->
      <!-- <li><a href="{{url('/administrator/my-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Akumulasi Belanja</a></li> -->
      <!-- <li><a href="{{url('/administrator/kartu')}}">&nbsp;&nbsp;&nbsp;Kartu Saya</a></li> -->--}}
      <li><a href="{{route('pemegang-saham-ganti-password')}}">&nbsp;&nbsp;&nbsp;Ganti Password</a></li>
    </ul>
  </li>
  <li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">KELUAR</a>
  </li>
</ul>
</div>
    </div>
</div>
