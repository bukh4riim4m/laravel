@extends('layouts.saham.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif
            @include('flash::message')
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<h4 class="page-title">Penjualan Saham</h4>
						</div>
					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{route('saham-penjualan-saham')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">

              <div class="col-sm-3 col-md-3 col-xs-6">
                <div class="form-group form-focus">
                  <label class="control-label">Dari Tgl</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
                </div>
              </div>
              <div class="col-sm-3 col-md-3 col-xs-6">
                <div class="form-group form-focus">
                  <label class="control-label">Sampai Tgl</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
                </div>
              </div>
              <div class="col-sm-3 col-xs-12">
  							<div class="form-group form-focus">
  								<label class="control-label">No Saham</label>
  								<input type="number" class="form-control floating" name="no_saham" value="{{$no_saham}}"/>
  							</div>
  						</div>
  						<div class="col-sm-3 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            <style>
              #kartuname {
                position: absolute;
                top:87px;
                color: black;
                font-size: 6px;
                bottom: 100px;
                margin-right: 33px;
                right: 35px;
                overflow: hidden;
              }
            </style>

						<!-- <div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget">
								<div class="profile-imges">
									<a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{asset('kartu/saham.png')}}" width="100%"/></a>
								</div>
								<h4 class="user-name m-t-10 m-b-0 text-ellipsis">312423522</h4>
								<h5 class="user-name m-t-10 m-b-0 text-ellipsis">Rp 10,000,000</h5>
								<div class="small text-muted"></div>
								<a href="" class="btn btn-default btn-sm m-t-10">Download</a>
								<a href="#" data-toggle="modal" data-target="#upload" class="btn btn-default btn-sm m-t-10">Edit</a>
							</div>
						</div> -->

            <div class="col-md-12">

              <div class="table-responsive">

                <table class="table table-striped custom-table">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Tgl. Transaksi<br>No. Transaksi</th>
                      <th>Nama Anggota<br>No.Anggota</th>
                      <th>No.Saham</th>
                      <th>Nominal</th>
                      <!-- <th style="min-width:100px;" class="text-right">Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1;$total = 0; ?>
                    @foreach($datas as $data)
                    <?php $total+= $data->nominal;?>
                    <tr>
                      <td>{{$no++}}.</td>
                      <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}<br>{{$data->no_trx}}</td>
                      <td>{{ $data->dataUser['name'] }} <br> {{$data->dataUser['no_anggota']}} </td>
                      <td>{{$data->no_saham}}</td>
                      <td>Rp {{number_format($data->nominal,'0','.',',')}}</td>

                      <!-- <td>Rp </td> -->
                      <!-- <td style="max-width:80px;" class="text-right">
                        <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anggota" id="editanggota">Edit</a>
                        <a href="#" class="btn btn-warning btn-sm rounded" data-toggle="modal" data-target="#edit">Edit</a>
                      </td> -->
                    </tr>
                    @endforeach
                    @if(count($datas) < 1)
                    <tr>
                      <td colspan="5" class="text-center">KOSONG</td>
                    </tr>
                    @endif
                  </tbody>

                </table>{!! $datas->render() !!}


                <!-- <label class="col-lg-3 control-label">Light Logo</label> -->
                <!-- <form class="" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
                  @csrf
                  <input type="hidden" name="action" value="csv">
              		<div class="col-lg-3"><br>
                    <span class="control-label">UPLOAD CSV</span>
                      <input class="form-control rounded" type="file" name="upload">
              		</div>
                  <div class="col-lg-9"><br>
              		</div>
                  <div class="col-lg-2"><br>
                      <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
              		</div>
                </form> -->
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th>Total </th>
                      <th>: Rp {{number_format($total,'0','.',',')}}</th>
                    </tr>
                    <!-- <tr>
                      <th>Total Seluruh Simpanan</th>
                      <th>: Rp </th>
                    </tr> -->
                  </thead>
                </table>
              </div>
            </div>
            <!-- ///// -->
            @foreach($datas as $use)
            <div id="detail{{$use->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content modal-md">
                  <div class="modal-header">
                    <h4 class="modal-title">SAHAM</h4>
                  </div>
                    <div class="modal-body card-box">
                      <img src="{{ asset('kartu/'.$use->gambar) }}" width="100%" alt="">
                      <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            @endforeach
					</div>
                </div>

    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
