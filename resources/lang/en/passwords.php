<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal harus enam karakter',
    'reset' => 'Kata sandi Anda telah disetel ulang!',
    'sent' => 'Kami telah mengirim e-mail pengaturan ulang kata sandi Anda!',
    'token' => 'Token atur ulang kata sandi ini tidak valid',
    'user' => "Alamat email tidak terdaftar",

];
