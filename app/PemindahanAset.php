<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemindahanAset extends Model
{
  protected $fillable = [
      'id','tgl_trx','no_trx','pemindahan','nominal','ket','admin','aktif','created_at','updated_at'
  ];
  public function dariKe(){
    return $this->belongsTo('App\ToEndTo','pemindahan');
  }
  public function userId(){
    return $this->belongsTo('App\User','admin');
  }
}
