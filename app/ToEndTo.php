<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToEndTo extends Model
{
    protected $fillable = [
      'id','name'
    ];
}
