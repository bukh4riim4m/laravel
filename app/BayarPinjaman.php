<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayarPinjaman extends Model
{
  protected $fillable = [
      'id','user_id','no_anggota','no_pinjamen','no_trx','tgl_trx','urutan_bayar','tgl_tempo','jumlah_piutang','bayar','kas_bank','sisa_pinjaman','keterangan','pokok','bunga','status','aktif','admin','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];


}
