<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
  protected $fillable = [
      'id','url','userid','key','secret','created_at','updated_at'
  ];
}
