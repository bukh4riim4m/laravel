<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaldoKasbank extends Model
{
  protected $fillable = [
      'id','name','saldo','created_at','updated_at'
  ];
}
