<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Simpanan extends Model
{
  protected $fillable = [
      'id','sequence','tanggalSetor','jenis_simpanan','mutasi','nominal','saldo','ket','created_at','updated_at'
  ];

  public function simpanan_id(){
    return $this->belongsTo('App\JenisSimpanan','jenis_simpanan');
  }
}
