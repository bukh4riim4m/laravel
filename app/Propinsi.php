<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model
{
  protected $fillable = [
      'id','name','aktif','created_at','updated_at'
  ];
}
