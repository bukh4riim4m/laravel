<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiAset extends Model
{
  protected $fillable = [
      'id','no_trx','tgl_trx','nominal','kasbank','jenis_one','jenis_two','ket','status','aktif','admin','created_at','updated_at'
  ];

  public function user_id(){
    return $this->belongsTo('App\User','admin');
  }
  public function jenis_id_one(){
    return $this->belongsTo('App\JenisMasukanKeluaran','jenis_one');
  }
  public function jenis_id_two(){
    return $this->belongsTo('App\JenisMasukanKeluaran','jenis_two');
  }
}
