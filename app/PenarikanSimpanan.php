<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenarikanSimpanan extends Model
{
  protected $fillable = [
    'id','user_id','no_trx','jenis_simpanan','no_anggota','saldo_kasbank','tgl_trx','nominal','ket','admin','aktif'
  ];
  public function userId(){
    return $this->belongsTo('App\User','user_id');
  }
}
