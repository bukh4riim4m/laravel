<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
  protected $fillable = [
      'id','judul','berita','aktif','admin','created_at','updated_at'
  ];
}
