<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MutasiKas extends Model
{
  protected $fillable = [
      'id','name','no_anggota','tgl_setor','kasbank','jth_tempo','no_trx','jenis_simpanan','mutasi','nominal','saldo','awal','ket','aktif','petugas','created_at','updated_at'
  ];
}
