<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisMasukanKeluaran extends Model
{
  protected $fillable = [
    'id','name','id_induk','status','aktif','admin'
  ];
}
