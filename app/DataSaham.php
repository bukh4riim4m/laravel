<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSaham extends Model
{
  protected $fillable = [
      'id','no_anggota','user_id','tgl_trx','no_trx','no_saham','nominal','jatuh_tempo','jumlah_tahun','kasbank_id','status','ket','aktif','admin','created_at','updated_at','updated_by'
  ];
  public function dataUser(){
    return $this->belongsTo('App\User','user_id');
  }
  public function kasBankId(){
    return $this->belongsTo('App\SaldoKasbank','kasbank_id');
  }
}
