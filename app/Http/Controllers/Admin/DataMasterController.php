<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\JenisMasukanKeluaran;
use DB;
use Log;
use Auth;

class DataMasterController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function ajpp(Request $request){
    $dashboard = "dataMaster";
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    if ($request->action=='tambahinduk') {
      $message = [
        'jenis.required'=>'Jenis Masukan / Keluaran Wajib diisi',
        'action.required'=>'Wajib diisi'
      ];
      $this->validate($request, [
            'jenis'=>'required',
            'action'=>'required'
      ],$message);

      $datas = JenisMasukanKeluaran::create([
        'name'=> $request->jenis,
        'id_induk'=>$request->id_induk,
        'status'=>$request->status,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      if ($datas) {
        flash()->overlay('Berhasil ditambahkan.', 'INFO');
        return redirect()->back();
      }
      flash()->overlay('Gagal ditambahkan.', 'INFO GAGAL');
      return redirect()->back();
    }elseif ($request->action=='edit') {
      $message = [
        'jenis.required'=>'Jenis Masukan / Keluaran Wajib diisi',
        'action.required'=>'Wajib diisi'
      ];
      $this->validate($request, [
            'jenis'=>'required',
            'action'=>'required'
      ],$message);
      $edit = JenisMasukanKeluaran::find($request->ids)->update([
        'name'=>$request->jenis,
        'admin'=>$request->user()->id
      ]);
      if ($edit) {
        flash()->overlay('Berhasil diedit.', 'INFO');
        return redirect()->back();
      }
      flash()->overlay('Gagal diedit.', 'INFO GAGAL');
      return redirect()->back();
    }elseif ($request->action=='hapus') {
      $edit = JenisMasukanKeluaran::find($request->ids)->update([
        'aktif'=>0,
        'admin'=>$request->user()->id
      ]);
      if ($edit) {
        flash()->overlay('Berhasil dihapus.', 'INFO');
        return redirect()->back();
      }
      flash()->overlay('Gagal dihapus.', 'INFO GAGAL');
      return redirect()->back();
    }
    $jenises = $request->jenis;
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $datas = JenisMasukanKeluaran::where('aktif',1)->where('status','induk')->where('id','LIKE','%'.$jenises.'%')->get();
    $alldatas = JenisMasukanKeluaran::where('aktif',1)->get();
    return view('administrator.datamaster.jenis_pengeluaran_pemasukan',compact('dashboard','datas','alldatas','jenises'));
  }
  public function pemegangsaham(Request $request){
    $dashboard = 'dataMaster';
    $nomor =$request->no_karyawan;
    $name =$request->name;
    $type="";
    if ($request->action =='tambah') {
      $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'no_karyawan' => 'required|min:6',
      'email'=>'required',
      'id_login' => 'required|min:6',
      'password_login' => 'required|min:6'
      ]);
      if ($idlog = User::where('sequence',$request->id_login)->first()) {
        flash()->overlay('GAGAL, ID Login Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      if ($idemail = User::where('email',$request->email)->first()) {
        flash()->overlay('GAGAL, Email Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      $user = User::create([
        'name' => $request->name,
        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
        'no_anggota' => $request->no_karyawan,
        'sequence' => $request->id_login,
        'email' => $request->email,
        'telp'=>$request->telp,
        'jenkel' => $request->jenkel,
        'password' => Hash::make($request->password_login),
        'saldo' => 0,
        'admin' => $request->user()->id,
        'aktif' => 1,
        'type' => 'saham'
      ]);
      if ($user) {
        flash()->overlay('Pemegang Saham berhasil di tambahkan.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Pemegang Saham gagal di tambahkan.','INFO');
      return redirect()->back();
    }elseif ($request->action =='edit') {
      if ($request->fotodiri) {
        $this->validate($request, [
              'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
      }
      DB::beginTransaction();
      try {
        $user = User::find($request->ids);
        $user->name=$request->name;
        $user->no_anggota=$request->no_karyawan;
        $user->tgl_lahir=date('Y-m-d', strtotime($request->tgl_lahir));
        $user->email=$request->email;
        $user->telp=$request->telp;
        $user->jenkel=$request->jenkel;

        if ($request->fotodiri) {
          if ($user->fotodiri =='null' || $user->fotodiri =='') {
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }else {
            $destination_foto =public_path('foto/'.$user->fotodiri);
            if(file_exists($destination_foto)){
                        unlink($destination_foto);
            }
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }
        }
        $user->update();
      } catch (\Exception $e) {
        Log::info('Gagal Edit Profil:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Edit Profil.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Data Profil berhasil di Edit.','INFO');
      return redirect()->back();
    }elseif ($request->action =='hapus') {
      flash()->overlay('DEMO TIDAK DI IJINKAN HAPUS.', 'INFO DEMO');
      return redirect()->back();
      $delete = User::find($request->ids);
      $delete->aktif = 0;
      if ($delete->update()) {
        flash()->overlay('Data Profil berhasil di Hapus.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Profil Gagal di Hapus.','INFO');
      return redirect()->back();
    }
    $user = User::where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$name.'%')->where('aktif',1)->where('type','saham')->orderBy('no_anggota','ASC')->get();
    return view('administrator.datamaster.pemegang_saham',compact('dashboard','user','nomor','name','type'));
  }
}
