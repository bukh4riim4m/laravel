<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\Pinjaman;
use App\BayarPinjaman;
use App\SaldoKasbank;
use App\MutasiKas;
use App\MutasiBank;
use App\TransaksiAset;
use App\JenisMasukanKeluaran;
use App\PenarikanSimpanan;
use App\DataSaham;
use App\PemindahanAset;
use Image;
use Excel;
use DB;
use Log;
// use Charts;
class TransaksiController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function adminsimpananpokok(Request $request){

    $dashboard ="transaksi";
    $nomor = $request->no_anggota;
    $jenissim = $request->jenis_simpanan;
    $from = $request->dari;
    $to = $request->sampai;
    $mutasis = $request->mutasi;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    if ($request->action =='cari') {
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',1)->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        return view('administrator.transaksi.simpanan_pokok', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
    } elseif ($request->action =='csv') {
        /////////////////////////
        $validatedData = $request->validate([
    'upload' => 'required'
    ]);
        $ktp = '12345.'.$request->upload->getClientOriginalExtension();
        $sequence = substr($ktp, -3);
        if ($sequence !=='csv') {
            flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
            return redirect()->back();
        }
        $upload = $request->file('upload');
        $filePath = $upload->getRealPath();
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader=[];
        //validate
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        while ($columns=fgetcsv($file)) {
            if ($columns[0]=="") {
                continue;
            }
            //trim data
            foreach ($columns as $key => &$value) {
                $value=$value;
            }
            // return $columns;
            $data = array_combine($escapedHeader, $columns);
            // setting type
            foreach ($data as $key => &$value) {
                $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
            }
            // return $data;
            // Table update
            $nokssds=$data['nokssd'];
            $namas=$data['nama'];
            $tanggals=$data['tanggal'];
            $notrx=$data['nomortransaksi'];
            $nominals = $data['nominal'];
            $saldos = $data['saldo'];
            $kodes = $data['kode'];
            $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
            $simpanan->no_anggota=$nokssds;
            $simpanan->tgl_setor = $tanggals;
            $simpanan->no_trx = $notrx;
            $simpanan->mutasi='Debet';
            $simpanan->nominal=$nominals;
            $simpanan->saldo=$saldos;
            $simpanan->ket=$kodes;
            $simpanan->aktif=1;
            $simpanan->jenis_simpanan=$kodes;
            $simpanan->petugas = $request->user()->id;
            $simpanan->save();
        }
        flash()->overlay('CSV Berhasil di Upload.', 'INFO');
        return redirect()->back();
    }
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('aktif', 1)->where('mutasi', 'LIKE', '%'.$mutasis.'%')->orderBy('tgl_setor', 'ASC')->get();
    return view('administrator.transaksi.simpanan_pokok', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function adminsimpananwajib(Request $request){
    $dashboard ="transaksi";
    $nomor = $request->no_anggota;
    $jenissim = $request->jenis_simpanan;
    $from = $request->dari;
    $to = $request->sampai;
    $mutasis = $request->mutasi;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    if ($request->action =='cari') {
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',2)->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        return view('administrator.transaksi.simpanan_wajib', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
    }
    //elseif ($request->action =='csv') {
    //     /////////////////////////
    //     $validatedData = $request->validate([
    // 'upload' => 'required'
    // ]);
    //     $ktp = '12345.'.$request->upload->getClientOriginalExtension();
    //     $sequence = substr($ktp, -3);
    //     if ($sequence !=='csv') {
    //         flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
    //         return redirect()->back();
    //     }
    //     $upload = $request->file('upload');
    //     $filePath = $upload->getRealPath();
    //     $file = fopen($filePath, 'r');
    //     $header = fgetcsv($file);
    //     $escapedHeader=[];
    //     //validate
    //     foreach ($header as $key => $value) {
    //         $lheader=strtolower($value);
    //         $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
    //         array_push($escapedHeader, $escapedItem);
    //     }
    //     //looping through othe columns
    //     while ($columns=fgetcsv($file)) {
    //         if ($columns[0]=="") {
    //             continue;
    //         }
    //         //trim data
    //         foreach ($columns as $key => &$value) {
    //             $value=$value;
    //         }
    //         // return $columns;
    //         $data = array_combine($escapedHeader, $columns);
    //         // setting type
    //         foreach ($data as $key => &$value) {
    //             $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
    //         }
    //         // return $data;
    //         // Table update
    //         $nokssds=$data['nokssd'];
    //         $namas=$data['nama'];
    //         $tanggals=$data['tanggal'];
    //         $notrx=$data['nomortransaksi'];
    //         $nominals = $data['nominal'];
    //         $saldos = $data['saldo'];
    //         $kodes = $data['kode'];
    //         $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
    //         $simpanan->no_anggota=$nokssds;
    //         $simpanan->tgl_setor = $tanggals;
    //         $simpanan->no_trx = $notrx;
    //         $simpanan->mutasi='Debet';
    //         $simpanan->nominal=$nominals;
    //         $simpanan->saldo=$saldos;
    //         $simpanan->ket=$kodes;
    //         $simpanan->aktif=1;
    //         $simpanan->jenis_simpanan=$kodes;
    //         $simpanan->petugas = $request->user()->id;
    //         $simpanan->save();
    //     }
    //     flash()->overlay('CSV Berhasil di Upload.', 'INFO');
    //     return redirect()->back();
    // }
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',2)->where('aktif', 1)->where('mutasi', 'LIKE', '%'.$mutasis.'%')->orderBy('tgl_setor', 'ASC')->get();
    return view('administrator.transaksi.simpanan_wajib', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function adminsimpanansukarela(Request $request){
    $dashboard ="transaksi";
    return view('administrator.transaksi.simpanan_sukarela', compact('dashboard'));

  }
  public function adminsimpanankhusus(Request $request){
    $dashboard ="transaksi";
    return view('administrator.transaksi.simpanan_khusus', compact('dashboard'));

  }
  // public function historysimpanan(Request $request){
  //   return 'OK';
  // }
  public function cekanggota(Request $request){
    if ($request->ajax()) {
      if ($anggota = User::where('no_anggota','LIKE','%'.$request->noanggota.'%')->where('aktif',1)->where('type','anggota')->first()) {
        $hasil = "<div class='col-sm-6'>
          <div class='form-group'>
            <label class='control-label'>Nama Anggota <span class='text-danger'>*</span></label>
            <label class='form-control btn-success'>".$anggota->name."</label>
          </div>
        </div>
        <div class='col-sm-6'>
          <div class='form-group'>
            <label class='control-label'>Nomor Hp <span class='text-danger'>*</span></label>
            <label class='form-control btn-success'>".$anggota->telp."</label>
          </div>
        </div>";
        $tabels = "<table class='table table-striped custom-table'><thead>
          <tr>
            <th>No.</th>
            <th>No.Angota</th>
            <th>Nama Anggota</th>
            <th>Tgl. Transaksi</th>
            <th>No. Transaksi</th>
            <th>Jenis Simpanan</th>
            <th>Nominal</th>
          </tr>
        </thead>
        <tbody>";
        if ($simpanans = Simpananadmin::where('no_anggota',$anggota->no_anggota)->where('jenis_simpanan',$request->jenis)->where('aktif',1)->get()) {
          foreach ($simpanans as $key => $simpanan) {
            $urut=$key+1;
            $tabels.="<tr>
              <td>".$urut.".</td>
              <td>".$simpanan->no_anggota."</td>
              <td>".$simpanan->name."</td>
              <td>".date('d-m-Y', strtotime($simpanan->tgl_setor))."</td>
              <td>".$simpanan->no_trx."</td>
              <td>".$simpanan->jenisSimpanan->name."</td>
              <td>Rp ".number_format($simpanan->nominal,0,'.',',')."</td>
            </tr>";
            $tabels.="</tbody>";
          }
          $tabels.="</tbody></table>";
        }
      }else {
        $errors = "<div class='col-sm-12'>
                    <div class='form-group'>

                      <label class='form-control btn btn-danger'>NOMOR ANGGOTA TIDAK DI TEMUKAN</label>
                    </div>
                  </div>";
      }
      if ($anggota && $hasil && $simpanans) {
        $response = [
          'code'=>200,
          'datas'=>$hasil,
          'table'=>$tabels
        ];
      }else {
        $response = [
          'code'=>400,
          'datas'=>$errors
        ];
      }

      return $response;
    }
  }
  public function cekpemegangsaham(Request $request){
    if ($request->ajax()) {
      if ($anggota = User::where('no_anggota','LIKE','%'.$request->noanggota.'%')->where('aktif',1)->where('type','<>','admin')->first()) {
        if ($anggota->type == 'karyawan') {
          $errors = "<div class='col-sm-12'>
                      <div class='form-group'>

                        <label class='form-control btn btn-danger'>NOMOR ANGGOTA TIDAK DI TEMUKAN</label>
                      </div>
                    </div>";

                    $response = [
                      'code'=>400,
                      'datas'=>$errors
                    ];

              return $response;
        }else {
          $hasil = "<div class='col-sm-6'>
            <div class='form-group'>
              <label class='control-label'>Nama Anggota <span class='text-danger'>*</span></label>
              <label class='form-control btn-success'>".$anggota->name."</label>
            </div>
          </div>
          <div class='col-sm-6'>
            <div class='form-group'>
              <label class='control-label'>Nomor Hp <span class='text-danger'>*</span></label>
              <label class='form-control btn-success'>".$anggota->telp."</label>
            </div>
          </div>";
        }
      }else {
        $errors = "<div class='col-sm-12'>
                    <div class='form-group'>

                      <label class='form-control btn btn-danger'>NOMOR ANGGOTA TIDAK DI TEMUKAN</label>
                    </div>
                  </div>";
      }
      if ($anggota && $hasil) {
        $response = [
          'code'=>200,
          'datas'=>$hasil
        ];
      }else {
        $response = [
          'code'=>400,
          'datas'=>$errors
        ];
      }

      return $response;
    }
  }
  public function pinjaman(Request $request){
    $dashboard ="transaksi";
    if ($request->action == "proses") {
      if (!$user = User::where('no_anggota','LIKE','%'.$request->no_anggota.'%')->where('type','anggota')->where('aktif',1)->first()) {
        flash()->overlay('Nomor anggota salah.', 'INFO');
        return redirect()->back();
      }
      $this->validate($request, [
            'angsuran' => 'required',
            'no_pinjam' =>'required',
            'no_anggota' =>'required',
            'nilai_pinjam' =>'required',
            'persen_bunga' =>'required',
            'ambil_uang'=>'required',
            'ket' =>'required'
      ]);

      $tglsetor = date('Y-m-d');
      $jumlahtgl = '+'.$request->angsuran.' months';
      $tgltempo = date('Y-m-d', strtotime($jumlahtgl, strtotime($tglsetor))); //operasi penjumlahan tanggal sebanyak 6 hari

      $nominals= str_replace(",", "", $request->nilai_pinjam);
      $nilaibunga = $nominals * $request->persen_bunga / 100;
      $nilaiangsuran = $nominals + $nilaibunga / $request->angsuran;
      $nilaitotal = $nominals + $nilaibunga;
      $pokok_per_bln  = $nominals/$request->angsuran;
      $bunga_per_bln = $nilaibunga / $request->angsuran;
      $saldos = SaldoKasbank::find($request->ambil_uang);
      if ($saldos->saldo < $nominals) {
        flash()->overlay('Saldo '.$saldos->name.' tidak mencukupi.', 'GAGAL');
        return redirect()->back();
      }
      DB::beginTransaction();
      try {
        for ($i=1; $i < $request->angsuran+1; $i++) {
          $jumlahtgl = '+'.$i.' months';
          $tgltempo = date('Y-m-d', strtotime($jumlahtgl, strtotime($tglsetor)));
          BayarPinjaman::create([
            'no_pinjamen'=>$request->no_pinjam,
            'user_id'=>$user->id,
            'no_anggota'=>$user->no_anggota,
            'no_trx'=>date('dHis')+$i,
            'tgl_tempo'=>$tgltempo,
            'jumlah_piutang'=>(int)$nominals+$nilaibunga,
            'urutan_bayar'=>$i,
            'bayar'=>(int)$pokok_per_bln+(int)$bunga_per_bln,
            'kas_bank'=>$request->ambil_uang,
            'sisa_pinjaman'=>0,
            'pokok'=>$pokok_per_bln,
            'bunga'=>$bunga_per_bln,
            'keterangan'=>$request->ket,
            'aktif'=>1,
            'admin'=>$request->user()->id
          ]);
        }
        $proses = Pinjaman::create([
          'no_pinjam' => $request->no_pinjam,
          'no_anggota' =>$user->no_anggota,
          'tgl_trx' =>date('Y-m-d'),
          'jatuh_tempo' =>$tgltempo,
          'kali_angsuran'=>$request->angsuran,
          'nilai_pinjam' =>$nominals,
          'bunga_persen' =>$request->persen_bunga,
          'nilai_bunga' =>$nilaibunga,
          'angsuran' =>$nilaiangsuran,
          'total_pinjam' =>$nilaitotal,
          'aktif' =>1,
          'admin' =>$request->user()->id,
          'status_pinjam' =>'Belum Lunas',
          'sisa_pinjam' =>$nilaitotal,
          'keterangan'=>$request->ket
        ]);

        $akhirsaldo = $saldos->saldo - $nominals;
        if ($request->ambil_uang==1) {
          $mutasi = MutasiKas::create([
            'name'=>$user->name,
            'no_trx'=>date('ymdHis'),
            'no_anggota'=>$user->no_anggota,
            'tgl_setor'=>date('Y-m-d'),
            'jenis_simpanan'=>0,
            'nominal'=>$nominals,
            'mutasi'=>'Debet',
            'kasbank'=>$saldos->name,
            'ket'=>$request->ket,
            'saldo'=>$akhirsaldo,
            'jth_tempo'=>date('Y-m-d'),
            'awal'=>0,
            'aktif'=>1,
            'petugas'=>$request->user()->id
          ]);
          $saldos->saldo = $akhirsaldo;
          $saldos->update();
          $user->save();
        }else {
          $mutasi = MutasiBank::create([
            'name'=>$user->name,
            'no_trx'=>date('ymdHis'),
            'no_anggota'=>$user->no_anggota,
            'tgl_setor'=>date('Y-m-d'),
            'jenis_simpanan'=>0,
            'nominal'=>$nominals,
            'mutasi'=>'Debet',
            'kasbank'=>$saldos->name,
            'ket'=>$request->ket,
            'saldo'=>$akhirsaldo,
            'jth_tempo'=>date('Y-m-d'),
            'awal'=>0,
            'aktif'=>1,
            'petugas'=>$request->user()->id
          ]);
          $saldos->saldo = $akhirsaldo;
          $saldos->update();
          $user->save();
        }

        $saldos->saldo = $akhirsaldo;
        $saldos->update();
      } catch (\Exception $e) {
        Log::info('Gagal Proses Pinjaman:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Proses Pinjaman.','GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Pinjaman berhasil di proses.', 'INFO');
      return redirect()->route('admin-laporan-pinjaman');
    }
    return view('administrator.transaksi.form_pinjaman', compact('dashboard'));
  }
  public function bayarpinjaman(Request $request){
    $dashboard ="transaksi";
    if ($request->action =="proses") {
      $this->validate($request, [
            'no_pinjaman'=>'required',
            'pembayaran' =>'required'
      ]);
      if (!$bayars = BayarPinjaman::where('no_pinjamen',$request->no_pinjaman)->where('aktif',1)->where('status',1)->orderBy('tgl_tempo','ASC')->first()) {
        flash()->overlay('Maaf, Cicilan sudah lunas.', 'GAGAL');
        return redirect()->back();
      }
      $notrx = "BP-".date('ymdhis');
      DB::beginTransaction();
      try {
        $bayars = BayarPinjaman::where('no_pinjamen',$request->no_pinjaman)->where('aktif',1)->where('status',1)->orderBy('tgl_tempo','ASC')->first();
        $sisa = Pinjaman::where('no_pinjam',$request->no_pinjaman)->where('aktif',1)->first();
        $user = User::where('no_anggota',$sisa->no_anggota)->first();
        $bayars->tgl_bayar = date('Y-m-d');
        $bayars->sisa_pinjaman = $sisa->sisa_pinjam - $bayars->bayar;
        $sisa->sisa_pinjam = $sisa->sisa_pinjam - $bayars->bayar;
        $bayars->status = 2;
        $saldokasbank = SaldoKasbank::find($request->pembayaran);
        $akhirsaldo = $saldokasbank->saldo+$bayars->bayar;
        $saldokasbank->saldo = $akhirsaldo;
        if ($sisa->kali_angsuran == $bayars->urutan_bayar) {
          $sisa->status_pinjam = "Sudah Lunas";
        }
        $saldokasbank->update();
        if ($request->pembayaran ==1) {
          $kredits = MutasiKas::create([
            'name'=>$user->name,
            'no_trx'=>$notrx,
            'no_anggota'=>$user->no_anggota,
            'tgl_setor'=>date('Y-m-d'),
            'jenis_simpanan'=>0,
            'nominal'=>$bayars->bayar,
            'mutasi'=>'Kredit',
            'kasbank'=>$saldokasbank->name,
            'ket'=>'Pembayaran pinjaman '.$request->no_pinjaman.' ke '.$bayars->urutan_bayar,
            'saldo'=>$akhirsaldo,
            'jth_tempo'=>date('Y-m-d'),
            'awal'=>0,
            'aktif'=>1,
            'petugas'=>$request->user()->id
          ]);
        }else {
          $kredits = MutasiBank::create([
            'name'=>$user->name,
            'no_trx'=>$notrx,
            'no_anggota'=>$user->no_anggota,
            'tgl_setor'=>date('Y-m-d'),
            'jenis_simpanan'=>0,
            'nominal'=>$bayars->bayar,
            'mutasi'=>'Kredit',
            'kasbank'=>$saldokasbank->name,
            'ket'=>'Pembayaran pinjaman '.$request->no_pinjaman.' ke '.$bayars->urutan_bayar,
            'saldo'=>$akhirsaldo,
            'jth_tempo'=>date('Y-m-d'),
            'awal'=>0,
            'aktif'=>1,
            'petugas'=>$request->user()->id
          ]);
        }
        $bayars->update();
        $sisa->update();
      } catch (\Exception $e) {
        Log::info('Gagal bayar pinjaman :'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Bayar Pinjaman.','GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Pembayaran Berhasil.','INFO');
      return redirect()->back();
    }
    return view('administrator.transaksi.bayar_pinjaman', compact('dashboard'));
  }
  public function kalkulasi(Request $request){
    if ($request->ajax()) {
      Log::info('REQUES:'.$request->nilaipinjam.','.$request->persenbunga.','.$request->angsuran.','.$request->tglsetor);


      $tglsetor = date('Y-m-d', strtotime($request->tglsetor));
      $jumlahtgl = '+'.$request->angsuran.' months';
      $tgltempo = date('d-m-Y', strtotime($jumlahtgl, strtotime($tglsetor))); //operasi penjumlahan tanggal sebanyak 6 hari

      $nominals= str_replace(",", "", $request->nilaipinjam);
      $nilaibunga = $nominals * $request->persenbunga / 100;
      $nilaiangsuran = ($nominals + $nilaibunga) / $request->angsuran;
      $nilaitotal = $nominals+$nilaibunga;
      $pokokperbulan = $nominals/$request->angsuran;
      $bungaperbln = $nilaibunga/$request->angsuran;


    }
    $bungabulan2 = "<input class='form-control' type='text' name='bungabln' value='".number_format($bungaperbln,0,'.',',')."'  disabled>";
    $pokokbulan2 = "<input class='form-control' type='text' name='pokokbln' value='".number_format($pokokperbulan,0,'.',',')."'  disabled>";
    $nilaibunga2 = "<input class='form-control' type='text' name='jatuh_tempo' value='".number_format($nilaibunga,0,'.',',')."'  disabled>";
    $jatuhtempo2 = "<input class='form-control' type='text' name='jatuh_tempo' value='".$tgltempo."'  disabled>";
    $nilaiangsuran2 = "<input class='form-control' type='text' name='nominal_angsuran' value='".number_format($nilaiangsuran,0,'.',',')."'  disabled>";
    $nilaitotal2 = "<input class='form-control' type='text' name='jumlah_total' value='".number_format($nilaitotal,0,'.',',')."'  disabled>";

    $datas = [
      'nilaibungaperbuln'=>$bungabulan2,
      'nilaitempo'=>$jatuhtempo2,
      'nilaibunga'=>$nilaibunga2,
      'nilaiangsuran'=>$nilaiangsuran2,
      'nilaitotal'=>$nilaitotal2,
      'nilaipokokperbulan'=>$pokokbulan2
    ];
    if ($datas) {
      $respon = [
        'code'=>200,
        'isi'=>$datas
      ];
    }

    return $respon;
  }
  public function pinjamananggota(Request $request){
    if ($request->ajax()) {
      if (!$pinjamans = Pinjaman::where('no_pinjam',$request->nopinjaman)->where('aktif',1)->first()) {

        $datas = "<label class='form-control btn btn-danger'>NOMOR PINJAMAN TIDAK DI TEMUKAN</label>";

        $respon = [
          'code'=>400,
          'isi'=>$datas
        ];

        return $respon;
      }

      if (!$pinjam = Pinjaman::where('no_pinjam',$request->nopinjaman)->where('aktif',1)->where('status_pinjam','Belum Lunas')->first()) {
        $datas = "<label class='form-control btn btn-danger'>PINJAMAN SUDAH LUNAS</label>";
        $respon = [
          'code'=>400,
          'isi'=>$datas
        ];
        return $respon;
      }


      $users = User::where('no_anggota',$pinjamans->no_anggota)->first();
      $sisa = BayarPinjaman::where('no_pinjamen',$pinjamans->no_pinjam)->where('aktif',1)->where('status',1)->orderBy('tgl_tempo','ASC')->sum('bayar');
      $urutan = BayarPinjaman::where('no_pinjamen',$pinjamans->no_pinjam)->where('aktif',1)->where('status',1)->orderBy('tgl_tempo','ASC')->first();
      $angsuran = $pinjamans->total_pinjam/$pinjamans->kali_angsuran;

      $nama_anggota = "<input class='form-control' type='text' name='nama_anggota' value='".$users->name."'  disabled>";
      $nomor_anggota = "<input class='form-control' type='text' name='nomor_anggota' value='".$users->no_anggota."'  disabled>";
      $tgl_bayar = "<input class='form-control' type='text' name='tgl_bayar' value='".date('d-m-Y')."'  disabled>";
      $jml_piutang = "<input class='form-control' type='text' name='jml_piutang' value='Rp ".number_format($pinjamans->total_pinjam,0,'.',',')."'  disabled>";
      $bayar = "<input class='form-control' type='text' name='bayar' value='Rp ".number_format($angsuran,0,'.',',')."'  disabled>";
      $pokok = "<input class='form-control' type='text' name='pokok' value='Rp ".number_format($pinjamans->nilai_pinjam/$pinjamans->kali_angsuran,0,'.',',')."'  disabled>";
      $bunga = "<input class='form-control' type='text' name='bunga' value='Rp ".number_format($pinjamans->nilai_bunga/$pinjamans->kali_angsuran,0,'.',',')."'  disabled>";
      $sisa_pinjaman = "<input class='form-control' type='text' name='sisa_pinjaman' value='Rp ".number_format($sisa,0,'.',',')."'  disabled>";
      $sisa_piutang = "<input class='form-control' type='text' name='sisa_piutang' value='Rp ".number_format($sisa-$angsuran,0,'.',',')."'  disabled>";
      $urutanbayar = "<input class='form-control' type='text' name='urutan_bayar' value='ke ".$urutan->urutan_bayar." Dari ".$pinjamans->kali_angsuran." Kali Angsuran'  disabled>";

      $datas = [
        'nama_anggota'=>$nama_anggota,
        'nomor_anggota'=>$nomor_anggota,
        'tgl_bayar'=>$tgl_bayar,
        'jml_piutang'=>$jml_piutang,
        'bayar'=>$bayar,
        'pokok'=>$pokok,
        'bunga'=>$bunga,
        'sisa_pinjaman'=>$sisa_pinjaman,
        'sisa_piutang'=>$sisa_piutang,
        'urutanbayar'=>$urutanbayar
      ];
      if ($datas) {
        $respon = [
          'code'=>200,
          'isi'=>$datas
        ];
      }else {
        $respon = [
          'code'=>400,
          'isi'=>'data kosong'
        ];
      }
      return $respon;
    }
  }
//FORM TRANSAKSI ASET MASUK
public function formasetmasuk(Request $request){
  $dashboard ="transaksiaset";
  return view('administrator.transaksi_aset.form_aset_masuk',compact('dashboard'));

}
public function formasetkeluar(Request $request){
  $dashboard ="transaksiaset";
  return view('administrator.transaksi_aset.form_aset_keluar',compact('dashboard'));

}
public function cekjenis(Request $request){
  if ($request->ajax()) {
    $jsn = JenisMasukanKeluaran::where('aktif',1)->where('id_induk',$request->jenis)->get();
    $select="";
    foreach ($jsn as $key => $jsnes) {
      $select.="<option id='".$jsnes->id."' value='".$jsnes->id."'>".$jsnes->name."</option>";
    }
    return $select;
  }
}
  //TARNSAKSI ASET
  public function asetmasuk(Request $request){
    $dashboard ="transaksiaset";
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    if ($request->action =='cari') {
      $from = $request->dari;
      $to = $request->sampai;
    }elseif ($request->action =='add') {
      $message = [
        'jenis.required'=>'Jenis Pemasukan 1 Wajib dipilih',
        'jenis2.required'=>'Jenis Pemasukan 2 Wajib dipilih',
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'action.required'=>'Wajib diisi',
        'nominal.required'=>'Nominal Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi',
      ];
      $this->validate($request, [
            'kasbank'=>'required',
            'action'=>'required',
            'nominal'=>'required',
            'jenis'=>'required',
            'jenis2'=>'required',
            'ket'=>'required'
      ],$message);
      DB::beginTransaction();
      $notrx = "AM-".date('ymdhis');
      $nominals= str_replace(",", "", $request->nominal);
      try {
        $tambah = TransaksiAset::create([
          'no_trx'=>$notrx,
          'tgl_trx'=>date('Y-m-d'),
          'nominal'=>$nominals,
          'kasbank'=>$request->kasbank,
          'jenis_one'=>$request->jenis,
          'jenis_two'=>$request->jenis2,
          'status'=>'masuk',
          'ket'=>$request->ket,
          'aktif'=>1,
          'admin'=>$request->user()->id
        ]);
        if ($request->kasbank =='Kas') {
          $mutasi = new MutasiKas;
          $saldokas = SaldoKasbank::find(1);
          $saldo = $saldokas->saldo + $nominals;
          $saldokas->saldo = $saldo;
          $saldokas->update();
        }else {
          $mutasi = new MutasiBank;
          $saldobank = SaldoKasbank::find(2);
          $saldo = $saldobank->saldo + $nominals;
          $saldobank->saldo = $saldo;
          $saldobank->update();
        }
        $mutasi->name = $request->user()->name;
        $mutasi->no_anggota = $request->user()->no_anggota;
        $mutasi->tgl_setor = date('Y-m-d');
        $mutasi->jth_tempo = date('Y-m-d');
        $mutasi->no_trx = $notrx;
        $mutasi->jenis_simpanan = 1;
        $mutasi->mutasi = 'Kredit';
        $mutasi->nominal = $nominals;
        $mutasi->kasbank = $request->kasbank;
        $mutasi->ket = $request->ket;
        $mutasi->saldo = $saldo;
        $mutasi->aktif = 1;
        $mutasi->awal = 0;
        $mutasi->petugas = $request->user()->id;
        $mutasi->save();
      } catch (\Exception $e) {
        Log::info('Gagal Proses Aset Masuk:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Aset Masuk Gagal diproses.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Aset Masuk Berhasil diproses.', 'INFO');
      return redirect()->route('admin-aset-masuk');
    }elseif ($request->action =='edit') {
      //return $request->all();
      $message = [
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'jenis.required'=>'Jenis Pemasukan Wajib dipilih',
        'jenis2.required'=>'Jenis Pemasukan 2 Wajib dipilih',
        'action.required'=>'Wajib diisi',
        'nominal.required'=>'Nominal Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi',
      ];
      $this->validate($request, [
            'kasbank'=>'required',
            'action'=>'required',
            'nominal'=>'required',
            'ket'=>'required',
            'jenis'=>'required',
            'jenis2'=>'required'
      ],$message);
      $nominals= str_replace(",", "", $request->nominal);

      DB::beginTransaction();
      try {
        $trx = TransaksiAset::find($request->ids);
        $saldos = SaldoKasbank::where('name',$request->kasbank)->first();
        $notrx = "EAM-".date('ymdhis');
        if ($request->kasbank == $trx->kasbank && $trx->kasbank =="Kas") {
          if ($trx->nominal > $nominals) {
            $selisih = $trx->nominal - $nominals;
            $saldoakhir = $saldos->saldo - $selisih;
            $debetkredit = "Debet";
          }else {
            $selisih = $nominals - $trx->nominal;
            $saldoakhir = $saldos->saldo + $selisih;
            $debetkredit = "Kredit";
          }

          $mutasi = new MutasiKas;
          $mutasi->name = $request->user()->name;
          $mutasi->no_anggota = $request->user()->no_anggota;
          $mutasi->tgl_setor = date('Y-m-d');
          $mutasi->jth_tempo = date('Y-m-d');
          $mutasi->no_trx = $notrx;
          $mutasi->jenis_simpanan = 1;
          $mutasi->mutasi = $debetkredit;
          $mutasi->nominal = $selisih;
          $mutasi->kasbank = $request->kasbank;
          $mutasi->ket = "Perubahan nominal No. Transaksi ".$trx->no_trx;
          $mutasi->saldo = $saldoakhir;
          $mutasi->aktif = 1;
          $mutasi->awal = 0;
          $mutasi->petugas = $request->user()->id;
          $mutasi->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();
          $saldos->saldo = $saldoakhir;
          $saldos->update();
        }elseif ($request->kasbank == $trx->kasbank && $trx->kasbank =="Bank"){
          if ($trx->nominal > $nominals) {
            $selisih = $trx->nominal - $nominals;
            $saldoakhir = $saldos->saldo - $selisih;
            $debetkredit = "Debet";
          }else {
            $selisih = $nominals - $trx->nominal;
            $saldoakhir = $saldos->saldo + $selisih;
            $debetkredit = "Kredit";
          }

          $mutasi = new MutasiBank;
          $mutasi->name = $request->user()->name;
          $mutasi->no_anggota = $request->user()->no_anggota;
          $mutasi->tgl_setor = date('Y-m-d');
          $mutasi->jth_tempo = date('Y-m-d');
          $mutasi->no_trx = $notrx;
          $mutasi->jenis_simpanan = 1;
          $mutasi->mutasi = $debetkredit;
          $mutasi->nominal = $selisih;
          $mutasi->kasbank = $request->kasbank;
          $mutasi->ket = "Perubahan nominal No. Transaksi ".$trx->no_trx;
          $mutasi->saldo = $saldoakhir;
          $mutasi->aktif = 1;
          $mutasi->awal = 0;
          $mutasi->petugas = $request->user()->id;
          $mutasi->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();
          $saldos->saldo = $saldoakhir;
          $saldos->update();
        }elseif ($request->kasbank != $trx->kasbank && $trx->kasbank =="Kas"){
          $saldos = SaldoKasbank::where('name','Kas')->first();
          $saldoakhir1 = $saldos->saldo - $trx->nominal;
          if ($saldoakhir1 < 0) {
            DB::rollback();
            Log::info('Gagal update Aset Masuk,saldo tidak boleh kurang dari 0');
            flash()->overlay('Gagal update Aset Masuk.', 'INFO GAGAL');
            return redirect()->back();
          }
          $mutasibank = new MutasiKas;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Debet";
          $mutasibank->nominal = $trx->nominal;
          $mutasibank->kasbank = $request->kasbank;
          $mutasibank->ket = "Debet nominal No. Transaksi ".$trx->no_trx;
          $mutasibank->saldo = $saldoakhir1;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;
          $mutasibank->save();

          $saldos2 = SaldoKasbank::where('name','Bank')->first();
          $saldoakhir2 = $saldos2->saldo + $nominals;
          $mutasikas = new MutasiBank;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Kredit";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = $request->kasbank;
          $mutasikas->ket = "Kredit Pemindahan No. Transaksi ".$trx->no_trx;
          $mutasikas->saldo = $saldoakhir2;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();

          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->kasbank = $request->kasbank;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();

          $saldos->saldo = $saldoakhir1;
          $saldos->update();

          $saldos2->saldo = $saldoakhir2;
          $saldos2->update();

          // flash()->overlay('dari Kas - Bank', 'INFO GAGAL');
          // return redirect()->back();
        }elseif ($request->kasbank != $trx->kasbank && $trx->kasbank =="Bank"){
          $saldos = SaldoKasbank::where('name','Bank')->first();
          $saldoakhir1 = $saldos->saldo - $trx->nominal;
          if ($saldoakhir1 < 0) {
            DB::rollback();
            Log::info('Gagal update Aset Masuk,saldo tidak boleh kurang dari 0');
            flash()->overlay('Gagal update Aset Masuk.', 'INFO GAGAL');
            return redirect()->back();
          }
          $mutasibank = new MutasiBank;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Debet";
          $mutasibank->nominal = $trx->nominal;
          $mutasibank->kasbank = $request->kasbank;
          $mutasibank->ket = "Debet nominal No. Transaksi ".$trx->no_trx;
          $mutasibank->saldo = $saldoakhir1;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;
          $mutasibank->save();

          $saldos2 = SaldoKasbank::where('name','Kas')->first();
          $saldoakhir2 = $saldos2->saldo + $nominals;
          $mutasikas = new MutasiKas;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Kredit";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = $request->kasbank;
          $mutasikas->ket = "Kredit Pemindahan No. Transaksi ".$trx->no_trx;
          $mutasikas->saldo = $saldoakhir2;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->kasbank = $request->kasbank;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();

          $saldos->saldo = $saldoakhir1;
          $saldos->update();

          $saldos2->saldo = $saldoakhir2;
          $saldos2->update();
        }


      } catch (\Exception $e) {
        DB::rollback();
        Log::info('Gagal Edit Aset Masuk:'.$e->getMessage());
        flash()->overlay('Gagal Edit Aset Masuk.', 'INFO GAGAL');
        return redirect()->route('admin-aset-masuk');
      }
      DB::commit();
      flash()->overlay('Aset Masuk Berhasil Update.', 'INFO');
      return redirect()->route('admin-aset-masuk');
    }elseif ($request->action =='hapus') {
      DB::beginTransaction();
      try {
        $trx = TransaksiAset::find($request->ids);
        $saldos = SaldoKasbank::where('name',$trx->kasbank)->first();
        $saldoakhir = $saldos->saldo - $trx->nominal;
        $saldos->saldo = $saldoakhir;
        $saldos->update();

        $notrx = "HAM-".date('ymdhis');
        $trx->aktif = 0;
        $trx->update();
        if ($saldoakhir < 0) {
          DB::rollback();
          Log::info('Gagal update hapus Masuk,saldo tidak boleh kurang dari 0');
          flash()->overlay('Gagal Hapus Aset Masuk, Sisa saldo Min 0.', 'INFO GAGAL');
          return redirect()->back();
        }
        if ($trx->kasbank =="Kas") {
          $mutasi = new MutasiKas;
        }else {
          $mutasi = new MutasiBank;
        }

        $mutasi->name = $request->user()->name;
        $mutasi->no_anggota = $request->user()->no_anggota;
        $mutasi->tgl_setor = date('Y-m-d');
        $mutasi->jth_tempo = date('Y-m-d');
        $mutasi->no_trx = $notrx;
        $mutasi->jenis_simpanan = 1;
        $mutasi->mutasi = "Debet";
        $mutasi->nominal = $trx->nominal;
        $mutasi->kasbank = $trx->kasbank;
        $mutasi->ket = "Hapus No. Transaksi ".$trx->no_trx;
        $mutasi->saldo = $saldoakhir;
        $mutasi->aktif = 1;
        $mutasi->awal = 0;
        $mutasi->petugas = $request->user()->id;
        $mutasi->save();
      } catch (\Exception $e) {
        DB::rollback();
        Log::info('Gagal Hapus Aset Masuk:'.$e->getMessage());
        flash()->overlay('Gagal Hapus Aset Masuk.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Aset Masuk Berhasil Hapus.', 'INFO');
      return redirect()->back();

    }
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $kasbank = $request->kasbank;
    $jns = $request->jenis;
    $datas = TransaksiAset::where('aktif',1)->where('status','masuk')->where('jenis_one','LIKE','%'.$jns.'%')->whereBetWeen('tgl_trx',[$dari,$ke])->where('kasbank','LIKE','%'.$kasbank.'%')->get();
    $totalasetmasuk = TransaksiAset::where('aktif',1)->where('status','masuk')->where('jenis_one','LIKE','%'.$jns.'%')->sum('nominal');
    return view('administrator.transaksi_aset.aset_masuk',compact('dashboard','from','to','datas','kasbank','totalasetmasuk','jns'));
  }


  public function asetkeluar(Request $request){
    $dashboard ="transaksiaset";
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    if ($request->action =='cari') {
      $from = $request->dari;
      $to = $request->sampai;
    }elseif ($request->action =='add') {
      $message = [
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'action.required'=>'Wajib diisi',
        'nominal.required'=>'Nominal Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi',
      ];
      $this->validate($request, [
            'kasbank'=>'required',
            'action'=>'required',
            'nominal'=>'required',
            'ket'=>'required'
      ],$message);
      DB::beginTransaction();
      $notrx = "AK-".date('ymdhis');
      $nominals= str_replace(",", "", $request->nominal);
      try {
        $tambah = TransaksiAset::create([
          'no_trx'=>$notrx,
          'tgl_trx'=>date('Y-m-d'),
          'nominal'=>$nominals,
          'kasbank'=>$request->kasbank,
          'status'=>'keluar',
          'jenis_one'=>$request->jenis,
          'jenis_two'=>$request->jenis2,
          'ket'=>$request->ket,
          'aktif'=>1,
          'admin'=>$request->user()->id
        ]);
        if ($request->kasbank =='Kas') {
          $mutasi = new MutasiKas;
          $saldokas = SaldoKasbank::find(1);
          $saldo = $saldokas->saldo - $nominals;
          $saldokas->saldo = $saldo;
          $saldokas->update();
        }else {
          $mutasi = new MutasiBank;
          $saldobank = SaldoKasbank::find(2);
          $saldo = $saldobank->saldo - $nominals;
          $saldobank->saldo = $saldo;
          $saldobank->update();
        }
        if ($saldo < 0) {
          DB::rollback();
          flash()->overlay('Saldo di '.$request->kasbank.' Kurang.', 'INFO GAGAL');
          return redirect()->back();
        }
        $mutasi->name = $request->user()->name;
        $mutasi->no_anggota = $request->user()->no_anggota;
        $mutasi->tgl_setor = date('Y-m-d');
        $mutasi->jth_tempo = date('Y-m-d');
        $mutasi->no_trx = $notrx;
        $mutasi->jenis_simpanan = 1;
        $mutasi->mutasi = 'Debet';
        $mutasi->nominal = $nominals;
        $mutasi->kasbank = $request->kasbank;
        $mutasi->ket = $request->ket;
        $mutasi->saldo = $saldo;
        $mutasi->aktif = 1;
        $mutasi->awal = 0;
        $mutasi->petugas = $request->user()->id;
        $mutasi->save();
      } catch (\Exception $e) {
        Log::info('Gagal Proses Aset Masuk:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Aset Keluar Gagal diproses.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Aset Keluar Berhasil diproses.', 'INFO');
      return redirect()->route('admin-aset-keluar');
    }elseif ($request->action =='edit') {
      $message = [
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'action.required'=>'Wajib diisi',
        'nominal.required'=>'Nominal Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi',
      ];
      $this->validate($request, [
            'kasbank'=>'required',
            'action'=>'required',
            'nominal'=>'required',
            'ket'=>'required'
      ],$message);
      $nominals= str_replace(",", "", $request->nominal);

      DB::beginTransaction();
      try {
        $trx = TransaksiAset::find($request->ids);
        $saldos = SaldoKasbank::where('name',$request->kasbank)->first();
        $notrx = "EAK-".date('ymdhis');
        if ($request->kasbank == $trx->kasbank && $trx->kasbank =="Kas") {
          if ($trx->nominal > $nominals) {
            $selisih = $trx->nominal - $nominals;
            $saldoakhir = $saldos->saldo + $selisih;
            $debetkredit = "Kredit";
          }else {
            $selisih = $nominals - $trx->nominal;
            $saldoakhir = $saldos->saldo - $selisih;
            $debetkredit = "Debet";
          }

          $mutasi = new MutasiKas;
          $mutasi->name = $request->user()->name;
          $mutasi->no_anggota = $request->user()->no_anggota;
          $mutasi->tgl_setor = date('Y-m-d');
          $mutasi->jth_tempo = date('Y-m-d');
          $mutasi->no_trx = $notrx;
          $mutasi->jenis_simpanan = 1;
          $mutasi->mutasi = $debetkredit;
          $mutasi->nominal = $selisih;
          $mutasi->kasbank = $request->kasbank;
          $mutasi->ket = "Perubahan nominal No. Transaksi ".$trx->no_trx;
          $mutasi->saldo = $saldoakhir;
          $mutasi->aktif = 1;
          $mutasi->awal = 0;
          $mutasi->petugas = $request->user()->id;
          $mutasi->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();
          $saldos->saldo = $saldoakhir;
          $saldos->update();
        }elseif ($request->kasbank == $trx->kasbank && $trx->kasbank =="Bank"){
          if ($trx->nominal > $nominals) {
            $selisih = $trx->nominal - $nominals;
            $saldoakhir = $saldos->saldo + $selisih;
            $debetkredit = "Kredit";
          }else {
            $selisih = $nominals - $trx->nominal;
            $saldoakhir = $saldos->saldo - $selisih;
            $debetkredit = "Debet";
          }

          $mutasi = new MutasiBank;
          $mutasi->name = $request->user()->name;
          $mutasi->no_anggota = $request->user()->no_anggota;
          $mutasi->tgl_setor = date('Y-m-d');
          $mutasi->jth_tempo = date('Y-m-d');
          $mutasi->no_trx = $notrx;
          $mutasi->jenis_simpanan = 1;
          $mutasi->mutasi = $debetkredit;
          $mutasi->nominal = $selisih;
          $mutasi->kasbank = $request->kasbank;
          $mutasi->ket = "Perubahan nominal No. Transaksi ".$trx->no_trx;
          $mutasi->saldo = $saldoakhir;
          $mutasi->aktif = 1;
          $mutasi->awal = 0;
          $mutasi->petugas = $request->user()->id;
          $mutasi->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();
          $saldos->saldo = $saldoakhir;
          $saldos->update();
        }elseif ($request->kasbank != $trx->kasbank && $trx->kasbank =="Kas"){
          $saldos = SaldoKasbank::where('name','Kas')->first();
          $saldoakhir1 = $saldos->saldo + $trx->nominal;

          $mutasibank = new MutasiKas;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Kredit";
          $mutasibank->nominal = $trx->nominal;
          $mutasibank->kasbank = $request->kasbank;
          $mutasibank->ket = "Kredit nominal No. Transaksi ".$trx->no_trx;
          $mutasibank->saldo = $saldoakhir1;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;


          $saldos2 = SaldoKasbank::where('name','Bank')->first();
          $saldoakhir2 = $saldos2->saldo - $nominals;
          if ($saldoakhir2 < 0) {
            DB::rollback();
            Log::info('Gagal update Aset Keluar,saldo tidak boleh kurang dari 0');
            flash()->overlay('Gagal update Aset Keluar.', 'INFO GAGAL');
            return redirect()->back();
          }
          $mutasibank->save();
          $mutasikas = new MutasiBank;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Debet";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = $request->kasbank;
          $mutasikas->ket = "Debet Pemindahan No. Transaksi ".$trx->no_trx;
          $mutasikas->saldo = $saldoakhir2;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->kasbank = $request->kasbank;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();

          $saldos->saldo = $saldoakhir1;
          $saldos->update();

          $saldos2->saldo = $saldoakhir2;
          $saldos2->update();

          // flash()->overlay('dari Kas - Bank', 'INFO GAGAL');
          // return redirect()->back();
        }elseif ($request->kasbank != $trx->kasbank && $trx->kasbank =="Bank"){
          $saldos = SaldoKasbank::where('name','Bank')->first();
          $saldoakhir1 = $saldos->saldo + $trx->nominal;

          $mutasibank = new MutasiBank;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Kredit";
          $mutasibank->nominal = $trx->nominal;
          $mutasibank->kasbank = $request->kasbank;
          $mutasibank->ket = "Kredit nominal No. Transaksi ".$trx->no_trx;
          $mutasibank->saldo = $saldoakhir1;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;


          $saldos2 = SaldoKasbank::where('name','Kas')->first();
          $saldoakhir2 = $saldos2->saldo - $nominals;
          if ($saldoakhir2 < 0) {
            DB::rollback();
            Log::info('Gagal update Aset Keluar,saldo tidak boleh kurang dari 0');
            flash()->overlay('Gagal update Aset Keluar.', 'INFO GAGAL');
            return redirect()->back();
          }
          $mutasibank->save();
          $mutasikas = new MutasiKas;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Debet";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = $request->kasbank;
          $mutasikas->ket = "Debet Pemindahan No. Transaksi ".$trx->no_trx;
          $mutasikas->saldo = $saldoakhir2;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();
          $trx->jenis_one = $request->jenis;
          $trx->jenis_two = $request->jenis2;
          $trx->kasbank = $request->kasbank;
          $trx->nominal = $nominals;
          $trx->ket = $request->ket;
          $trx->update();

          $saldos->saldo = $saldoakhir1;
          $saldos->update();

          $saldos2->saldo = $saldoakhir2;
          $saldos2->update();
        }


      } catch (\Exception $e) {
        DB::rollback();
        Log::info('Gagal Edit Aset Keluar:'.$e->getMessage());
        flash()->overlay('Gagal Edit Aset Keluar.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Aset Keluar Berhasil Update.', 'INFO');
      return redirect()->route('admin-aset-keluar');
    }elseif ($request->action =='hapus') {
      DB::beginTransaction();
      try {
        $trx = TransaksiAset::find($request->ids);
        $saldos = SaldoKasbank::where('name',$trx->kasbank)->first();
        $saldoakhir = $saldos->saldo + $trx->nominal;
        $saldos->saldo = $saldoakhir;
        $saldos->update();

        $notrx = "HAK-".date('ymdhis');
        $trx->aktif = 0;
        $trx->update();
        if ($saldoakhir < 0) {
          DB::rollback();
          Log::info('Gagal update hapus Masuk,saldo tidak boleh kurang dari 0');
          flash()->overlay('Gagal Hapus Aset Masuk, Sisa saldo Min 0.', 'INFO GAGAL');
          return redirect()->back();
        }
        if ($trx->kasbank =="Kas") {
          $mutasi = new MutasiKas;
        }else {
          $mutasi = new MutasiBank;
        }

        $mutasi->name = $request->user()->name;
        $mutasi->no_anggota = $request->user()->no_anggota;
        $mutasi->tgl_setor = date('Y-m-d');
        $mutasi->jth_tempo = date('Y-m-d');
        $mutasi->no_trx = $notrx;
        $mutasi->jenis_simpanan = 1;
        $mutasi->mutasi = "Kredit";
        $mutasi->nominal = $trx->nominal;
        $mutasi->kasbank = $trx->kasbank;
        $mutasi->ket = "Hapus No. Transaksi ".$trx->no_trx;
        $mutasi->saldo = $saldoakhir;
        $mutasi->aktif = 1;
        $mutasi->awal = 0;
        $mutasi->petugas = $request->user()->id;
        $mutasi->save();
      } catch (\Exception $e) {
        DB::rollback();
        Log::info('Gagal Hapus Aset Keluar:'.$e->getMessage());
        flash()->overlay('Gagal Hapus Aset Keluar.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Aset Keluar Berhasil Hapus.', 'INFO');
      return redirect()->back();

    }
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $kasbank = $request->kasbank;
    $datas = TransaksiAset::where('aktif',1)->where('status','keluar')->whereBetWeen('tgl_trx',[$dari,$ke])->where('kasbank','LIKE','%'.$kasbank.'%')->get();
    $totalasetkeluar = TransaksiAset::where('aktif',1)->where('status','keluar')->sum('nominal');
    return view('administrator.transaksi_aset.aset_keluar',compact('dashboard','from','to','datas','kasbank','totalasetkeluar'));
  }

  public function pindahaset(Request $request){
    $dashboard ="transaksiaset";
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    if ($request->action =='cari') {
      $from = $request->dari;
      $to = $request->sampai;
    }elseif ($request->action =='add') {
      $message = [
        'dari_ke.required'=>'Pemindahan Wajib dipilih',
        'action.required'=>'Wajib diisi',
        'nominal.required'=>'Nominal Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi',
      ];
      $this->validate($request, [
            'dari_ke'=>'required',
            'action'=>'required',
            'nominal'=>'required',
            'ket'=>'required'
      ],$message);
      $nominals= str_replace(",", "", $request->nominal);
      $notrx = "PA-".date('ymdhis');
      DB::beginTransaction();
      try {
        $input  = PemindahanAset::create([
          'tgl_trx'=>date('Y-m-d'),
          'no_trx'=>$notrx,
          'pemindahan'=>$request->dari_ke,
          'nominal'=>$nominals,
          'ket'=>$request->ket,
          'aktif'=>1,
          'admin'=>$request->user()->id
        ]);
        if ($request->dari_ke == 1) { //KAS KE BANK
          $saldokas = SaldoKasbank::find(1);
          $saldoakhirkas = $saldokas->saldo - $nominals;
          $saldokas->saldo = $saldoakhirkas;
          $saldokas->update();

          $saldobank = SaldoKasbank::find(2);
          $saldoakhirbank = $saldobank->saldo + $nominals;
          $saldobank->saldo = $saldoakhirbank;
          $saldobank->update();

          $mutasikas = new MutasiKas;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Debet";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = 1;
          $mutasikas->ket = "Pemindahan aset dari Kas ke Bank";
          $mutasikas->saldo = $saldoakhirkas;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();

          $mutasibank = new MutasiBank;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Kredit";
          $mutasibank->nominal = $nominals;
          $mutasibank->kasbank = 1;
          $mutasibank->ket = "Pemindahan aset dari Kas ke Bank";
          $mutasibank->saldo = $saldoakhirbank;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;
          $mutasibank->save();

        }else {
          $saldokas = SaldoKasbank::find(1);
          $saldoakhirkas = $saldokas->saldo + $nominals;
          $saldokas->saldo = $saldoakhirkas;
          $saldokas->update();

          $saldobank = SaldoKasbank::find(2);
          $saldoakhirbank = $saldobank->saldo - $nominals;
          $saldobank->saldo = $saldoakhirbank;
          $saldobank->update();

          $mutasikas = new MutasiKas;
          $mutasikas->name = $request->user()->name;
          $mutasikas->no_anggota = $request->user()->no_anggota;
          $mutasikas->tgl_setor = date('Y-m-d');
          $mutasikas->jth_tempo = date('Y-m-d');
          $mutasikas->no_trx = $notrx;
          $mutasikas->jenis_simpanan = 1;
          $mutasikas->mutasi = "Kredit";
          $mutasikas->nominal = $nominals;
          $mutasikas->kasbank = 1;
          $mutasikas->ket = "Pemindahan aset dari Bank ke Kas";
          $mutasikas->saldo = $saldoakhirkas;
          $mutasikas->aktif = 1;
          $mutasikas->awal = 0;
          $mutasikas->petugas = $request->user()->id;
          $mutasikas->save();

          $mutasibank = new MutasiBank;
          $mutasibank->name = $request->user()->name;
          $mutasibank->no_anggota = $request->user()->no_anggota;
          $mutasibank->tgl_setor = date('Y-m-d');
          $mutasibank->jth_tempo = date('Y-m-d');
          $mutasibank->no_trx = $notrx;
          $mutasibank->jenis_simpanan = 1;
          $mutasibank->mutasi = "Debet";
          $mutasibank->nominal = $nominals;
          $mutasibank->kasbank = 1;
          $mutasibank->ket = "Pemindahan aset dari Bank ke Kas";
          $mutasibank->saldo = $saldoakhirbank;
          $mutasibank->aktif = 1;
          $mutasibank->awal = 0;
          $mutasibank->petugas = $request->user()->id;
          $mutasibank->save();
        }
      } catch (\Exception $e) {
        DB::rollback();
        Log::info('Gagal Input pemindahan aset:'.$e->getMessage());
        flash()->overlay('Gagal Input pemindahan aset.', 'INFO GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Berhasil Input pemindahan aset.', 'INFO');
      return redirect()->back();
    }
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $kasbank = $request->dari_ke;
    $datas = PemindahanAset::whereBetWeen('tgl_trx',[$dari,$ke])->where('pemindahan','LIKE','%'.$kasbank.'%')->get();
    return view('administrator.transaksi_aset.pemindahan_saldo',compact('dashboard','from','to','kasbank','datas'));

  }
  public function editmasetmasuk($id){
    $dashboard ="transaksiaset";
    $datas = TransaksiAset::find($id);
    return view('administrator.transaksi_aset.edit_aset_masuk',compact('dashboard','datas'));
  }
  public function editmasetkeluar($id){
    $dashboard ="transaksiaset";
    $datas = TransaksiAset::find($id);
    return view('administrator.transaksi_aset.edit_aset_keluar',compact('dashboard','datas'));
  }
  public function tariksimpanan(Request $request){
    $dashboard = 'bukuSaldo';
    $message = [
      'kasbank.required'=>'Kas / Bank Wajib dipilih',
      'action.required'=>'Wajib diisi',
      'tgl_setor.required'=>'Tanggal Wajib diisi',
      'no_anggota.required'=>'Nomor anggota wajib diisi',
      'dari.required'=>'Tahun Wajib diisi',
      'sampai.required'=>'Tahun Wajib diisi',
      'jenis_simpanan.required'=>'Jenis Wajib diisi',
      'nominal.required'=>'Nominal Wajib diisi',
      'ket.required'=>'Keterangan Wajib diisi'
    ];
    $this->validate($request, [
          'tgl_setor'=>'required',
          'action'=>'required',
          'no_anggota'=>'required',
          'dari'=>'required',
          'sampai'=>'required',
          'jenis_simpanan'=>'required',
          'mutasi'=>'required',
          'kasbank'=>'required',
          'nominal'=>'required',
          'ket'=>'required'
    ],$message);
    $nominals= str_replace(",", "", $request->nominal);
    $notrx = "PS-".date('ymdhis');
    DB::beginTransaction();
    try {
      $users = User::where('no_anggota', $request->no_anggota)->where('type','anggota')->where('aktif',1)->first();
      $saldouser = $users->saldo - $nominals;

      if ($request->jenis_simpanan != 4) {
        $users->saldo_non_khusus = $users->saldo_non_khusus - $nominals;
      }
      $users->saldo = $saldouser;
      $jnssimpanan = JenisSimpanan::find($request->jenis_simpanan);
      if ($saldosimpanan = Simpananadmin::where('no_anggota', $users->no_anggota)->where('jenis_simpanan', $request->jenis_simpanan)->where('aktif', 1)->orderBy('id', 'DESC')->first()) {
          $saldoakhirsim = $saldosimpanan->saldo;
      } else {
          $saldoakhirsim = 0;
      }

      $mutasis = 'Debet';
      $saldo_akhir = $saldoakhirsim - $nominals;
      if ($saldo_akhir < 0) {
        DB::rollback();
        Log::info('Gagal tarik simpanan saldo kurang:');
        flash()->overlay('Penarikan '.$jnssimpanan->name.' berlebihan.', 'INFO GAGAL');
        $jns = $request->jenis_simpanan;
        $anggota = $request->no_anggota;
        $cari = $request->action;
        $from =$request->dari;
        $until =$request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first();
        $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'cari', 'jns'));

      }
      $saldoskasbank = SaldoKasbank::find($request->kasbank);
      // return $request->kasbank;
      $sisakas = $saldoskasbank->saldo - $nominals;
      $saldoskasbank->saldo = $sisakas;
      // flash()->overlay('DENET.', 'INFO GAGAL');
      // return redirect()->back();
      if ($saldoskasbank->name =="Kas") {
        $mutasi = new MutasiKas;
      }else {
        $mutasi = new MutasiBank;
      }
      $no_trx = date('ymdHis');
      $mutasi->name = $users->name;
      $mutasi->no_anggota = $users->no_anggota;
      $mutasi->tgl_setor = date('Y-m-d');
      $mutasi->jth_tempo = date('Y-m-d');
      $mutasi->no_trx = $notrx;
      $mutasi->jenis_simpanan = $jnssimpanan->id;
      $mutasi->mutasi = "Debet";
      $mutasi->nominal = $nominals;
      $mutasi->kasbank = $saldoskasbank->name;
      $mutasi->ket = "Penarikan simpanan ".$jnssimpanan->name.' No.Transaksi '.$no_trx;
      $mutasi->saldo = $sisakas;
      $mutasi->aktif = 1;
      $mutasi->awal = 0;
      $mutasi->petugas = $request->user()->id;



      $simpan = Simpananadmin::create([
        'name'=>$users->name,
        'no_trx'=>$no_trx,
        'no_anggota'=>$users->no_anggota,
        'tgl_setor'=>date('Y-m-d'),
        'jenis_simpanan'=>$jnssimpanan->id,
        'nominal'=>$nominals,
        'mutasi'=>$mutasis,
        'kasbank'=>$saldoskasbank->name,
        'ket'=>$request->ket,
        'saldo'=>$saldo_akhir,
        'jth_tempo'=>date('Y-m-d'),
        'awal'=>0,
        'aktif'=>1,
        'petugas'=>$request->user()->id
      ]);
      $tarik = PenarikanSimpanan::create([
        'user_id'=>$users->id,
        'no_trx'=>$notrx,
        'no_anggota'=>$users->no_anggota,
        'tgl_trx'=>date('Y-m-d'),
        'jenis_simpanan'=>$jnssimpanan->id,
        'nominal'=>$nominals,
        'saldo_kasbank'=>$saldoskasbank->id,
        'ket'=>'Penarikan simpanan '.$jnssimpanan->name,
        'admin'=>$request->user()->id,
        'aktif'=>1
      ]);
      $saldoskasbank->update();
      $mutasi->save();
      $users->update();
    } catch (\Exception $e) {
      DB::rollback();
      Log::info('Gagal tarik simpanan:'.$e->getMessage());
      flash()->overlay('Simpanan gagal ditarik.', 'INFO GAGAL');
      $jns = $request->jenis_simpanan;
      $anggota = $request->no_anggota;
      $cari = $request->action;
      $from =$request->dari;
      $until =$request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($until));
      $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first();
      $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
      $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
      $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
      $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
      return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'cari', 'jns'));

    }
    DB::commit();
    flash()->overlay('Simpanan Berhasil ditarik.', 'INFO');
    $jns = $request->jenis_simpanan;
    Log::info('Ini jenis simpanan:'.$jns);
    $anggota = $request->no_anggota;
    $cari = $request->action;
    $from =$request->dari;
    $until =$request->sampai;
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($until));
    $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first();
    $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
    $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
    $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
    $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
    return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'cari', 'jns'));

  }
  public function penjualansaham(Request $request){
    $dashboard = 'transaksi';
    if ($request->action == 'proses') {
      $message = [
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'action.required'=>'Action Wajib diisi',
        'no_anggota.required'=>'Nomor anggota wajib diisi',
        'no_anggota.min'=>'Nomor anggota Minimal 6 Karakter',
        'no_saham.required'=>'Nomor Saham wajib diisi',
        'no_saham.min'=>'Nomor Saham Minimal 3 Karakter',
        'nominal.required'=>'Nominal Wajib diisi',
        'tgl_jual.required'=>'Tanggal Penjualan Wajib diisi',
        'jatuh_tempo.required'=>'Jatuh Tempo Wajib dipilih',
        'ket.required'=>'Keterangan Wajib diisi'
      ];
      $this->validate($request, [

            'action'=>'required',
            'no_anggota'=>'required|min:6',
            'no_saham'=>'required|min:3',
            // 'tgl_jual'=>'required',
            'jatuh_tempo'=>'required',
            'kasbank'=>'required',
            'nominal'=>'required',
            'ket'=>'required'
      ],$message);

      if ($users = User::where('no_anggota','LIKE','%'.$request->no_anggota)->where('aktif',1)->where('type','<>','admin')->first()) {
        if ($users->type == 'saham' || $users->type == 'anggota') {
          $notrx = "JS-".date('ymdhis');
          $nominals= str_replace(",", "", $request->nominal);
          $req_tempo = date('Y-m-d', strtotime($request->jatuh_tempo));
          $jatuh_tempo = date('Y-m-d', strtotime('+'.$request->jatuh_tempo.' years', strtotime(date('Y-m-d'))));
          // return $jatuh_tempo;
          DB::beginTransaction();
          try {

            $datas = DataSaham::create([
              'user_id'=>$users->id,
              'tgl_trx'=>date('Y-m-d'),
              'no_trx'=>$notrx,
              'no_saham'=>$request->no_saham,
              'nominal'=>$nominals,
              'jatuh_tempo'=>$jatuh_tempo,
              'jumlah_tahun'=>$request->jatuh_tempo,
              'kasbank_id'=>$request->kasbank,
              'status'=>'jual',
              'aktif'=>1,
              'admin'=>$request->user()->id,
              'no_anggota'=>$users->no_anggota,
              'ket'=>$request->ket,
            ]);
            $saldoskasbank = SaldoKasbank::find($request->kasbank);
            $sisakas = $saldoskasbank->saldo + $nominals;
            $saldoskasbank->saldo = $sisakas;
            if ($saldoskasbank->name == 'Kas') {
              $mutasi = new MutasiKas;
            }else {
              $mutasi = new MutasiBank;
            }
            $mutasi->name = $users->name;
            $mutasi->no_anggota = $users->no_anggota;
            $mutasi->tgl_setor = date('Y-m-d');
            $mutasi->jth_tempo = date('Y-m-d');
            $mutasi->no_trx = $notrx;
            $mutasi->jenis_simpanan = 1;
            $mutasi->mutasi = "Kredit";
            $mutasi->nominal = $nominals;
            $mutasi->kasbank = $saldoskasbank->name;
            $mutasi->ket = "Penjualan saham No. ".$request->no_saham;
            $mutasi->saldo = $sisakas;
            $mutasi->aktif = 1;
            $mutasi->awal = 0;
            $mutasi->petugas = $request->user()->id;
            $saldoskasbank->update();
            $mutasi->save();
          } catch (\Exception $e) {
            DB::rollback();
            Log::info('Gagal diproses:'.$e->getMessage());
            flash()->overlay('Gagal diproses.', 'INFO GAGAL');
            return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Berhasil diproses.', 'INFO');
          return redirect()->route('admin-laporan-penjualan-saham');
        }
      }
      return 'tidak di temukan';
    }
    return view('administrator.transaksi.form_penjualan_saham',compact('dashboard'));
  }
  public function pembeliansaham(Request $request){
    $dashboard = 'transaksi';
    if ($request->action == 'proses') {
      $message = [
        'kasbank.required'=>'Kas / Bank Wajib dipilih',
        'action.required'=>'Action Wajib diisi',
        'no_anggota.required'=>'Nomor anggota wajib diisi',
        'no_anggota.min'=>'Nomor anggota Minimal 6 Karakter',
        'no_saham.required'=>'Nomor Saham wajib diisi',
        'no_saham.min'=>'Nomor Saham Minimal 3 Karakter',
        'nominal.required'=>'Nominal Wajib diisi',
        // 'tgl_beli.required'=>'Tanggal Pembelian Wajib diisi',
        'ket.required'=>'Keterangan Wajib diisi'
      ];
      $this->validate($request, [

            'action'=>'required',
            'no_anggota'=>'required|min:6',
            'no_saham'=>'required|min:3',
            // 'tgl_beli'=>'required',
            'kasbank'=>'required',
            'nominal'=>'required',
            'ket'=>'required'
      ],$message);

      if ($users = User::where('no_anggota','LIKE','%'.$request->no_anggota)->where('aktif',1)->where('type','<>','admin')->first()) {
        if ($users->type == 'saham' || $users->type == 'anggota') {
          $notrx = "JS-".date('ymdhis');
          $nominals= str_replace(",", "", $request->nominal);
          $jatuh_tempo = date('Y-m-d', strtotime($request->jatuh_tempo));
          DB::beginTransaction();
          try {
            $datas = DataSaham::create([
              'user_id'=>$users->id,
              'tgl_trx'=>date('Y-m-d'),
              'no_trx'=>$notrx,
              'no_saham'=>$request->no_saham,
              'nominal'=>$nominals,
              'jatuh_tempo'=>date('Y-m-d'),
              'status'=>'beli',
              'kasbank_id'=>$request->kasbank,
              'aktif'=>1,
              'admin'=>$request->user()->id,
              'no_anggota'=>$users->no_anggota,
              'ket'=>$request->ket,
            ]);
            $saldoskasbank = SaldoKasbank::find($request->kasbank);
            $sisakas = $saldoskasbank->saldo - $nominals;
            $saldoskasbank->saldo = $sisakas;
            if ($saldoskasbank->name == 'Kas') {
              $mutasi = new MutasiKas;
            }else {
              $mutasi = new MutasiBank;
            }
            $mutasi->name = $users->name;
            $mutasi->no_anggota = $users->no_anggota;
            $mutasi->tgl_setor = date('Y-m-d');
            $mutasi->jth_tempo = date('Y-m-d');
            $mutasi->no_trx = $notrx;
            $mutasi->jenis_simpanan = 1;
            $mutasi->mutasi = "Debet";
            $mutasi->nominal = $nominals;
            $mutasi->kasbank = $saldoskasbank->name;
            $mutasi->ket = "Penjualan saham No. ".$request->no_saham;
            $mutasi->saldo = $sisakas;
            $mutasi->aktif = 1;
            $mutasi->awal = 0;
            $mutasi->petugas = $request->user()->id;
            $saldoskasbank->update();
            $mutasi->save();
          } catch (\Exception $e) {
            DB::rollback();
            Log::info('Gagal diproses:'.$e->getMessage());
            flash()->overlay('Gagal diproses.', 'INFO GAGAL');
            return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Berhasil diproses.', 'INFO');
          return redirect()->route('admin-laporan-pembelian-saham');
        }
      }
      flash()->overlay('Nomor anggota salah', 'INFO GAGAL');
      return redirect()->back();
    }
    return view('administrator.transaksi.form_pembelian_saham',compact('dashboard'));
  }
}
