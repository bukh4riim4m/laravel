<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\Pinjaman;
use App\BayarPinjaman;
use App\MutasiKas;
use App\MutasiBank;
use Excel;
use DB;
use Log;

class DatamutasiController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function mutasikas(Request $request){
    $dashboard ="datamutasi";
    $from = date('01-m-Y');
    $until = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($until));
    if ($request->action=="cari") {
      $from = $request->dari;
      $until = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($until));
    }

    $mutasikases = MutasiKas::whereBetWeen('tgl_setor',[$dari,$ke])->get();
    return view('administrator.datamutasi.mutasikas', compact('dashboard','mutasikases','from','until'));
  }
  public function mutasibank(Request $request){
    $dashboard ="datamutasi";
    $from = date('01-m-Y');
    $until = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($until));
    if ($request->action=="cari") {
      $from = $request->dari;
      $until = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($until));
    }
    $mutasikases = MutasiBank::whereBetWeen('tgl_setor',[$dari,$ke])->get();
    return view('administrator.datamutasi.mutasibank', compact('dashboard','mutasikases','from','until'));
  }
}
