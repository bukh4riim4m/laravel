<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Akumulasi;
// use Charts;
use App\Charts\SampleChart;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Simpananadmin;
use App\Pinjaman;
use Excel;
use DB;
use Log;
use PDF;

class StatistikController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }

  public function statistikakumulasi(Request $request){
    $dashboard ="statistik";
    $dari = date("Y-m-d");
    $sampai = date("Y-m-d");
    if ($request->dari && $request->sampai) {
      $dari = date('Y-m-d', strtotime($request->dari));
      $sampai = date('Y-m-d', strtotime($request->sampai));
    }
    $totalramanuju = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Anggota'");
    $totalgrogol = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Anggota'");
    $totalpci = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Anggota'");
    $totalramanujunon = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Non'");
    $totalgrogolnon = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Non'");
    $totalpcinon = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Non'");
    if ($request->action =='excel' && $request->excel ==1) {
      return Excel::create($dari.'-'.$sampai, function ($excel) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
            $excel->sheet('Excel sheet', function($sheet) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
            $sheet->cells('A1:C1', function($cells) {
              $cells->setBackground('#ffff00');
            });
            $sheet->cells('A6:C6', function($cells) {
              $cells->setBackground('#ffff00');
            });
            $sheet->cells('A8:C8', function($cells) {
              $cells->setBackground('#66ff66');
            });
            $sheet->cells('A13:C13', function($cells) {
              $cells->setBackground('#66ff66');
            });
            $sheet->cells('A15:C15', function($cells) {
              $cells->setBackground('#66ffcc');
            });
            $sheet->cells('A20:C20', function($cells) {
              $cells->setBackground('#66ffcc');
            });
            $sheet->loadView('administrator.report._report_statistik_penjualan', compact('totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
          });
      })->export('xls');
    }elseif ($request->action =='pdf' && $request->excel ==1) {
      $pdf = PDF::loadView('administrator.report.pdf_statistik_penjualan',compact('totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
      return $pdf->download($dari.'_'.$sampai.'_Statistik.pdf');
    }
    return view('administrator.statistik',compact('dashboard','totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
  }

  public function karyawan(Request $request){
    $dashboard ="dataMaster";
    $nomor =$request->no_karyawan;
    $name =$request->name;
    $type="";
    if ($request->action =='tambah') {
      $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'no_karyawan' => 'required|min:6',
      'email'=>'required',
      'id_login' => 'required|min:6',
      'password_login' => 'required|min:6'
      ]);
      if ($idlog = User::where('sequence',$request->id_login)->first()) {
        flash()->overlay('GAGAL, ID Login Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      if ($idemail = User::where('email',$request->email)->first()) {
        flash()->overlay('GAGAL, Email Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      $user = User::create([
        'name' => $request->name,
        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
        'no_anggota' => $request->no_karyawan,
        'sequence' => $request->id_login,
        'email' => $request->email,
        'telp'=>$request->telp,
        'jenkel' => $request->jenkel,
        'password' => Hash::make($request->password_login),
        'saldo' => 0,
        'admin' => $request->user()->id,
        'aktif' => 1,
        'type' => 'karyawan'
      ]);
      if ($user) {
        flash()->overlay('Karyawan berhasil di tambahkan.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Karyawan gagal di tambahkan.','INFO');
      return redirect()->back();
    }elseif ($request->action =='edit') {
      if ($request->fotodiri) {
        $this->validate($request, [
              'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
      }
      DB::beginTransaction();
      try {
        $user = User::find($request->ids);
        $user->name=$request->name;
        $user->no_anggota=$request->no_karyawan;
        $user->tgl_lahir=date('Y-m-d', strtotime($request->tgl_lahir));
        $user->email=$request->email;
        $user->telp=$request->telp;
        $user->jenkel=$request->jenkel;

        if ($request->fotodiri) {
          if ($user->fotodiri =='null' || $user->fotodiri =='') {
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }else {
            $destination_foto =public_path('foto/'.$user->fotodiri);
            if(file_exists($destination_foto)){
                        unlink($destination_foto);
            }
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }
        }
        $user->update();
      } catch (\Exception $e) {
        Log::info('Gagal Edit Profil:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Edit Profil.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Data Profil berhasil di Edit.','INFO');
      return redirect()->back();
    }elseif ($request->action =='hapus') {
      $delete = User::find($request->ids);
      $delete->aktif = 0;
      if ($delete->update()) {
        flash()->overlay('Data Profil berhasil di Hapus.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Profil Gagal di Hapus.','INFO');
      return redirect()->back();
    }
    $user = User::where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$name.'%')->where('aktif',1)->where('type','karyawan')->orderBy('no_anggota','ASC')->get();
    return view('administrator.dataKaryawan',compact('dashboard','user','nomor','name','type'));
  }
  public function chart(Request $request){
    $dashboard = "statistik";
    $lakis = DB::select("SELECT SUM(users.aktif) as totalLaki FROM users WHERE users.aktif ='1' AND users.jenkel = 'Laki-Laki' AND users.type ='anggota'");
    $perempuan = DB::select("SELECT SUM(users.aktif) as totalPerempuan FROM users WHERE users.aktif ='1' AND users.jenkel = 'Perempuan' AND users.type ='anggota'");
    return view('administrator.statistikJenkel', compact('dashboard','lakis','perempuan'));
  }
  public function statistikkelurahan(){
    $dashboard = "statistik";
    $kelurahans = Kelurahan::where('aktif',1)->get();
    return view('administrator.statistikKelurahan', compact('dashboard','kelurahans'));

  }
  public function statistikkecamatan(){
    $dashboard = "statistik";
    $kecamatans = Kecamatan::where('aktif',1)->get();
    return view('administrator.statistik_kecamatan', compact('dashboard','kecamatans'));

  }
  public function statistikkabupaten(){
    $dashboard = "statistik";
    $kabupatens = Kabupaten::where('aktif',1)->get();
    return view('administrator.statistk_kabupaten', compact('dashboard','kabupatens'));
  }
  public function statistiksimpanan(Request $request){
    $dashboard = "statistik";
    $pokoks = DB::select("SELECT SUM(simpananadmins.nominal) as totalpokok FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '1' AND simpananadmins.mutasi = 'Kredit'");
    $wajins = DB::select("SELECT SUM(simpananadmins.nominal) as totalwajib FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '2' AND simpananadmins.mutasi = 'Kredit'");
    $sukarelas = DB::select("SELECT SUM(simpananadmins.nominal) as totalsukarela FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '3' AND simpananadmins.mutasi = 'Kredit'");
    $investasis = DB::select("SELECT SUM(simpananadmins.nominal) as totalinvestasi FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '4' AND simpananadmins.mutasi = 'Kredit'");
    $wakafs = DB::select("SELECT SUM(simpananadmins.nominal) as totalwakaf FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '5' AND simpananadmins.mutasi = 'Kredit'");
    $infaqs = DB::select("SELECT SUM(simpananadmins.nominal) as totalinfak FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '6' AND simpananadmins.mutasi = 'Kredit'");
    $shus = DB::select("SELECT SUM(simpananadmins.nominal) as totalshu FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '7' AND simpananadmins.mutasi = 'Kredit'");
    $lains = DB::select("SELECT SUM(simpananadmins.nominal) as totallain FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '8' AND simpananadmins.mutasi = 'Kredit'");
    return view('administrator.statistik_simpanan', compact('dashboard','pokoks','wajins','sukarelas','investasis','wakafs','infaqs','shus','lains'));
  }
  public function statistikpinjaman(Request $request){
    $dashboard = "statistik";
    $tahun = '2019';
    $januari = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-01').'%')->where('aktif',1)->sum('nilai_pinjam');
    $februari = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-02').'%')->where('aktif',1)->sum('nilai_pinjam');
    $maret = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-03').'%')->where('aktif',1)->sum('nilai_pinjam');
    $april = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-04').'%')->where('aktif',1)->sum('nilai_pinjam');
    $mei = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-05').'%')->where('aktif',1)->sum('nilai_pinjam');
    $juni = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-06').'%')->where('aktif',1)->sum('nilai_pinjam');
    $juli = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-07').'%')->where('aktif',1)->sum('nilai_pinjam');
    $agustus = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-08').'%')->where('aktif',1)->sum('nilai_pinjam');
    $september = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-09').'%')->where('aktif',1)->sum('nilai_pinjam');
    $oktober = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-10').'%')->where('aktif',1)->sum('nilai_pinjam');
    $november = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-11').'%')->where('aktif',1)->sum('nilai_pinjam');
    $desember = Pinjaman::where('tgl_trx','LIKE','%'.date($tahun.'-12').'%')->where('aktif',1)->sum('nilai_pinjam');
    return view('administrator.grafik.grafik_pinjaman',compact('dashboard','januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'));
  }
  public function statistiksimpananpokok(Request $request){
    $dashboard = "statistik";
    $tahun = '2019';
    $januari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $februari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $maret = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $april = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $mei = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $juni = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $juli = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $agustus = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $september = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $oktober = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $november = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');
    $desember = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',1)->where('aktif',1)->sum('nominal');

    $januariw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $februariw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $maretw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $aprilw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $meiw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $juniw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $juliw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $agustusw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $septemberw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $oktoberw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $novemberw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
    $desemberw = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');

    $januaris = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $februaris = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $marets = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $aprils = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $meis = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $junis = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $julis = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $agustuss = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $septembers = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $oktobers = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $novembers = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
    $desembers = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');

    $januarik = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $februarik = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $maretk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $aprilk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $meik = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $junik = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $julik = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $agustusk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $septemberk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $oktoberk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $novemberk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
    $desemberk = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');

    return view('administrator.grafik.grafik_simpanan_pokok',compact('dashboard','januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember','januariw','februariw','maretw','aprilw','meiw','juniw','juliw','agustusw','septemberw','oktoberw','novemberw','desemberw','januaris','februaris','marets','aprils','meis','junis','julis','agustuss','septembers','oktobers','novembers','desembers','januarik','februarik','maretk','aprilk','meik','junik','julik','agustusk','septemberk','oktoberk','novemberk','desemberk'));
  }
  // public function statistiksimpananwajib(Request $request){
  //   $dashboard = "statistik";
  //   $tahun = '2019';
  //   $januari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $februari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $maret = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $april = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $mei = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $juni = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $juli = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $agustus = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $september = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $oktober = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $november = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   $desember = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',2)->where('aktif',1)->sum('nominal');
  //   return view('administrator.grafik.grafik_simpanan_wajib',compact('dashboard','januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'));
  // }
  // public function statistiksimpanansukarela(Request $request){
  //   $dashboard = "statistik";
  //   $tahun = '2019';
  //   $januari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $februari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $maret = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $april = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $mei = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $juni = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $juli = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $agustus = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $september = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $oktober = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $november = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   $desember = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',3)->where('aktif',1)->sum('nominal');
  //   return view('administrator.grafik.grafik_simpanan_sukarela',compact('dashboard','januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'));
  // }
  // public function statistiksimpanankhusus(Request $request){
  //   $dashboard = "statistik";
  //   $tahun = '2019';
  //   $januari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-01').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $februari = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-02').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $maret = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-03').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $april = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-04').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $mei = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-05').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $juni = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-06').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $juli = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-07').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $agustus = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-08').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $september = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-09').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $oktober = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-10').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $november = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-11').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   $desember = Simpananadmin::where('tgl_setor','LIKE','%'.date($tahun.'-12').'%')->where('jenis_simpanan',4)->where('aktif',1)->sum('nominal');
  //   return view('administrator.grafik.grafik_simpanan_khusus',compact('dashboard','januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'));
  // }
}
