<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\City;
use App\SaldoKasbank;
use App\MutasiKas;
use App\MutasiBank;
use Image;
use Excel;
use DB;
use Log;

class ShuController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function index(Request $request){
    $dashboard ="dataShu";
    $nomor = $request->no_anggota;
    $name =$request->name;
    $user = User::where('aktif', 1)->where('type','anggota')->where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$name.'%')->paginate(100);
    $totalsimpanannonkhusus = User::where('aktif', 1)->where('type','<>','karyawan')->sum('saldo_non_khusus');
    $totalsemuasimpanan = User::where('aktif', 1)->where('type','<>','karyawan')->sum('saldo');
    return view('administrator.shu.index_shu', compact('dashboard','totalsimpanannonkhusus','totalsemuasimpanan', 'user','nomor','name'));
  }
}
