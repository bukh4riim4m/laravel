<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportUsers;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\City;
use App\Pinjaman;
use App\BayarPinjaman;
use App\SaldoKasbank;
use App\MutasiKas;
use App\MutasiBank;
use App\PenarikanSimpanan;
use App\DataSaham;
use App\TransaksiAset;
use Image;
// use Excel;
use PDF;
use DB;
use Log;

class LaporanController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function simpananadmin(Request $request)
  {
      $dashboard ="laporan";
      $nomor = $request->no_anggota;
      $jenissim = $request->jenis_simpanan;
      $from = $request->dari;
      $to = $request->sampai;
      $mutasis = $request->mutasi;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
      if ($request->action =='cari') {
          $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->paginate(200);
          return view('administrator.laporan.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
      } elseif ($request->action =='csv') {
          /////////////////////////
          $validatedData = $request->validate([
      'upload' => 'required'
      ]);
          $ktp = '12345.'.$request->upload->getClientOriginalExtension();
          $sequence = substr($ktp, -3);
          if ($sequence !=='csv') {
              flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
              return redirect()->back();
          }
          $upload = $request->file('upload');
          $filePath = $upload->getRealPath();
          $file = fopen($filePath, 'r');
          $header = fgetcsv($file);
          $escapedHeader=[];
          //validate
          foreach ($header as $key => $value) {
              $lheader=strtolower($value);
              $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
              array_push($escapedHeader, $escapedItem);
          }
          //looping through othe columns
          while ($columns=fgetcsv($file)) {
              if ($columns[0]=="") {
                  continue;
              }
              //trim data
              foreach ($columns as $key => &$value) {
                  $value=$value;
              }
              // return $columns;
              $data = array_combine($escapedHeader, $columns);
              // setting type
              foreach ($data as $key => &$value) {
                  $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
              }
              // return $data;
              // Table update
              $nokssds=$data['nokssd'];
              $namas=$data['nama'];
              $tanggals=$data['tanggal'];
              $notrx=$data['nomortransaksi'];
              $nominals = $data['nominal'];
              $saldos = $data['saldo'];
              $kodes = $data['kode'];
              $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
              $simpanan->no_anggota=$nokssds;
              $simpanan->tgl_setor = $tanggals;
              $simpanan->no_trx = $notrx;
              $simpanan->mutasi='Debet';
              $simpanan->nominal=$nominals;
              $simpanan->saldo=$saldos;
              $simpanan->ket=$kodes;
              $simpanan->aktif=1;
              $simpanan->jenis_simpanan=$kodes;
              $simpanan->petugas = $request->user()->id;
              $simpanan->save();
          }
          flash()->overlay('CSV Berhasil di Upload.', 'INFO');
          return redirect()->back();
      }elseif ($request->action == "export") {

        //return Excel::download(new ExportUsers, 'users.xlsx');
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        $totalQuery = count($simpanan);
        $while = ceil($totalQuery / 500);
        $collections = collect($simpanan);
        return Excel::create($from.' - '.$to, function ($excel) use ($while, $collections) {
            for ($i = 1; $i <= $while; $i++) {
                $items = $collections->forPage($i, 500);
                $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                    $sheet->loadView('administrator.report._report_simpanan_anggota', ['simpanan' => $items]);
                });
            }
        })->export('xlsx');
      }
      $from = date('01-m-Y');
      $to = date('d-m-Y');
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d');
      $simpanan = Simpananadmin::where('no_anggota','LIKE','%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('aktif', 1)->where('mutasi', 'Kredit')->orderBy('tgl_setor', 'ASC')->paginate(200);
      return view('administrator.laporan.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function pinjaman(Request $request){
    $dashboard ="laporan";
    $nomor = $request->no_anggota;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    if ($request->action =="cari") {
      $from = date('d-m-Y', strtotime($request->dari));
      $to = date('d-m-Y', strtotime($request->sampai));
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }elseif ($request->action =="history") {
      if ($pinjamans = Pinjaman::where('no_pinjam',$request->no_pinjam)->where('aktif',1)->first()) {
        $users = User::where('no_anggota',$pinjamans->no_anggota)->first();
        $historys = BayarPinjaman::where('no_pinjamen',$request->no_pinjam)->where('aktif',1)->get();
        $terbayar = BayarPinjaman::where('no_pinjamen',$request->no_pinjam)->where('aktif',1)->where('status',2)->sum('bayar');
        return view('administrator.laporan.history_pembayaran_pinjam',compact('dashboard','historys','pinjamans','terbayar','users'));
      }
      flash()->overlay('Nomor pinjaman tidak ditemukan','GAGAL');
      return redirect()->back();
    }elseif ($request->action =="downloadpdf") {
      $pinjamans = Pinjaman::where('no_pinjam',$request->no_pinjam)->where('aktif',1)->first();
      $users = User::where('no_anggota',$pinjamans->no_anggota)->first();
      $historys = BayarPinjaman::where('no_pinjamen',$request->no_pinjam)->where('aktif',1)->get();
      $terbayar = BayarPinjaman::where('no_pinjamen',$request->no_pinjam)->where('aktif',1)->where('status',2)->sum('bayar');
      $pdf = PDF::loadView('administrator.report._history_pembayaran_pinjaman_pdf',compact('historys','users','terbayar','pinjamans'))->setPaper('a4', 'landscape');
      return $pdf->download('Pembayaran_'.$request->no_pinjam.'.pdf');
    }
    $pinjamans = Pinjaman::where('no_anggota','LIKE','%'.$nomor.'%')->whereBetWeen('tgl_trx', [$dari,$ke])->where('status_pinjam','LIKE','%'.$request->status_pinjam.'%')->where('aktif', 1)->orderBy('tgl_trx', 'ASC')->get();
    return view('administrator.laporan.pinjaman', compact('dashboard', 'nomor', 'pinjamans', 'from', 'to'));
  }
  public function pembayaran(Request $request){
    $dashboard ="laporan";
    $nomor = $request->no_pinjaman;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    $status = $request->status;
    if ($request->action == "cari") {
      $from = $request->dari;
      $to = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    $laporans = BayarPinjaman::whereBetWeen('tgl_bayar',[$dari,$ke])->where('no_pinjamen','LIKE','%'.$nomor.'%')->where('aktif',1)->where('status','LIKE','%'.$status.'%')->get();

    return view('administrator.laporan.pinjaman_terbayar', compact('dashboard', 'nomor', 'from', 'to','laporans','status'));
  }

  public function tempo(Request $request){
    $dashboard ="jatuhTempo";
    $no_anggota = $request->no_anggota;
    $nama_anggota = $request->nama_anggota;
    if ($request->action=='export' && $request->excel==1) {
      $users = User::where('aktif',1)->where('type','anggota')->where('no_anggota','LIKE','%'.$no_anggota.'%')->where('name','LIKE','%'.$nama_anggota.'%')->get();
      $totalQuery = count($users);
      $while = ceil($totalQuery / 500);
      $collections = collect($users);
      return Excel::create('Simpanan jatuh tempo', function ($excel) use ($while, $collections) {
          for ($i = 1; $i <= $while; $i++) {
              $items = $collections->forPage($i, 500);
              $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                  $sheet->loadView('administrator.report._excel_simpanan_wajib_jatuh_tempo', ['users' => $items]);
              });
          }
      })->export('xlsx');
    }
    $users = User::where('aktif',1)->where('type','anggota')->where('no_anggota','LIKE','%'.$no_anggota.'%')->where('name','LIKE','%'.$nama_anggota.'%')->paginate(200);
    return view('administrator.alert_simpanan_wajib',compact('dashboard','no_anggota','nama_anggota','users'));
  }
  public function pembayaranpinjaman(Request $request){
    $dashboard="laporan";
    if ($request->action =='bayar') {
      $message = [
        'pembayaran.required'=>'Pembayaran Wajib dipilih',
        'action.required'=>'Tidak disarankan',
        'ids.required'=>'Tidak disarankan'
      ];
      $this->validate($request, [
            'pembayaran'=>'required',
            'action'=>'required',
            'ids'=>'required'
      ]);
      $notrx = "BP-".date('ymdhis');
      DB::beginTransaction();
      try {
        $bayar = BayarPinjaman::find($request->ids);
        $ceksisa = Pinjaman::where('no_pinjam',$bayar->no_pinjamen)->where('aktif',1)->first();
        $saldo = SaldoKasbank::find($request->pembayaran);
        $saldoakhir = $saldo->saldo + $bayar->bayar;
        if ($saldo->name == 'Kas') {
          $mutasi = new MutasiKas;
        }else {
          $mutasi = new MutasiBank;
        }
        $mutasi->name = $request->user()->name;
        $mutasi->no_anggota = $ceksisa->no_anggota;
        $mutasi->tgl_setor = date('Y-m-d');
        $mutasi->jth_tempo = date('Y-m-d');
        $mutasi->no_trx = $notrx;
        $mutasi->jenis_simpanan = 1;
        $mutasi->mutasi = 'Kredit';
        $mutasi->nominal = $bayar->bayar;
        $mutasi->kasbank = $saldo->name;
        $mutasi->ket = 'Pembayaran pinjaman '.$ceksisa->no_pinjam.' ke '.$bayar->urutan_bayar;
        $mutasi->saldo = $saldoakhir;
        $mutasi->aktif = 1;
        $mutasi->awal = 0;
        $mutasi->petugas = $request->user()->id;
        $mutasi->save();

        $saldo->saldo = $saldoakhir;
        $saldo->update();
        $sisa = $ceksisa->sisa_pinjam - $bayar->bayar;
        $ceksisa->sisa_pinjam = $sisa;
        $bayar->tgl_bayar = date('Y-m-d');
        $bayar->sisa_pinjaman = $sisa;
        if ($sisa ==0) {
          $ceksisa->status_pinjam = 'Sudah Lunas';
        }
        $bayar->status = 2;
        $bayar->created_by = $request->user()->name;
        $bayar->admin = $request->user()->id;

        $bayar->update();
        $ceksisa->update();


      } catch (\Exception $e) {
        Log::info('Gagal bayar pinjaman :'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Bayar Pinjaman.','GAGAL');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Pembayaran Berhasil.','INFO');
      $pinjamans = Pinjaman::where('no_pinjam',$ceksisa->no_pinjam)->where('aktif',1)->first();
      $users = User::where('no_anggota',$ceksisa->no_anggota)->first();
      $historys = BayarPinjaman::where('no_pinjamen',$ceksisa->no_pinjam)->where('aktif',1)->get();
      $terbayar = BayarPinjaman::where('no_pinjamen',$ceksisa->no_pinjam)->where('aktif',1)->where('status',2)->sum('bayar');
      // return $pinjamans;
      return view('administrator.laporan.history_pembayaran_pinjam',compact('dashboard','historys','pinjamans','terbayar','users'));
      }
    }
    public function laporantariksimpanan(Request $request){
      // flash()->overlay("<p class='alert alert-danger'>BELUM DAPAT DI GUNAKAN.</p>", "INFO");
      // return redirect()->back();
      $dashboard="laporan";
      $from = date('01-m-Y');
      $to = date('d-m-Y');
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
      $nomor = $request->no_anggota;
      if ($request->action == "cari") {
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
      }elseif ($request->action == "export") {
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('mutasi', 'Debet')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        $totalQuery = count($simpanan);
        $while = ceil($totalQuery / 500);
        $collections = collect($simpanan);
        return Excel::create('Penarikan_'.$from.' - '.$to, function ($excel) use ($while, $collections) {
            for ($i = 1; $i <= $while; $i++) {
                $items = $collections->forPage($i, 500);
                $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                    $sheet->loadView('administrator.report._excel_penarikan_simpanan', ['simpanan' => $items]);
                });
            }
        })->export('xlsx');
      }
      $laporans = PenarikanSimpanan::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->get();
      return view('administrator.laporan.laporan_tarik_simpanan',compact('dashboard','from','to','laporans','nomor'));
  }
  public function laporankeseluruhan(Request $request){
    // flash()->overlay("<p class='alert alert-danger'>BELUM DAPAT DI GUNAKAN.</p>", "INFO");
    // return redirect()->back();
      $dashboard="laporan";
      $from = date('01-m-Y');
      $to = date('d-m-Y');
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
      if ($request->action == "cari") {
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
      }
      $simpananpokok = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',1)->where('mutasi','Kredit')->sum('nominal');
      $simpananwajib = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',2)->where('mutasi','Kredit')->sum('nominal');
      $simpanansukarela = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',3)->where('mutasi','Kredit')->sum('nominal');
      $simpanankhusus = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',4)->where('mutasi','Kredit')->sum('nominal');

      $tariksimpananpokok = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',1)->where('mutasi','Debet')->sum('nominal');
      $tariksimpananwajib = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',2)->where('mutasi','Debet')->sum('nominal');
      $tariksimpanansukarela = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',3)->where('mutasi','Debet')->sum('nominal');
      $tariksimpanankhusus = Simpananadmin::whereBetWeen('tgl_setor',[$dari,$ke])->where('aktif',1)->where('jenis_simpanan',4)->where('mutasi','Debet')->sum('nominal');

      $totalpinjaman = Pinjaman::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->sum('nilai_pinjam');
      $totalbayarpinjaman = BayarPinjaman::whereBetWeen('tgl_bayar',[$dari,$ke])->where('aktif',1)->where('status',2)->sum('bayar');

      $totalasetmasuk = TransaksiAset::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->where('status','masuk')->sum('nominal');
      $totalasetkeluar = TransaksiAset::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->where('status','keluar')->sum('nominal');

      $totalsahamterjual = DataSaham::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->where('status','jual')->sum('nominal');
      $totalsahamdibeli = DataSaham::whereBetWeen('tgl_trx',[$dari,$ke])->where('aktif',1)->where('status','beli')->sum('nominal');

      $totalpemasukan = [$simpananpokok,$simpananwajib,$simpanansukarela,$simpanankhusus,$totalbayarpinjaman,$totalasetmasuk,$totalsahamterjual];
      $totalpengeluaran = [$tariksimpananpokok,$tariksimpananwajib,$tariksimpanansukarela,$tariksimpanankhusus,$totalpinjaman,$totalasetkeluar,$totalsahamdibeli];
      return view('administrator.laporan.all_transaksi',compact('totalpengeluaran','totalpemasukan','totalsahamdibeli','totalsahamterjual','dashboard','from','to','simpananpokok','simpananwajib','simpanansukarela','simpanankhusus','tariksimpananpokok','tariksimpananwajib','tariksimpanansukarela','tariksimpanankhusus','totalpinjaman','totalbayarpinjaman','totalasetmasuk','totalasetkeluar'));
  }
  public function jualsaham(Request $request){
    $dashboard = 'laporan';
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $nomor = $request->no_anggota;
    if ($request->action == "cari") {
      $from = $request->dari;
      $to = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    $datas = DataSaham::where('status','jual')->whereBetWeen('tgl_trx',[$dari,$ke])->where('no_anggota','LIKE','%'.$nomor.'%')->paginate(200);
    return view('administrator.laporan.penjualan_saham',compact('dashboard','datas','nomor','from','to'));
  }
  public function belisaham(Request $request){
    $dashboard = 'laporan';
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $nomor = $request->no_anggota;
    if ($request->action == "cari") {
      $from = $request->dari;
      $to = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    // return 'OK';
    $datas = DataSaham::where('status','beli')->whereBetWeen('tgl_trx',[$dari,$ke])->where('no_anggota','LIKE','%'.$nomor.'%')->get();
    return view('administrator.laporan.pembelian_saham',compact('dashboard','datas','nomor','from','to'));
  }
  public function listsimpanan(Request $request){
    $dashboard ="laporan";
    $nomor = $request->no_anggota;
    $nama = $request->nama_anggota;
    if ($request->action == "export" && $request->export == 1) {
      $datas = User::where('saldo','<>',0)->where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$nama.'%')->where('aktif',1)->get();
      $totalQuery = count($datas);
      $while = ceil($totalQuery / 500);
      $collections = collect($datas);
      return Excel::create('Download Simpanan '.date('d-m-Y'), function ($excel) use ($while, $collections) {
          for ($i = 1; $i <= $while; $i++) {
              $items = $collections->forPage($i, 500);
              $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                  $sheet->loadView('administrator.report._excel_simpanan_anggota', ['datas' => $items]);
              });
          }
      })->export('xlsx');
    }
    $datas = User::where('saldo','<>',0)->where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$nama.'%')->where('aktif',1)->get();
    return view('administrator.laporan.list_simpanan', compact('dashboard','nomor','nama', 'datas'));
  }
}
