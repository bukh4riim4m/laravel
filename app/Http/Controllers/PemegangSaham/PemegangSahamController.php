<?php

namespace App\Http\Controllers\PemegangSaham;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\JenisMasukanKeluaran;
use App\DataSaham;
use DB;
use Log;
use Auth;

class PemegangSahamController extends Controller
{
  public function __construct()
  {
      $this->middleware('saham');
  }
  public function homesaham()
  {
      $dashboard ="dashboard";
      return view('pemegang_saham.index', compact('dashboard'));
  }
  public function datajualsaham(Request $request){
    $dashboard ="datasaham";
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $no_saham = $request->no_saham;
    if ($request->action == "cari") {
      $from = $request->dari;
      $to = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    $datas = DataSaham::where('status','beli')->whereBetWeen('tgl_trx',[$dari,$ke])->where('no_anggota',$request->user()->no_anggota)->where('no_saham','LIKE','%'.$no_saham.'%')->paginate(100);
    return view('pemegang_saham.penjualan_saham',compact('dashboard','datas','no_saham','from','to'));
  }
  public function databelisaham(Request $request){
    $dashboard = 'datasaham';
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    $no_saham = $request->no_saham;
    if ($request->action == "cari") {
      $from = $request->dari;
      $to = $request->sampai;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    $datas = DataSaham::where('status','jual')->whereBetWeen('tgl_trx',[$dari,$ke])->where('no_anggota',$request->user()->no_anggota)->where('no_saham','LIKE','%'.$no_saham.'%')->paginate(100);
    return view('pemegang_saham.pembelian_saham',compact('dashboard','datas','no_saham','from','to'));
  }
  public function profil(Request $request)
  {
      $dashboard ="profil";
      if ($request->fotodiri) {
          $this->validate($request, [
          'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ]);
          $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
      }

      if ($request->tgl_lahir =="") {
          $tanggal_lahir = "";
      } else {
          $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
      }
      if ($request->action =='edit') {
          DB::beginTransaction();
          try {
              $user = User::find($request->user()->id);
              $user->nik=$request->nik;
              $user->tpt_lahir=$request->tpt_lahir;
              $user->tgl_lahir=$tanggal_lahir;
              $user->npwp=$request->npwp;
              $user->email=$request->email;
              $user->agama=$request->agama;
              $user->pendidikan=$request->pendidikan;
              $user->statuskeluarga=$request->statuskeluarga;
              $user->nama_ibu=$request->nama_ibu;
              $user->nama_ayah=$request->nama_ayah;
              $user->telp=$request->telp;
              $user->jenkel=$request->jenkel;
              $user->alamat=$request->alamat;
              $user->kelurahan=$request->kelurahan;
              $user->kecamatan=$request->kecamatan;
              $user->kabupaten=$request->kabupaten;
              $user->komunitas=$request->komunitas;
              $user->propinsi=$request->propinsi;
              $user->pendapatan=$request->pendapatan;
              $user->pekerjaan=$request->pekerjaan;
              if ($request->fotodiri) {
                  if ($user->fotodiri =='null' || $user->fotodiri =='') {
                      $user->fotodiri=$diri;
                      $destination_foto =public_path('foto');
                      $request->fotodiri->move($destination_foto, $diri);
                  } else {
                      $destination_foto =public_path('foto/'.$user->fotodiri);
                      if (file_exists($destination_foto)) {
                          unlink($destination_foto);
                      }
                      $user->fotodiri=$diri;
                      $destination_foto =public_path('foto');
                      $request->fotodiri->move($destination_foto, $diri);
                  }
              }
              $user->update();
          } catch (\Exception $e) {
              Log::info('Gagal Edit Profil:'.$e->getMessage());
              DB::rollback();
              flash()->overlay('Gagal Edit Profil.', 'INFO');
              return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Data Profil berhasil di Edit.', 'INFO');
          return redirect()->back();
      }
      return view('pemegang_saham.profil', compact('dashboard'));
  }
  public function password()
  {
      $dashboard ="gantiPassword";
      return view('pemegang_saham.gantiPassword', compact('dashboard'));
  }
  public function gantipassword(Request $request)
  {
      // Log::info(' ALERt :'.$request);
      $message = [
        'password_lm.required'=>'Nomor anggota wajib diisi',
        'password_lm.min'=>'Password Lama Minimal 6 Karakter',
        'password_br.required'=>'Nomor Saham wajib diisi',
        'password_br.min'=>'Password Baru Minimal 6 Karakter'

      ];
      $this->validate($request, [
            'password_lm'=>'required|min:6',
            'password_br'=>'required|min:6'
      ],$message);

      if (\Hash::check($request->input('password_lm'), $request->user()->password)) {
          $user = User::find($request->user()->id);
          $user->password = bcrypt($request->input('password_br'));
          $user->update();
          flash()->overlay('Password Berhasil di rubah.', 'INFO');
          return redirect()->back();
      } else {
          flash()->overlay('Password Gagal di rubah<br>Password Lama Salah', 'INFO');
          return redirect()->back();
      }
  }
}
